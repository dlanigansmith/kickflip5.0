
#include "ESP.hpp"
//include all types of esp
#include "BoxESP.h"
#include "WorldESP.hpp"
#include "Glow.hpp"
#include "CLRRender.hpp"
#include "CNadeTracer.hpp"
void ESP::Run()
{
	if (!SourceEngine::Interfaces::Engine()->IsInGame()) return;
	Glow::GlowTick();
	for (int i = 1; i < SourceEngine::Interfaces::EntityList()->GetHighestEntityIndex(); i++) //loop through entities
	{
		BoxESP::ESPTick(i);
		WorldESP::WorldESPTick(i);
		CLRRender::CLRRenderTick(i);
		NadeTracer->Run(i);
	}
	if (GlobalVars.Settings.Visuals.Tracers)
	{
		NadeTracer->Draw();
		NadeTracer->Clear();
	}
} 