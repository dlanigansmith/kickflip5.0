#include "includes.hpp"
#include "Hooks.hpp"
#include "Config.hpp"
#include "SkinConfigFile.hpp"

void CConfig::Setup()
{
	//RAGE STUFF
	SetupValue(GlobalVars.Settings.Aim.Rage.Enabled, false, XorStr("Rage"), XorStr("Enabled"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AlwaysOn, false, XorStr("Rage"), XorStr("AlwaysOn"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AimStep, false, XorStr("Rage"), XorStr("Aimstep"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AutoStop, false, XorStr("Rage"), XorStr("AutoStop"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AutoCrouch, false, XorStr("Rage"), XorStr("AutoCrouch"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AutoFire, false, XorStr("Rage"), XorStr("Autofire"));
	SetupValue(GlobalVars.Settings.Aim.Rage.IsVisible, true, XorStr("Rage"), XorStr("IsVisible"));
	SetupValue(GlobalVars.Settings.Aim.Rage.AutoWall, true, XorStr("Rage"), XorStr("AutoWall"));
	SetupValue(GlobalVars.Settings.Aim.Rage.Silent, true, XorStr("Rage"), XorStr("Silent"));
	SetupValue(GlobalVars.Settings.Aim.Rage.FOVLimit, 180, XorStr("Rage"), XorStr("FOVLimit"));
	SetupValue(GlobalVars.Settings.Aim.Rage.Key, 0x43, XorStr("Rage"), XorStr("Vkey"));
	
	//VISUALS STUFF
	SetupValue(GlobalVars.Settings.Visuals.CLRRendering.Enabled, false, XorStr("Visuals"), XorStr("CLRRenderEnabled"));
	SetupValue(GlobalVars.Settings.Visuals.CLRRendering.TeamColor, 0, XorStr("Visuals"), XorStr("CLRRenderTeamCol"));
	SetupValue(GlobalVars.Settings.Visuals.CLRRendering.EnemyColor, 0, XorStr("Visuals"), XorStr("CLRRenderEnemyCol"));
	SetupValue(GlobalVars.Settings.Visuals.CLRRendering.Team, true, XorStr("Visuals"), XorStr("CLRRenderFilterTeam"));
	SetupValue(GlobalVars.Settings.Visuals.CLRRendering.Enemy, true, XorStr("Visuals"), XorStr("CLRRenderFilterEnemies"));
	SetupValue(GlobalVars.Settings.Visuals.Removals.Enabled, false, XorStr("Visuals"), XorStr("EnableRemovals"));
	SetupValue(GlobalVars.Settings.Visuals.NoVisualRecoil, true, XorStr("Visuals"), XorStr("RemovalsNoVisualRecoil"));
	SetupValue(GlobalVars.Settings.Visuals.Removals.NoFlash, false, XorStr("Visuals"), XorStr("RemovalsNoFlash"));
	SetupValue(GlobalVars.Settings.Visuals.Removals.NoHands, false, XorStr("Visuals"), XorStr("RemovalsNoHandsTV"));
	SetupValue(GlobalVars.Settings.Visuals.Removals.NoWeapon, false, XorStr("Visuals"), XorStr("RemovalsNoGun"));

	//ESP STUFF! NIFTY! 
	//box
	SetupValue(GlobalVars.Enabled.ESP, true, XorStr("ESPs"), XorStr("Enabled"));
	SetupValue(GlobalVars.Settings.ESP.ESPType, 1, XorStr("ESPs"), XorStr("ESPType"));
	SetupValue(GlobalVars.Settings.ESP.HealthBar, false, XorStr("ESPs"), XorStr("HealthBar"));
	SetupValue(GlobalVars.Settings.ESP.HealthText, true, XorStr("ESPs"), XorStr("HealthText"));
	SetupValue(GlobalVars.Settings.ESP.Name, true, XorStr("ESPs"), XorStr("Name"));
	SetupValue(GlobalVars.Settings.ESP.Weapon, true, XorStr("ESPs"), XorStr("Weapon"));
	SetupValue(GlobalVars.Settings.ESP.Team, false, XorStr("ESPs"), XorStr("FilterTeam"));
	SetupValue(GlobalVars.Settings.ESP.Enemy, true, XorStr("ESPs"), XorStr("FilterEnemy"));
	SetupValue(GlobalVars.Settings.ESP.Weapons, false, XorStr("ESPs"), XorStr("FilterWeapons"));
	SetupValue(GlobalVars.Settings.ESP.BombTimer, true, XorStr("ESPs"), XorStr("FilterC4"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPEnemyColor[0], 255.f, XorStr("ESPs"), XorStr("EnemyColR"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPEnemyColor[1], 0.f, XorStr("ESPs"), XorStr("EnemyColG"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPEnemyColor[2], 0.f, XorStr("ESPs"), XorStr("EnemyColB"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPEnemyColor[3], 255.f, XorStr("ESPs"), XorStr("EnemyColA"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPTeamColor[0], 0.f, XorStr("ESPs"), XorStr("TeamColR"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPTeamColor[1], 0.f, XorStr("ESPs"), XorStr("TeamColG"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPTeamColor[2], 255.f, XorStr("ESPs"), XorStr("TeamColB"));
	SetupValue(GlobalVars.Settings.ESP.g_bESPTeamColor[3], 255.f, XorStr("ESPs"), XorStr("TeamColA"));
	//glow
	SetupValue(GlobalVars.Enabled.Glow, true, XorStr("Glow"), XorStr("Enabled"));
	SetupValue(GlobalVars.Settings.ESP.Glow.C4, true, XorStr("Glow"), XorStr("GlowBomb"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Teammates, true, XorStr("Glow"), XorStr("GlowTeam"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Enemies, true, XorStr("Glow"), XorStr("GlowEnemies"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Weapons, true, XorStr("Glow"), XorStr("GlowWeapon"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Enemy.r, 192.f, XorStr("Glow"), XorStr("EnemyColR"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Enemy.g, 160.f, XorStr("Glow"), XorStr("EnemyColG"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Enemy.b, 96.f, XorStr("Glow"), XorStr("EnemyColB"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Enemy.a, 64.f, XorStr("Glow"), XorStr("EnemyColA"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Team.r, 96.f, XorStr("Glow"), XorStr("TeamColR"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Team.g, 128.f, XorStr("Glow"), XorStr("TeamColG"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Team.b, 192.f, XorStr("Glow"), XorStr("TeamColB"));
	SetupValue(GlobalVars.Settings.ESP.Glow.Team.a, 64.f, XorStr("Glow"), XorStr("TeamColA"));

	//LEGIT
	SetupValue(GlobalVars.Settings.Aim.RCS.Enabled, true, XorStr("Legit"), XorStr("RCS"));
	SetupValue(GlobalVars.Settings.Aim.Legit.Misc.AutoPistol, false, XorStr("Legit"), XorStr("AutoPistol"));

	//MISC -> wooooooooo
	SetupValue(GlobalVars.Enabled.AutoAccept, true, XorStr("Misc"), XorStr("AutoAccept"));
	SetupValue(GlobalVars.Enabled.BHop, true, XorStr("Misc"), XorStr("Bunnyhop"));
	SetupValue(GlobalVars.Enabled.ChatSpam, true, XorStr("Misc"), XorStr("ChatSpam"));
	SetupValue(GlobalVars.Settings.Misc.RevealRanks, true, XorStr("Misc"), XorStr("RevealRanks"));
	SetupValue(GlobalVars.Settings.Misc.Airstuck, true, XorStr("Misc"), XorStr("AirStuck"));
	SetupValue(GlobalVars.Settings.Misc.AirStuckKey, 0x56, XorStr("Misc"), XorStr("AirStuckKey"));
	SetupValue(GlobalVars.Settings.Misc.Memewalk, true, XorStr("Misc"), XorStr("Surfing"));
	

}

void CConfig::SetupValue(int &value, int def, std::string category, std::string name)
{
	value = def;
	ints.push_back(new ConfigValue<int>(category, name, &value));
}

void CConfig::SetupValue(float &value, float def, std::string category, std::string name)
{
	value = def;
	floats.push_back(new ConfigValue<float>(category, name, &value));
}

void CConfig::SetupValue(bool &value, bool def, std::string category, std::string name)
{
	value = def;
	bools.push_back(new ConfigValue<bool>(category, name, &value));
}

void CConfig::Save()
{
	SkinsConfig->Save();
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + XorStr("\\kickflipcs\\");
		file = std::string(path) + XorStr("\\kickflipcs\\config.ini");
	}

	CreateDirectory(folder.c_str(), NULL);

	for (auto value : ints)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : floats)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : bools)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), *value->value ? "true" : "false", file.c_str());
}

void CConfig::Load()
{
	SkinsConfig->Load();
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + XorStr("\\kickflipcs\\");
		file = std::string(path) + XorStr("\\kickflipcs\\config.ini");
	}

	CreateDirectory(folder.c_str(), NULL);

	char value_l[32] = { '\0' };

	for (auto value : ints)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atoi(value_l);
	}

	for (auto value : floats)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atof(value_l);
	}

	for (auto value : bools)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = !strcmp(value_l, "true");
	}
}

CConfig* Config = new CConfig();
