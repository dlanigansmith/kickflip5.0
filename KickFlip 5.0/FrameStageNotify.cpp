#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "SkinFunctions.hpp"

void __fastcall Hooks::Hooked_FrameStageNotify(void* ecx, void* edx, SourceEngine::ClientFrameStage_t Stage) 
{
	using namespace SourceEngine;

	QAngle aim_punch_old;
	QAngle view_punch_old;

	QAngle* aim_punch = nullptr;
	QAngle* view_punch = nullptr;

	if (Interfaces::Engine()->IsInGame() && Stage == ClientFrameStage_t::FRAME_RENDER_START && GlobalVars.Settings.Visuals.NoVisualRecoil)
	{
		
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		if (pLocal && pLocal->IsAlive())
		{
			aim_punch = pLocal->AimPunch();
			view_punch = pLocal->ViewPunch();

			aim_punch_old = *aim_punch;
			view_punch_old = *view_punch;

			*aim_punch = QAngle(0, 0, 0);
			*view_punch = QAngle(0, 0, 0);
		}
	}
	

	if (GlobalVars.Enabled.Skins && Stage == ClientFrameStage_t::FRAME_NET_UPDATE_POSTDATAUPDATE_START) 
	{
		SkinFunctions::SetSkinConfig();

		int localPlayerID = Interfaces::Engine()->GetLocalPlayer();
		C_CSPlayer* pLocal = (C_CSPlayer*)Interfaces::EntityList()->GetClientEntity(localPlayerID);

		if (!pLocal || !pLocal->IsAlive())
			return;

		UINT* hWeapons = pLocal->GetWeapons();

		if (!hWeapons)
			return;

		PlayerInfo playerInfo;
		Interfaces::Engine()->GetPlayerInfo(localPlayerID, &playerInfo);

		for (int nIndex = 0; hWeapons[nIndex]; nIndex++) 
		{
			C_BaseCombatWeapon* pWeapon = (C_BaseCombatWeapon*)Interfaces::EntityList()->GetClientEntityFromHandle(hWeapons[nIndex]);

			if (!pWeapon)
				continue;

			int nWeaponIndex = *pWeapon->GetItemDefinitionIndex();

			SkinFunctions::ApplyCustomModel(pLocal, pWeapon, nWeaponIndex);

			// Compare original owner XUIDs.
			if (playerInfo.m_nXuidLow != *pWeapon->GetOriginalOwnerXuidLow())
				continue;

			if (playerInfo.m_nXuidHigh != *pWeapon->GetOriginalOwnerXuidHigh())
				continue;

			SkinFunctions::ApplyCustomSkin(pWeapon, nWeaponIndex);

			// Fix up the account ID so StatTrak will display correctly.
			*pWeapon->GetAccountID() = playerInfo.m_nXuidLow;
		}
	}

	g_fnOriginalFrameStageNotify(ecx, Stage);
	if (aim_punch && view_punch && GlobalVars.Settings.Visuals.NoVisualRecoil)
	{

		*aim_punch = aim_punch_old;
		*view_punch = view_punch_old;
	}
} 