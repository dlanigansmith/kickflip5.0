﻿#pragma once

#include "includes.hpp"
#include "SourceEngine\SDK.hpp"
#include "CSGOStructs.hpp"

#ifndef SKINFUNCTIONS_H
#define SKINFUNCTIONS_H



class SkinFunctions
{
public:
	 
	  static bool ApplyCustomSkin(C_BaseCombatWeapon* pWeapon, int nWeaponIndex) 
	  {
		  // Check if this weapon has a valid override defined.
		  if (GlobalVars.Skins.g_SkinChangerCfg.find(nWeaponIndex) == GlobalVars.Skins.g_SkinChangerCfg.end())
			  return false;

		  // Apply our changes to the fallback variables.
		  *pWeapon->GetFallbackPaintKit() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackPaintKit;
		  *pWeapon->GetEntityQuality() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iEntityQuality;
		  *pWeapon->GetFallbackSeed() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackSeed;
		  *pWeapon->GetFallbackStatTrak() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackStatTrak;
		  *pWeapon->GetFallbackWear() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].flFallbackWear;

		  if (GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iItemDefinitionIndex)
			  *pWeapon->GetItemDefinitionIndex() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iItemDefinitionIndex;

		  // If a name is defined, write it now.
		  if (GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].szCustomName) 
		  {
			  sprintf_s(pWeapon->GetCustomName(), 32, "%s", GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].szCustomName);
		  }

		  // Edit "m_iItemIDHigh" so fallback values will be used.
		  *pWeapon->GetItemIDHigh() = -1;

		  return true;
	  }

	  static bool ApplyCustomModel(C_CSPlayer* pLocal, C_BaseCombatWeapon* pWeapon, int nWeaponIndex) 
	  {
		  C_CBaseViewModel* pViewModel = pLocal->GetViewModel();

		  if (!pViewModel)
			  return false;

		  DWORD hViewModelWeapon = pViewModel->GetWeapon();
		  C_BaseCombatWeapon* pViewModelWeapon = (C_BaseCombatWeapon*)SourceEngine::Interfaces::EntityList()->GetClientEntityFromHandle(hViewModelWeapon);

		  if (pViewModelWeapon != pWeapon)
			  return false;

		  int nViewModelIndex = pViewModel->GetModelIndex();

		  if (GlobalVars.Skins.g_ViewModelCfg.find(nViewModelIndex) == GlobalVars.Skins.g_ViewModelCfg.end())
			  return false;
		
		  pViewModel->SetWeaponModel(GlobalVars.Skins.g_ViewModelCfg[nViewModelIndex], pWeapon);
		  pViewModel->SetModelIndex(SourceEngine::Interfaces::ModelInfo()->GetModelIndex(GlobalVars.Skins.g_ViewModelCfg[nViewModelIndex])); 
		  return true;
	  }

	  static bool ApplyCustomKillIcon(SourceEngine::IGameEvent* pEvent)
	  {
		  // Get the user ID of the attacker.
		  int nUserID = pEvent->GetInt(XorStr("attacker"));

		  if (!nUserID)
			  return false;

		  // Only continue if we were the attacker.
		  if (SourceEngine::Interfaces::Engine()->GetPlayerForUserID(nUserID) != SourceEngine::Interfaces::Engine()->GetLocalPlayer())
			  return false;

		  // Get the original weapon used to kill.
		  const char* szWeapon = pEvent->GetString(XorStr("weapon"));

		  for (auto ReplacementIcon : GlobalVars.Skins.g_KillIconCfg) 
		  {
			  // Search for a valid replacement.
			  if (!strcmp(szWeapon, ReplacementIcon.first)) 
			  {
				  // Replace with user defined value.
				  pEvent->SetString("weapon", ReplacementIcon.second);
				  break;
			  }
		  }

		  return true;
	  }

	 static void SetSkinConfig()
	  {
		  using namespace SourceEngine;
		  // Awp asiimov
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackPaintKit = 279;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].flFallbackWear = 0.9891f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackStatTrak = 1337;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].szCustomName = "&#9730;";

		  // AK47 JetSet
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackPaintKit = 340;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].flFallbackWear = 0.02f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackStatTrak = 65000;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].szCustomName = "&#9730;";

		  // m4a4 des
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackPaintKit = 588;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].flFallbackWear = 0.02f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackStatTrak = 80085;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].szCustomName = "&#9730;";

		  // Negev  | Powerloader
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackPaintKit = 514;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackStatTrak = 6969;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].szCustomName = "&#9730;";
		  
		  //FAMAS After Image 
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackPaintKit = 154;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].szCustomName = "&#9730;";
		  //ssg08 GhostCrusadaer
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackPaintKit = 554;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].szCustomName = "&#9730;";
		  //sg553 Cyrex
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackPaintKit = 487;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].szCustomName = "&#9730;";
		  //AUG HENtia one
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackPaintKit = 455;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].szCustomName = "&#9730;";

		  //Scar20 cyrek
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackPaintKit = 312;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].szCustomName = "&#9730;";
		  //g3sg1 chrosos
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackPaintKit = 438;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].szCustomName = "&#9730;";
		  //lCerberus
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackPaintKit = 379;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].szCustomName = "&#9730;";
                         //Pistols


		  //FIVE-SEVEN All blue
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackPaintKit = 44;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].flFallbackWear = 0.31010684370995;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackSeed = 189;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackStatTrak = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].szCustomName = "&#9730;";
		  
		  // P250 Assimov
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackPaintKit = 551;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].szCustomName = "&#9730;";
		  //tec9 Re-entry
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackPaintKit = 555;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].szCustomName = "&#9730;";
		  //dual berettas Ventilators
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackPaintKit = 544;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].szCustomName = "&#9730;";
		  
		  // Deagle blaze
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackPaintKit = 37;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackStatTrak = 5;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].szCustomName = "&#9730;";

		  // Glock-18 | Fade
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GLOCK].nFallbackPaintKit = 586;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GLOCK].flFallbackWear = 0.02f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GLOCK].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GLOCK].nFallbackStatTrak = 80085;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GLOCK].szCustomName = "&#9730;";

		  // USP-S | Road Rash
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackPaintKit = 504;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackStatTrak = 80085;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].szCustomName = "&#9730;";

		  //CZ-75  The Fuschia Is Now
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackPaintKit = 269;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].szCustomName = "&#9730;";

		  //SMG

		  //p90 Asiimov
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackPaintKit = 359;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].szCustomName = "Jewminator;";
		  

		  //mp7 Nemesis
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackPaintKit = 481;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].szCustomName = "&#9730;";
		  //ump45 Primal tigger
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackPaintKit = 556;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].szCustomName = "&#9730;";
		  //Bizon Judgement of Anubis
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackPaintKit = 542;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].szCustomName = "&#9730;";
		  //mp9 BullDozer
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackPaintKit = 39;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].szCustomName = "&#9730;";
		  //mac10 Neon rider  
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackPaintKit = 433;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].szCustomName = "&#9730;";
		  //SHOTGUNS/heavy

		  //nova hyper Beast
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackPaintKit = 537;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].szCustomName = "&#9730;";

		  //mag7 Heat
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].nFallbackPaintKit = 431;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].szCustomName = "&#9730;";
		  //xm1014 Black tie
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackPaintKit = 557;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].szCustomName = "&#9730;";
		  //sawed-off FuBar
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackPaintKit = 552;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].szCustomName = "&#9730;";
		  //m249
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackPaintKit = 547;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackSeed = 420;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackStatTrak = 444;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].szCustomName = "&#9730;";
		 


		  //CT: Gamma 
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].iItemDefinitionIndex = WEAPON_KNIFE_KARAMBIT;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackPaintKit = 571;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].iEntityQuality = 1;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackSeed = 6;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackStatTrak = 355;
		  //T: M9 Doppler
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].iItemDefinitionIndex = WEAPON_KNIFE_M9_BAYONET;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackPaintKit = 572;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].iEntityQuality = 1;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].flFallbackWear = 0.001f;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackSeed = 648;
		  GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackStatTrak = 420;
	  }
	  

	  static void SetModelConfig() 
	  {
		  using namespace SourceEngine;
		  // Get the indexes of the models we want to replace.
		  int nOriginalKnifeCT = Interfaces::ModelInfo()->GetModelIndex("models/weapons/v_knife_default_ct.mdl");
		  int nOriginalKnifeT = Interfaces::ModelInfo()->GetModelIndex("models/weapons/v_knife_default_t.mdl");
		  
		  // Configure model replacements.
		  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_karam.mdl"; 
		  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_m9_bay.mdl";
		  
	  }

	  static void SetKillIconCfg()
	  {
		  // Define replacement kill icons. (these only apply to you)
		  GlobalVars.Skins.g_KillIconCfg["knife_default_ct"] = "knife_karambit";
		  GlobalVars.Skins.g_KillIconCfg["knife_t"] = "knife_m9bay";
	  }
	
};
#endif // !SKINFUNCTIONS_H





