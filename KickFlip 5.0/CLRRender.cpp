#include "CLRRender.hpp"
struct clr
{
	clr(byte r, byte g, byte b, byte a) : r(r), g(g), b(b), a(a)
	{

	}

	byte r, g, b, a;
};

void CLRRender::CLRRenderTick(int i)
{
	using namespace SourceEngine;
	if (!GlobalVars.Settings.Visuals.CLRRendering.Enabled)
		return;
	auto pLocal = C_CSPlayer::GetLocalPlayer(); //get localplayer
	if (i == Interfaces::Engine()->GetLocalPlayer()) return; //ignore if local player
	auto gEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(i)); //Get ent
	if (!gEntity) return; //check if null
	if (!(gEntity->GetClientClass()->m_ClassID == EClassIds::CCSPlayer))
		return;
	static int clrRender = GET_NETVAR("DT_BaseEntity", "m_clrRender");
	if (!clrRender)
		return;
	if ((gEntity->GetTeamNum()) != (pLocal->GetTeamNum()) && GlobalVars.Settings.Visuals.CLRRendering.Enemy)
	{
	
		switch (GlobalVars.Settings.Visuals.CLRRendering.EnemyColor)
		{
		case 0:
			*(clr*)((DWORD)gEntity + clrRender) = clr(255, 0, 0, 255);
			break;
		case 1:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 255, 0, 255);
			break;
		case 2:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 0, 255, 255);
			break;
		case 3:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 0, 0, 255);
			break;
		case 4:
			*(clr*)((DWORD)gEntity + clrRender) = clr(255, 255, 255, 255);
			break;
		}
	}
	if ((gEntity->GetTeamNum()) == (pLocal->GetTeamNum()) && GlobalVars.Settings.Visuals.CLRRendering.Team)
	{
		
		switch (GlobalVars.Settings.Visuals.CLRRendering.TeamColor)
		{
		case 0:
			*(clr*)((DWORD)gEntity + clrRender) = clr(255, 0, 0, 255);
			break;
		case 1:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 255, 0, 255);
			break;
		case 2:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 0, 255, 255);
			break;
		case 3:
			*(clr*)((DWORD)gEntity + clrRender) = clr(0, 0, 0, 255);
			break;
		case 4:
			*(clr*)((DWORD)gEntity + clrRender) = clr(255, 255, 255, 255);
			break;
		}
		
	}
}