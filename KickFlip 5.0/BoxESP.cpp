#include "BoxESP.h"


void BoxESP::ESPTick(int i)
{
	using namespace SourceEngine;

	if (!GlobalVars.Enabled.ESP) return; //check if esp is on
	
	if (!Interfaces::Engine()->IsInGame()) return; //check if in game

	auto pLocal = C_CSPlayer::GetLocalPlayer(); //get localplayer


	if (i == Interfaces::Engine()->GetLocalPlayer()) return; //ignore if local player
	auto gEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(i)); //Get ent
	if (!gEntity) return; //check if null

	if (!gEntity->IsAlive() || gEntity->IsDormant()) return;  //dormant/alive checks
	if (gEntity->GetClientClass()->m_ClassID == EClassIds::CCSPlayer) //get classid make sure its a playa
	{
		//Hey! We want to draw these guys!
		//Sweet! 
		//LETS DO IT!
		Vector vecMax = gEntity->GetCollideable()->OBBMaxs();
		Vector vecPos, vecPos3D;
		Vector vecTop, vecTop3D;
		vecPos3D = gEntity->GetOrigin();
		vecTop3D = vecPos3D + Vector(0, 0, vecMax.z);

		if (!Utils::WorldToScreen(vecPos3D, vecPos) || !Utils::WorldToScreen(vecTop3D, vecTop)) return;
		int iMiddle = (vecPos.y - vecTop.y) + 4;
		int iWidth = iMiddle / 3.5;
		int iCornerWidth = iWidth / 4;

		static Color colESP;
		if (gEntity->GetTeamNum() != pLocal->GetTeamNum())
			colESP = Color(GlobalVars.Settings.ESP.g_bESPEnemyColor[0], GlobalVars.Settings.ESP.g_bESPEnemyColor[1], GlobalVars.Settings.ESP.g_bESPEnemyColor[2], GlobalVars.Settings.ESP.g_bESPEnemyColor[3]);
		else
			colESP = Color(GlobalVars.Settings.ESP.g_bESPTeamColor[0], GlobalVars.Settings.ESP.g_bESPTeamColor[1], GlobalVars.Settings.ESP.g_bESPTeamColor[2], GlobalVars.Settings.ESP.g_bESPTeamColor[3]);
		if ((gEntity->GetTeamNum() == pLocal->GetTeamNum()) && !GlobalVars.Settings.ESP.Team) return;

		if (GlobalVars.Settings.ESP.ESPType == 0) //BOX
		{
			Draw::DrawOutlinedRect(vecTop.x - iWidth, vecTop.y, iWidth * 2, iMiddle, Color(0, 0, 0));
			Draw::DrawOutlinedRect(vecTop.x - iWidth + 1, vecTop.y + 1, iWidth * 2 - 2, iMiddle - 2, colESP);
			Draw::DrawOutlinedRect(vecTop.x - iWidth + 2, vecTop.y + 2, iWidth * 2 - 4, iMiddle - 4, Color(0, 0, 0));
		}
		if (GlobalVars.Settings.ESP.ESPType == 1) //CORNER
		{
			Draw::DrawCornerEsp(vecTop.x - (iWidth + 3), vecTop.y, iWidth * 2, iMiddle, colESP);
		}
		if (GlobalVars.Settings.ESP.ESPType == 2) //BONE
		{
			studiohdr_t* pStudioModel = Interfaces::ModelInfo()->GetStudioModel(gEntity->GetModel());
			if (pStudioModel)
			{
				static matrix3x4_t pBoneToWorldOut[128];
				if (gEntity->SetupBones(pBoneToWorldOut, 128, 256, Interfaces::GlobalVars()->curtime))
				{
					for (int i = 0; i < pStudioModel->numbones; i++)
					{
						mstudiobone_t* pBone = pStudioModel->pBone(i);
						if (!pBone || !(pBone->flags & 256) || pBone->parent == -1)
							continue;

						Vector vBonePos1;
						if (!Utils::WorldToScreen(Vector(pBoneToWorldOut[i][0][3], pBoneToWorldOut[i][1][3], pBoneToWorldOut[i][2][3]), vBonePos1))
							continue;

						Vector vBonePos2;
						if (!Utils::WorldToScreen(Vector(pBoneToWorldOut[pBone->parent][0][3], pBoneToWorldOut[pBone->parent][1][3], pBoneToWorldOut[pBone->parent][2][3]), vBonePos2))
							continue;

						Draw::DrawLine((int)vBonePos1.x, (int)vBonePos1.y, (int)vBonePos2.x, (int)vBonePos2.y, colESP);
					}
				}
			}
		}


		int iClampedHealth = gEntity->GetHealth();
		if (iClampedHealth >= 101)
			iClampedHealth = 100;

		int iHealthG = iClampedHealth * 2.55;
		int iHealthR = 255 - iHealthG;

		Color colHealth = Color(iHealthR, iHealthG, 0, 255);
		if (GlobalVars.Settings.ESP.HealthBar)
		{
			Draw::DrawRect(vecTop.x + iWidth + 3, vecTop.y + 1, iWidth / 8, iMiddle * 0.01 * iClampedHealth - 2, colHealth);
			Draw::DrawOutlinedRect(vecTop.x + iWidth + 2, vecTop.y, (iWidth / 8) + 2, iMiddle, Color(0, 0, 0, 255));
		}

		if (GlobalVars.Settings.ESP.HealthText)
		{
			Draw::DrawString(5, vecTop.x + iWidth + (iWidth / 8) + 8, vecTop.y + 16, colHealth, false, "%i", iClampedHealth);
		}
		if (GlobalVars.Settings.ESP.Name)
		{
			PlayerInfo pInfo;
			Interfaces::Engine()->GetPlayerInfo(gEntity->EntIndex(), &pInfo);
			Draw::DrawString(5, vecTop.x + iWidth + (iWidth / 8) + 8, vecTop.y + 6, colESP, false, "%s", pInfo.m_szPlayerName);
		}
		if (GlobalVars.Settings.ESP.Weapon)
		{
			//weapon class crashes game
			auto wEntity = reinterpret_cast<C_BaseCombatWeapon*>(gEntity->GetWeapon());
			if (!wEntity)
				return;
			
			std::string Name = wEntity->GetWeaponName(); //REALLY BIG HACK BUT IT WORKS
			std::wstring wName(Name.begin(), Name.end());
			Draw::DrawStringUnicode(5, vecTop.x + iWidth + (iWidth / 8), vecTop.y + 26, colESP, false, wName.c_str());
		}
	}
	

}
