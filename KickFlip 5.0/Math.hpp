#pragma once
#include "includes.hpp"
#include "SourceEngine\SDK.hpp"

#include "Utilities.hpp"
#define square( x ) ( x * x )
enum
{
	PITCH = 0,	// up / down
	YAW,		// left / right
	ROLL		// fall over
};
#ifndef MATH_H
#define MATH_H
namespace M
{
	
	extern inline void SinCos(float radians, float *sine, float *cosine);
	extern void VectorAngles(const SourceEngine::Vector& forward, SourceEngine::QAngle &angles);
	extern void AngleVectors(const SourceEngine::QAngle& angles, SourceEngine::Vector *forward);
	extern void AngleVectors(const SourceEngine::QAngle &angles, SourceEngine::Vector *forward, SourceEngine::Vector *right, SourceEngine::Vector *up);
	extern SourceEngine::QAngle CalcAngle(SourceEngine::Vector v1, SourceEngine::Vector v2);
	extern float GetFov(const SourceEngine::QAngle& viewAngle, const SourceEngine::QAngle& aimAngle);
	extern float VectorDistance(SourceEngine::Vector v1, SourceEngine::Vector v2);

	extern float GetDelta(float hspeed, float maxspeed, float airaccelerate);
	extern SourceEngine::Vector ExtrapolateTick(SourceEngine::Vector p0, SourceEngine::Vector v0);

	extern inline float RandFloat(float M, float N);

	double inline __declspec (naked) __fastcall FASTSQRT(double n)
	{
		_asm fld qword ptr[esp + 4]
		_asm fsqrt
		_asm ret 8
	}

	inline float sseSqrt(float x)
	{
		float root = 0.0f;

		__asm sqrtss xmm0, x
		__asm movss root, xmm0

		return root;
	}
}
#endif