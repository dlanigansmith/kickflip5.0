﻿#pragma once

#include "includes.hpp"
#include "SourceEngine\SDK.hpp"
#include "CSGOStructs.hpp"

#ifndef SKINFUNCTIONS_H
#define SKINFUNCTIONS_H

struct StickerInfo
{
	int iStickerID;
	float fWearProgress;
	float fPatternScale;
	float fPatternRotation;
}; 

class SkinFunctions
{
public:
	  static void GenerateStickerMaterials()
	  {

	  }
	  static bool ApplyCustomSkin(C_BaseCombatWeapon* pWeapon, int nWeaponIndex) 
	  {
		  // Check if this weapon has a valid override defined.
		  if (GlobalVars.Skins.g_SkinChangerCfg.find(nWeaponIndex) == GlobalVars.Skins.g_SkinChangerCfg.end())
			  return false;

		  // Apply our changes to the fallback variables.
		  *pWeapon->GetFallbackPaintKit() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackPaintKit;
		  *pWeapon->GetEntityQuality() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iEntityQuality;
		  *pWeapon->GetFallbackSeed() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackSeed;
		  if(GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackStatTrak > 0)
			*pWeapon->GetFallbackStatTrak() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].nFallbackStatTrak;
		  *pWeapon->GetFallbackWear() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].flFallbackWear;

		  if (GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iItemDefinitionIndex)
			  *pWeapon->GetItemDefinitionIndex() = GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].iItemDefinitionIndex;

		  // If a name is defined, write it now
		
		  if (GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].szCustomName != '\0') 
		  {
			  sprintf_s(pWeapon->GetCustomName(), 32, "%s", GlobalVars.Skins.g_SkinChangerCfg[nWeaponIndex].szCustomName);
		  }

		  // Edit "m_iItemIDHigh" so fallback values will be used.
		  *pWeapon->GetItemIDHigh() = -1;

		  return true;
	  }

	  static bool ApplyCustomModel(C_CSPlayer* pLocal, C_BaseCombatWeapon* pWeapon, int nWeaponIndex) 
	  {
		  C_CBaseViewModel* pViewModel = pLocal->GetViewModel();

		  if (!pViewModel)
			  return false;

		  DWORD hViewModelWeapon = pViewModel->GetWeapon();
		  C_BaseCombatWeapon* pViewModelWeapon = (C_BaseCombatWeapon*)SourceEngine::Interfaces::EntityList()->GetClientEntityFromHandle(hViewModelWeapon);

		  if (pViewModelWeapon != pWeapon)
			  return false;

		  int nViewModelIndex = pViewModel->GetModelIndex();

		  if (GlobalVars.Skins.g_ViewModelCfg.find(nViewModelIndex) == GlobalVars.Skins.g_ViewModelCfg.end())
			  return false;
		
		  pViewModel->SetWeaponModel(GlobalVars.Skins.g_ViewModelCfg[nViewModelIndex], pWeapon);
		  pViewModel->SetModelIndex(SourceEngine::Interfaces::ModelInfo()->GetModelIndex(GlobalVars.Skins.g_ViewModelCfg[nViewModelIndex])); 
		  return true;
	  }

	  static bool ApplyCustomKillIcon(SourceEngine::IGameEvent* pEvent)
	  {
		  // Get the user ID of the attacker.
		  int nUserID = pEvent->GetInt(XorStr("attacker"));

		  if (!nUserID)
			  return false;

		  // Only continue if we were the attacker.
		  if (SourceEngine::Interfaces::Engine()->GetPlayerForUserID(nUserID) != SourceEngine::Interfaces::Engine()->GetLocalPlayer())
			  return false;

		  // Get the original weapon used to kill.
		  const char* szWeapon = pEvent->GetString(XorStr("weapon"));

		  for (auto ReplacementIcon : GlobalVars.Skins.g_KillIconCfg) 
		  {
			  // Search for a valid replacement.
			  if (!strcmp(szWeapon, ReplacementIcon.first)) 
			  {
				  // Replace with user defined value.
				  pEvent->SetString("weapon", ReplacementIcon.second);
				  break;
			  }
		  }

		  return true;
	  }

	 static void SetSkinConfig()
	  {
		 
	  } 
	  

	  static void SetModelConfig() 
	  {
		  using namespace SourceEngine;
		  // Get the indexes of the models we want to replace.
		  int nOriginalKnifeCT = Interfaces::ModelInfo()->GetModelIndex(XorStr("models/weapons/v_knife_default_ct.mdl"));
		  int nOriginalKnifeT = Interfaces::ModelInfo()->GetModelIndex(XorStr("models/weapons/v_knife_default_t.mdl"));
		  // Configure model replacements.
		  switch (GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].iItemDefinitionIndex) //ENCRYPT THESE
		  {
		  case WEAPON_KNIFE_BAYONET:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_bayonet.mdl"; break;
		  case WEAPON_KNIFE_FLIP:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_flip.mdl"; break;
		  case WEAPON_KNIFE_GUT:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_gut.mdl"; break;
		  case WEAPON_KNIFE_KARAMBIT:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_karam.mdl"; break;
		  case WEAPON_KNIFE_M9_BAYONET:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_m9_bay.mdl"; break;
		  case WEAPON_KNIFE_TACTICAL:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_tactical.mdl"; break;
		  case WEAPON_KNIFE_FALCHION:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_falchion_advanced.mdl"; break;
		  case WEAPON_KNIFE_SURVIVAL_BOWIE:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_survival_bowie.mdl"; break;
		  case WEAPON_KNIFE_BUTTERFLY:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_butterfly.mdl"; break;
		  case WEAPON_KNIFE_PUSH:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeCT] = "models/weapons/v_knife_push.mdl"; break;
		  default:
			  break;
			
		  }
		  switch (GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].iItemDefinitionIndex)
		  {
		  case WEAPON_KNIFE_BAYONET:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_bayonet.mdl"; break;
		  case WEAPON_KNIFE_FLIP:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_flip.mdl"; break;
		  case WEAPON_KNIFE_GUT:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_gut.mdl"; break;
		  case WEAPON_KNIFE_KARAMBIT:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_karam.mdl"; break;
		  case WEAPON_KNIFE_M9_BAYONET:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_m9_bay.mdl"; break;
		  case WEAPON_KNIFE_TACTICAL:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_taTical.mdl"; break;
		  case WEAPON_KNIFE_FALCHION:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_falchion_advanced.mdl"; break;
		  case WEAPON_KNIFE_SURVIVAL_BOWIE:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_survival_bowie.mdl"; break;
		  case WEAPON_KNIFE_BUTTERFLY:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_butterfly.mdl"; break;
		  case WEAPON_KNIFE_PUSH:
			  GlobalVars.Skins.g_ViewModelCfg[nOriginalKnifeT] = "models/weapons/v_knife_push.mdl"; break;
		  default:
			  break;

		  }
	  }

	  static void SetKillIconCfg()
	  {
		  // Define replacement kill icons. (these only apply to you)
		  GlobalVars.Skins.g_KillIconCfg["knife_default_ct"] = "knife_m9_bayonet";
		  GlobalVars.Skins.g_KillIconCfg["knife_t"] = "knife_karambit";
	  }
	
};
#endif // !SKINFUNCTIONS_H


 


