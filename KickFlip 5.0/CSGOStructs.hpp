#pragma once

#include "includes.hpp"
#include "Math.hpp"
#include "SourceEngine/SDK.hpp"
#include "NetvarManager.hpp"
#include "VFTTable.hpp"
#include "Utilities.hpp"

typedef __int16					int16;
typedef unsigned __int16		uint16;
typedef __int32					int32;
typedef unsigned __int32		uint32;
typedef __int64					int64;
typedef unsigned __int64		uint64;
inline float BitsToFloat(uint32 i)
{
	union Convertor_t
	{
		float f;
		unsigned long ul;
	}tmp;
	tmp.ul = i;
	return tmp.f;
}

#define FLOAT32_NAN_BITS     (uint32)0x7FC00000	// not a number!
#define FLOAT32_NAN          BitsToFloat( FLOAT32_NAN_BITS )

#define VEC_T_NAN FLOAT32_NAN
#define CHECK_VALID( _v ) 0
#define Assert( _exp ) ((void)0)

#define DECL_ALIGN(x)			__declspec( align( x ) )

#define ALIGN4 DECL_ALIGN(4)
#define ALIGN8 DECL_ALIGN(8)
#define ALIGN16 DECL_ALIGN(16)
#define ALIGN32 DECL_ALIGN(32)
#define ALIGN128 DECL_ALIGN(128)
#define	HITGROUP_GENERIC	0
#define	HITGROUP_HEAD		1
#define	HITGROUP_CHEST		2
#define	HITGROUP_STOMACH	3
#define HITGROUP_LEFTARM	4	
#define HITGROUP_RIGHTARM	5
#define HITGROUP_LEFTLEG	6
#define HITGROUP_RIGHTLEG	7
#define HITGROUP_GEAR		10	

class WeaponInfo_t
{
public:
	char pad_0x0000[99];      //0x0000
	char m_name[80];          //0x00
	char pad_0x0800[0x74D];   //0x0000
	float m_flArmorRatio;       //0x0800
	float unkwn1;               //0x0804
	float unkwn2;               //0x0808
	__int32 unkwn3;             //0x080C
	__int32 unkwn4;             //0x0810
	float m_flPenetration;      //0x0814
	__int32 m_iDamage;          //0x0818
	float m_flRange;            //0x081C
	float m_flRangeModifier;    //0x0820
};




class C_CSPlayer;
class C_BaseCombatWeapon;

class C_BaseCombatWeapon : public SourceEngine::IClientEntity 
{
	template<class T>
	inline T GetFieldValue(int offset) {
		return *(T*)((DWORD)this + offset);
	}
	template<class T>
	T* GetFieldPointer(int offset) {
		return (T*)((DWORD)this + offset);
	}
public:
	C_CSPlayer* GetOwner()
	{
		using namespace SourceEngine;
		static int m_hOwnerEntity = GET_NETVAR(XorStr("DT_BaseEntity"), XorStr("m_hOwnerEntity"));
		return (C_CSPlayer*)Interfaces::EntityList()->GetClientEntityFromHandle(GetFieldValue<CHandle<C_CSPlayer>>(m_hOwnerEntity));
	}
	float NextPrimaryAttack() {
		static int m_flNextPrimaryAttack = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("LocalActiveWeaponData"), XorStr("m_flNextPrimaryAttack"));
		return GetFieldValue<float>(m_flNextPrimaryAttack);
	}
	int GetId() {
		typedef int(__thiscall* tGetId)(void*);
		return SourceEngine::CallVFunction<tGetId>(this, 458)(this);
	}
	const char* GetName() {
		typedef const char* (__thiscall* tGetName)(void*);
		return SourceEngine::CallVFunction<tGetName>(this, 378)(this);
	}
	int* GetViewModelIndex() {
		static int m_iViewModelIndex = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iViewModelIndex"));
		return GetFieldPointer<int>(m_iViewModelIndex);
	}
	int* GetWorldModelIndex() {
		static int m_iWorldModelIndex = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iWorldModelIndex "));
		return GetFieldPointer<int>(m_iWorldModelIndex);
	}
	int* GetItemDefinitionIndex() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_iItemDefinitionIndex
		static int m_iItemDefinitionIndex = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_AttributeManager"), XorStr("m_Item"), XorStr("m_iItemDefinitionIndex"));
		return GetFieldPointer<int>(m_iItemDefinitionIndex);
	}
	int* GetItemIDHigh() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_iItemIDHigh
		static int m_iItemIDHigh = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_AttributeManager"), XorStr("m_Item"), XorStr("m_iItemIDHigh"));
		return GetFieldPointer<int>(m_iItemIDHigh);
	}
	int* GetAccountID() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_iAccountID
		static int m_iAccountID = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_AttributeManager"), XorStr("m_Item"), XorStr("m_iAccountID"));
		return GetFieldPointer<int>(m_iAccountID);
	}
	int* GetEntityQuality() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_iEntityQuality
		static int m_iEntityQuality = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_AttributeManager"), XorStr("m_Item"), XorStr("m_iEntityQuality"));
		return GetFieldPointer<int>(m_iEntityQuality);

	}
	char* GetCustomName() {
		// DT_BaseAttributableItem -> m_AttributeManager -> m_Item -> m_szCustomName
		static int m_szCustomName = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_AttributeManager"), XorStr("m_Item"), XorStr("m_szCustomName"));
		return GetFieldPointer<char>(m_szCustomName);

	}
	int* GetOriginalOwnerXuidLow() {
		// DT_BaseAttributableItem -> m_OriginalOwnerXuidLow
		static int m_OriginalOwnerXuidLow = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_OriginalOwnerXuidLow"));
		return GetFieldPointer<int>(m_OriginalOwnerXuidLow);

	}
	int* GetOriginalOwnerXuidHigh() {
		// DT_BaseAttributableItem -> m_OriginalOwnerXuidHigh
		static int m_OriginalOwnerXuidHigh = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_OriginalOwnerXuidHigh"));
		return GetFieldPointer<int>(m_OriginalOwnerXuidHigh);

	}
	int* GetFallbackPaintKit() {
		// DT_BaseAttributableItem -> m_nFallbackPaintKit
		static int m_nFallbackPaintKit = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_nFallbackPaintKit"));
		return GetFieldPointer<int>(m_nFallbackPaintKit);

	}
	int* GetFallbackSeed() {
		// DT_BaseAttributableItem -> m_nFallbackSeed
		static int m_nFallbackSeed = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_nFallbackSeed"));
		return GetFieldPointer<int>(m_nFallbackSeed);

	}
	float* GetFallbackWear() {
		// DT_BaseAttributableItem -> m_flFallbackWear
		static int m_flFallbackWear = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_flFallbackWear"));
		return GetFieldPointer<float>(m_flFallbackWear);

	}
	int* GetFallbackStatTrak() {
		// DT_BaseAttributableItem -> m_nFallbackStatTrak
		static int m_nFallbackStatTrak = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_nFallbackStatTrak"));
		return GetFieldPointer<int>(m_nFallbackStatTrak);

	}
	float* GetAccuracyPenalty()
	{
		static int m_fAccuracyPenalty = GET_NETVAR(XorStr("DT_WeaponCSBase"), XorStr("m_fAccuracyPenalty"));
		return GetFieldPointer<float>(m_fAccuracyPenalty); 
	}
	/* TODO MAKE THIS WORK 
	float* GetReloading()
	{
		static int m_fAccuracyPenalty = GET_NETVAR(XorStr("DT_WeaponCSBase"), XorStr("m_fAccuracyPenalty"));
		return GetFieldPointer<float>(m_fAccuracyPenalty);
	} */

	
	WeaponInfo_t* GetCSWeaponData()
	{
		if (!this) return nullptr;

		typedef WeaponInfo_t*(__thiscall* OriginalFn)(void*);
		return SourceEngine::CallVFunction<OriginalFn>(this, 456)(this);
	}
	bool IsEmpty() 
	{
		static int m_iClip = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iClip1"));
		return GetFieldValue<int>(m_iClip) == 0;
	}
	bool IsAutoPistolWorthy()
	{
		if (!this)
			return false;

		int id = this->GetId();
		using namespace SourceEngine;
		switch (id)
		{
		case WEAPON_DEAGLE:
		case WEAPON_ELITE:
		case WEAPON_FIVESEVEN:
		case WEAPON_GLOCK:
		case WEAPON_TEC9:
		case WEAPON_HKP2000:
		case WEAPON_P250:
			return true;
		default:
			return false;
		}
	}
	bool IsGun()
	{
		if (!this)
			return false;

		int id = this->GetId();
		using namespace SourceEngine;
		switch (id)
		{
		case WEAPON_DEAGLE:
		case WEAPON_ELITE:
		case WEAPON_FIVESEVEN:
		case WEAPON_GLOCK:
		case WEAPON_AK47:
		case WEAPON_AUG:
		case WEAPON_AWP:
		case WEAPON_FAMAS:
		case WEAPON_G3SG1:
		case WEAPON_GALILAR:
		case WEAPON_M249:
		case WEAPON_M4A1:
		case WEAPON_MAC10:
		case WEAPON_P90:
		case WEAPON_UMP45:
		case WEAPON_XM1014:
		case WEAPON_BIZON:
		case WEAPON_MAG7:
		case WEAPON_NEGEV:
		case WEAPON_SAWEDOFF:
		case WEAPON_TEC9:
			return true;
		case WEAPON_TASER:
			return false;
		case WEAPON_HKP2000:
		case WEAPON_MP7:
		case WEAPON_MP9:
		case WEAPON_NOVA:
		case WEAPON_P250:
		case WEAPON_SCAR20:
		case WEAPON_SG556:
		case WEAPON_SSG08:
			return true;
		case WEAPON_KNIFE:
		case WEAPON_FLASHBANG:
		case WEAPON_HEGRENADE:
		case WEAPON_SMOKEGRENADE:
		case WEAPON_MOLOTOV:
		case WEAPON_DECOY:
		case WEAPON_INCGRENADE:
		case WEAPON_C4:
		case WEAPON_KNIFE_T:
			return false;
		case WEAPON_M4A1_SILENCER:
		case WEAPON_USP_SILENCER:
		case WEAPON_CZ75A:
		case WEAPON_REVOLVER:
			return true;
		default:
			return false;
		}
	}
	std::string GetWeaponName()
	{
		if (!this)
			return "";

		int id = this->GetId();
		using namespace SourceEngine;
		switch (id)
		{
		case WEAPON_DEAGLE:
			return XorStr("Desert Eagle");
		case WEAPON_ELITE:
			return XorStr("Dual Berettas");
		case WEAPON_FIVESEVEN:
			return XorStr("Five-SeveN");
		case WEAPON_GLOCK:
			return XorStr("Glock-18");
		case WEAPON_AK47:
			return XorStr("AK-47");
		case WEAPON_AUG:
			return XorStr("AUG");
		case WEAPON_AWP:
			return XorStr("AWP");
		case WEAPON_FAMAS:
			return XorStr("FAMAS");
		case WEAPON_G3SG1:
			return XorStr("G3SG1");
		case WEAPON_GALILAR:
			return XorStr("Galil");
		case WEAPON_M249:
			return XorStr("M249");
		case WEAPON_M4A1:
			return XorStr("M4A1");
		case WEAPON_MAC10:
			return XorStr("MAC-10");
		case WEAPON_P90:
			return XorStr("P90");
		case WEAPON_UMP45:
			return XorStr("UMP-45");
		case WEAPON_XM1014:
			return XorStr("XM1014");
		case WEAPON_BIZON:
			return XorStr("PP-Bizon");
		case WEAPON_MAG7:
			return XorStr("MAG-7");
		case WEAPON_NEGEV:
			return XorStr("Negev");
		case WEAPON_SAWEDOFF:
			return XorStr("Sawed-Off");
		case WEAPON_TEC9:
			return XorStr("Tec-9");
		case WEAPON_TASER:
			return XorStr("Taser");
		case WEAPON_HKP2000:
			return XorStr("P2000");
		case WEAPON_MP7:
			return XorStr("MP7");
		case WEAPON_MP9:
			return XorStr("MP9");
		case WEAPON_NOVA:
			return XorStr("Nova");
		case WEAPON_P250:
			return XorStr("P250");
		case WEAPON_SCAR20:
			return XorStr("SCAR-20");
		case WEAPON_SG556:
			return XorStr("SG 553");
		case WEAPON_SSG08:
			return XorStr("SSG 08");
		case WEAPON_KNIFE:
			return XorStr("Knife");
		case WEAPON_FLASHBANG:
			return XorStr("Flashbang");
		case WEAPON_HEGRENADE:
			return XorStr("HE Grenade");
		case WEAPON_SMOKEGRENADE:
			return XorStr("Smoke Grenade");
		case WEAPON_MOLOTOV:
			return XorStr("Molotov");
		case WEAPON_DECOY:
			return XorStr("Decoy");
		case WEAPON_INCGRENADE:
			return XorStr("Incendiary Grenade");
		case WEAPON_C4:
			return XorStr("C4");
		case WEAPON_KNIFE_T:
			return XorStr("Knife");
		case WEAPON_M4A1_SILENCER:
			return XorStr("M4A1-S");
		case WEAPON_USP_SILENCER:
			return XorStr("USP-S");
		case WEAPON_CZ75A:
			return XorStr("CZ75-Auto");
		case WEAPON_REVOLVER:
			return XorStr("R8 Revolver");
		default:
			return XorStr("Knife");
		}

		return "";
	} 

};

class C_CBaseViewModel : public SourceEngine::IClientEntity 
{
	template<class T>
	inline T GetFieldValue(int offset) {
		return *(T*)((DWORD)this + offset);
	}

public:
	int GetModelIndex() {
		static int m_nModelIndex = GET_NETVAR(XorStr("DT_BaseViewModel"), XorStr("m_nModelIndex"));
		return GetFieldValue<int>(m_nModelIndex);
	}
	void SetModelIndex(int index)
	{
		static int m_nModelIndex = GET_NETVAR(XorStr("DT_BaseViewModel"), XorStr("m_nModelIndex"));
		*(int*)(DWORD)(this + m_nModelIndex) = index; 
	}
	DWORD GetOwner() {
		static int m_hOwner = GET_NETVAR(XorStr("DT_BaseViewModel"), XorStr("m_hOwner"));
		return GetFieldValue<DWORD>(m_hOwner);
	}
	DWORD GetWeapon() {
		static int m_hWeapon = GET_NETVAR(XorStr("DT_BaseViewModel"), XorStr("m_hWeapon"));
		return GetFieldValue<DWORD>(m_hWeapon);
	}
	void SetWeaponModel(const char* Filename, IClientEntity* Weapon) {
		using namespace SourceEngine;
		typedef void(__thiscall* tSetWeaponModel)(void*, const char*, IClientEntity*);
		return CallVFunction<tSetWeaponModel>(this, 242)(this, Filename, Weapon);
	}
};

class C_CSPlayer : public SourceEngine::IClientEntity {
	template<class T>
	inline T GetFieldValue(int offset) {
		return *(T*)((DWORD)this + offset);
	}
	template<class T>
	T* GetFieldPointer(int offset) {
		return (T*)((DWORD)this + offset);
	}
public:
	bool m_visible = false;
	static C_CSPlayer* GetLocalPlayer() {
		return (C_CSPlayer*)SourceEngine::Interfaces::EntityList()->GetClientEntity(SourceEngine::Interfaces::Engine()->GetLocalPlayer());
	}

	SourceEngine::Vector GetPredicted(SourceEngine::Vector p0)
	{
		
		SourceEngine::Vector Tick = p0 + (this->GetVelocity() * SourceEngine::Interfaces::GlobalVars()->interval_per_tick); 
		return Tick;
	}
	
	char* GetLastPlaceName()
	{
		static int m_szLastPlaceName = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_szLastPlaceName"));
		
		return GetFieldPointer<char>(m_szLastPlaceName);
	}

	
	SourceEngine::Vector GetVelocity()
	{
		static int m_vecVelocity = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("localdata"), XorStr("m_vecVelocity[0]"));
		return GetFieldValue<SourceEngine::Vector>(m_vecVelocity); 
	}
	int GetHealth() {
		static int m_iHealth = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_iHealth"));
		return GetFieldValue<int>(m_iHealth);
	}
	bool IsAlive() {
		static int m_lifeState = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_lifeState"));
		return GetFieldValue<int>(m_lifeState) == 0;
	}
	bool GetImmune()
	{
		static int m_bGunGameImmunity = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_bGunGameImmunity"));
		
		return GetFieldValue<bool>(m_bGunGameImmunity) == 0;
	}
	bool IsVisible(SourceEngine::ECSPlayerBones bone)
	{
		
		SourceEngine::Ray_t ray;
		SourceEngine::trace_t tr;
		m_visible = false;
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		
		ray.Init(pLocal->GetEyePos(), Utils::GetEntityBone(this, bone)); 
		
		SourceEngine::CTraceFilter filter;
		filter.pSkip = pLocal;
		
		SourceEngine::Interfaces::EngineTrace()->TraceRay(ray, MASK_SHOT, &filter, &tr);

		if ((tr.m_pEnt) == this)
		{
			m_visible = true;
			return true;
		}

		return false;
	}
	bool IsScoped() 
	{
		static int m_bIsScoped = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_bIsScoped"));
		return GetFieldValue<int>(m_bIsScoped) == 0;
	}
	int GetTeamNum() {
		static int m_iTeamNum = GET_NETVAR(XorStr("DT_BaseEntity"), XorStr("m_iTeamNum"));
		return GetFieldValue<int>(m_iTeamNum);
	}
	SourceEngine::ClientClass* GetClientClass()
	{
		using namespace SourceEngine;
		PVOID pNetworkable = (PVOID)((DWORD)(this) + 0x8);
		typedef ClientClass*(__thiscall* NetWorkFunc)(PVOID);
		
		std::unique_ptr<VFTableHook> ClientClassHook = nullptr;
		
		ClientClassHook = std::make_unique<VFTableHook>((PPDWORD)pNetworkable, true);
		return ClientClassHook->GetOriginal<NetWorkFunc>(2)(pNetworkable);
		
	}
	int GetTickBase() {
		static int m_nTickBase = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("localdata"), XorStr("m_nTickBase"));
		return GetFieldValue<int>(m_nTickBase); //THIS IS CRASHING 
	}
	int GetFlags() 
	{
		static int m_fFlags = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_fFlags"));
		return GetFieldValue<int>(m_fFlags);
	}
	BYTE GetLifeState() {
		static int m_lifeState = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_lifeState"));
		return GetFieldValue<BYTE>(m_lifeState);
	}
	UINT* GetWeapons() {
		static int m_hMyWeapons = GET_NETVAR(XorStr("DT_BaseCombatCharacter"), XorStr("m_hMyWeapons"));
		return GetFieldPointer<UINT>(m_hMyWeapons);
	}
	SourceEngine::Vector GetViewOffset() {
		static int m_vecViewOffset = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("localdata"), XorStr("m_vecViewOffset[0]"));
		return GetFieldValue<SourceEngine::Vector>(m_vecViewOffset);
	}
	SourceEngine::Vector GetOrigin() {
		static int m_vecOrigin = GET_NETVAR(XorStr("DT_BaseEntity"), XorStr("m_vecOrigin"));
		return GetFieldValue<SourceEngine::Vector>(m_vecOrigin);
	}
	SourceEngine::Vector GetEyePos()
	{
		
		return GetOrigin() + GetViewOffset();
	}
	SourceEngine::Vector* ViewPunch() {
		static int m_viewPunchAngle = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("localdata"), XorStr("m_Local"), XorStr("m_viewPunchAngle"));
		return GetFieldPointer<SourceEngine::Vector>(m_viewPunchAngle);
	}
	SourceEngine::Vector* AimPunch() {
		static int m_aimPunchAngle = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("localdata"), XorStr("m_Local"), XorStr("m_aimPunchAngle"));
		return GetFieldPointer<SourceEngine::Vector>(m_aimPunchAngle);
	}
	C_CBaseViewModel* GetViewModel() {
		using namespace SourceEngine;
		static int m_hViewModel = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_hViewModel[0]"));
		return (C_CBaseViewModel*)Interfaces::EntityList()->GetClientEntityFromHandle(GetFieldValue< CHandle<IClientEntity> >(m_hViewModel));
	}
	C_BaseCombatWeapon* GetWeapon() {
		using namespace SourceEngine;
		static int m_hActiveWeapon = GET_NETVAR(XorStr("DT_BaseCombatCharacter"), XorStr("m_hActiveWeapon"));
		return (C_BaseCombatWeapon*)Interfaces::EntityList()->GetClientEntityFromHandle(GetFieldValue< CHandle<IClientEntity> >(m_hActiveWeapon));
	}
	int GetMoveType()
	{
		return GetFieldValue<int>(0x258); //MOVETYPE OFFSET
	}
};