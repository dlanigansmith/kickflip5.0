#pragma once
#include "includes.hpp"
#ifndef CFG
#define CFG
template <typename T>
class SConfigItem
{
	std::string category, name;
	T* value;
public:
	SConfigItem(std::string category, std::string name, T* value)
	{
		this->category = category;
		this->name = name;
		this->value = value;
	}
};

template <typename T>
class SConfigValue
{
public:
	SConfigValue(std::string category_, std::string name_, T* value_)
	{
		category = category_;
		name = name_;
		value = value_;
	}

	std::string category, name;
	T* value;
};

class SCConfig
{
protected:
	std::vector<SConfigValue<int>*> ints;
	std::vector<SConfigValue<bool>*> bools;
	std::vector<SConfigValue<float>*> floats;
	std::vector<SConfigValue<char*>*> strings;
private:
	void SetupValue(int&, int, std::string, std::string);
	void SetupValue(bool&, bool, std::string, std::string);
	void SetupValue(float&, float, std::string, std::string);
	void SetupValue(char* &, char*, std::string, std::string);
public:
	SCConfig()
	{
		Setup();
	}

	void Setup();

	void Save();
	void Load();
};

extern SCConfig* SkinsConfig;
#endif // !CFG
