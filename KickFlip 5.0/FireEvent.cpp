#include "Hooks.hpp"

bool HasBeenDefused = false;
bool IsSomeoneDefusing = false;
bool BombDetonated = false;

bool __fastcall Hooks::Hooked_FireEvent(void* ecx, void* edx, SourceEngine::IGameEvent* pEvent, bool DontBroadcast)
{

	//BOMB DEFUSE CRAP
	if (!strcmp(pEvent->GetName(), XorStr("bomb_begindefuse")))
		IsSomeoneDefusing = true;
	if (!strcmp(pEvent->GetName(), XorStr("bomb_abortdefuse")))
		IsSomeoneDefusing = false;
	if (!strcmp(pEvent->GetName(), XorStr("bomb_defused")))
	{
		IsSomeoneDefusing = false;
		HasBeenDefused = true;
	}
	if (!strcmp(pEvent->GetName(), XorStr("bomb_exploded")))
	{
		HasBeenDefused = false;
		IsSomeoneDefusing = false;
		BombDetonated = true;
	}
	if (!strcmp(pEvent->GetName(), XorStr("round_prestart")))
	{
		HasBeenDefused = false;
		IsSomeoneDefusing = false;
		BombDetonated = false;
	}
	return g_fnOriginalFireEvent(ecx, pEvent, DontBroadcast);
}

bool __fastcall Hooks::Hooked_FireEventClientSide(void* ecx, void* edx, SourceEngine::IGameEvent* pEvent)
{
	if (!pEvent)
	{
		Utils::PrintMessage(XorStr("Pointer Is Invalid"));
		return g_fnOriginalFireEventClientSide(ecx, pEvent);
	}

	// Update model indexes when switching servers.
	if (!strcmp(pEvent->GetName(), XorStr("game_newmap")))
	{
		// Clear existing values.
		if (GlobalVars.Skins.g_ViewModelCfg.size() >= 1)
			GlobalVars.Skins.g_ViewModelCfg.clear();

		// Call SetModelConfig.
		SkinFunctions::SetModelConfig();
		Utils::PrintMessage(XorStr("Kickflip Model Config Initalised.\n"));
	}

	// Run our replacement function when a "player_death" event is fired.
	if (!strcmp(pEvent->GetName(), XorStr("player_death")))
		SkinFunctions::ApplyCustomKillIcon(pEvent);

	return g_fnOriginalFireEventClientSide(ecx, pEvent);
}