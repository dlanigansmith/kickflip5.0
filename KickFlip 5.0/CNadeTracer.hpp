#pragma once
#ifndef _CNADETRACER_H
#define _CNADETRACER_H

#include "Hooks.hpp"
#include "draw.hpp"

#include <vector>
#include <set>
#include <unordered_map>

class CTimer
{
private:
	__int64 initialTS, currentTS;
	float secsPerCount;

public:
	CTimer()
	{
		__int64 countsPerSec = initialTS = currentTS = 0;
		QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
		if (countsPerSec == 0)
			secsPerCount = 1.0f;
		else
			secsPerCount = 1.0f / (float)countsPerSec;
	}
	void init()
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&initialTS);
	}
	float diff()
	{
		QueryPerformanceCounter((LARGE_INTEGER*)&currentTS);
		return ((float)currentTS - (float)initialTS)*secsPerCount;
	}
};

struct CTraceObj
{
	CTraceObj() = default;
	CTraceObj(SourceEngine::Color, unsigned int);

	SourceEngine::Color m_colorTrace;
	unsigned int m_iMaxLength;
	std::vector<SourceEngine::Vector> m_vecTracePoints;
	CTimer m_timerPointLock;
};

class CNadeTracer
{
private:
	

	std::unordered_map<C_CSPlayer*, CTraceObj> m_mapGrenades;
	std::set<C_CSPlayer*> m_setGrenadeExists;

public:
	CNadeTracer();
	void AddTracer(C_CSPlayer*, SourceEngine::Color, unsigned int);
	void Draw();
	void Clear();
	void Run(int i);
};

extern CNadeTracer* NadeTracer;
#endif