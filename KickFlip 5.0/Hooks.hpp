
#include "includes.hpp"
#include "SourceEngine/SDK.hpp"
#include "VFTTable.hpp"
#include "SkinFunctions.hpp"
#include "OffsetManager.hpp"
#include "CSGOStructs.hpp"



#include <d3d9.h>
#include <d3dx9.h>
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
//D3D
typedef long(__stdcall* EndScene_t)(IDirect3DDevice9* device);
typedef long(__stdcall* Reset_t)(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pp);

typedef bool(__thiscall* PaintTraverse_t)(void*, unsigned int, bool, bool);
typedef bool(__thiscall* CreateMove_t)(SourceEngine::IClientMode*, float, SourceEngine::CUserCmd*);
typedef void(__thiscall* FrameStageNotify_t)(void*, SourceEngine::ClientFrameStage_t);
typedef bool(__thiscall* FireEventClientSide)(void*, SourceEngine::IGameEvent*);
typedef bool(__thiscall* FireEvent_t)(void*, SourceEngine::IGameEvent*, bool);
typedef void(__thiscall* OverrideView_t)(SourceEngine::IClientMode* pThis, SourceEngine::CViewSetup* pSetup);
typedef void(__thiscall* PlaySound_t)(SourceEngine::ISurface*, const char*);
//typedef void(__stdcall* DrawModelExecute_t)(void*, void*, const SourceEngine::ModelRenderInfo_t&, SourceEngine::matrix3x4_t*);
typedef void(__thiscall* DrawModelExecute_t)(void*, void*, void*, const SourceEngine::ModelRenderInfo_t&, SourceEngine::matrix3x4_t*);
using ServerRankRevealAllFn = bool(__cdecl*)(float*);


extern EndScene_t                         g_fnOriginalEndScene;
extern Reset_t                            g_fnOriginalReset;

extern CreateMove_t                       g_fnOriginalCreateMove;
extern FrameStageNotify_t                 g_fnOriginalFrameStageNotify;
extern FireEventClientSide                g_fnOriginalFireEventClientSide;
extern FireEvent_t						  g_fnOriginalFireEvent;
extern PaintTraverse_t					  g_fnOriginalPaintTraverse;
extern OverrideView_t                     g_fnOriginalOverRideView;
extern PlaySound_t                        g_fnOriginalPlaySound;

extern DrawModelExecute_t                 g_fnOriginalDrawModelExecute;

extern SourceEngine::RecvVarProxyFn	      g_fnSequenceProxyFn;

extern std::unique_ptr<VFTableHook>       g_pClientModeHook;
extern std::unique_ptr<VFTableHook>       g_pClientHook;
extern std::unique_ptr<VFTableHook>       g_pGameEventHook;
extern std::unique_ptr<VFTableHook>       g_pVGUIHook;
extern std::unique_ptr<VFTableHook>       g_pMatSurfaceHook;
extern std::unique_ptr<VFTableHook>       g_ModelRenderHook;
extern std::unique_ptr<VFTableHook>       g_pD3DDevice9Hook;


extern ServerRankRevealAllFn			  ServerRankRevealAllEx;
extern WNDPROC                            g_pOldWindowProc;
extern HWND                               g_hWindow;

extern bool                               vecPressedKeys[256];
extern bool                               g_bWasInitialized; 
extern bool							          g_hasGuiInit;


//GAMEEVENT STUFF
//Variable for bomb states
extern bool IsSomeoneDefusing;
extern bool HasBeenDefused;
extern bool BombDetonated;

extern bool bSendPacket; 
extern bool KillAll;
extern HMODULE DLL;
extern bool Aimbotting;
extern bool Return;
extern SourceEngine::CUserCmd* UserCMD;




namespace Hooks
{
	//Hook the proxy
	void Hook_Proxy();

	//Constructors/Deconstructors
	void Initialize(LPVOID lpParameter);
	
	
	void Remove(LPVOID lpParameter);

	//Misc
	
	//D3D Hooks
	HRESULT   __stdcall Hooked_EndScene(IDirect3DDevice9* pDevice);
	HRESULT   __stdcall Hooked_Reset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters);
	//HOOKS
	
	bool      __stdcall  Hooked_CreateMove(float sample_input_frametime, SourceEngine::CUserCmd* pCmd);
	void      __fastcall Hooked_FrameStageNotify(void* ecx, void* edx, SourceEngine::ClientFrameStage_t Stage);
	bool      __fastcall Hooked_FireEventClientSide(void* ecx, void* edx, SourceEngine::IGameEvent* pEvent);
	bool      __fastcall Hooked_FireEvent(void* ecx, void* edx, SourceEngine::IGameEvent* pEvent, bool DontBroadcast);
	void __fastcall      Hooked_PaintTraverse(void* _this, void* edx, unsigned int pPanel,  bool forceRepaint, bool allowForce);
	void __fastcall		 Hooked_OverrideView(SourceEngine::IClientMode* pThis, DWORD _EDX, SourceEngine::CViewSetup* pSetup);
	void				 SetViewModelSequence(const SourceEngine::CRecvProxyData *pDataConst, void *pStruct, void *pOut);
	void      __stdcall  Hooked_PlaySound(const char* szFileName);
	
	void __fastcall Hooked_DrawModelExecute(void* thisptr, int edx, void* ctx, void* state, const SourceEngine::ModelRenderInfo_t &pInfo, SourceEngine::matrix3x4_t *pCustomBoneToWorld);
	
	//replaced wndproc so we can get that dope input
	LRESULT   __stdcall Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
}; 