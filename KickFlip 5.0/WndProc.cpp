#include "includes.hpp"
#include "Hooks.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"
extern LRESULT ImGui_ImplDX9_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
void OpenMenu();
LRESULT   __stdcall Hooks::Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	//Captures the keys states
	switch (uMsg)
	{
	case WM_LBUTTONDOWN:
		vecPressedKeys[VK_LBUTTON] = true;
		break;
	case WM_LBUTTONUP:
		vecPressedKeys[VK_LBUTTON] = false;
		break;
	case WM_RBUTTONDOWN:
		vecPressedKeys[VK_RBUTTON] = true;
		break;
	case WM_RBUTTONUP:
		vecPressedKeys[VK_RBUTTON] = false;
		break;
	case WM_MBUTTONDOWN:
		vecPressedKeys[VK_MBUTTON] = true;
		break;
	case WM_MBUTTONUP:
		vecPressedKeys[VK_MBUTTON] = false;
		break;
	case WM_XBUTTONDOWN:
	{
		UINT button = GET_XBUTTON_WPARAM(wParam);
		if (button == XBUTTON1)
		{
			vecPressedKeys[VK_XBUTTON1] = true;
		}
		else if (button == XBUTTON2)
		{
			vecPressedKeys[VK_XBUTTON2] = true;
		}
		break;
	}
	case WM_XBUTTONUP:
	{
		UINT button = GET_XBUTTON_WPARAM(wParam);
		if (button == XBUTTON1)
		{
			vecPressedKeys[VK_XBUTTON1] = false;
		}
		else if (button == XBUTTON2)
		{
			vecPressedKeys[VK_XBUTTON2] = false;
		}
		break;
	}
	case WM_KEYDOWN:
		vecPressedKeys[wParam] = true;
		break;
	case WM_KEYUP:
		vecPressedKeys[wParam] = false;
		break;
	default: break;
	}

	OpenMenu();

	if (g_hasGuiInit && GlobalVars.Enabled.MainWinOpen && ImGui_ImplDX9_WndProcHandler(hWnd, uMsg, wParam, lParam))
		return true; //Input was consumed, return	
	
					 
	return CallWindowProc(g_pOldWindowProc, hWnd, uMsg, wParam, lParam);
}

void OpenMenu()
{

	static bool is_down = false;
	static bool is_clicked = false;

	if (vecPressedKeys[VK_INSERT])
	{
		is_clicked = false;
		is_down = true;
	}
	else if (!vecPressedKeys[VK_INSERT] && is_down)
	{
		is_clicked = true;
		is_down = false;
	}
	else
	{
		is_clicked = false;
		is_down = false;
	}

	if (is_clicked)
	{
		GlobalVars.Enabled.MainWinOpen = !GlobalVars.Enabled.MainWinOpen;
		std::string msg = XorStr("cl_mouseenable ") + std::to_string(!GlobalVars.Enabled.MainWinOpen);
		SourceEngine::Interfaces::Engine()->ClientCmd_Unrestricted(msg.c_str());
	}
}