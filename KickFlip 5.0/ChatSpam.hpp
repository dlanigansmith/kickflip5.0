#pragma once
#include "includes.hpp"
#include "Hooks.hpp"

#ifndef CHATSPAM
#define CHATSPAM
class cChatSpam
{
public:
	static void Run()
	{
		static float NextTime = 0.f;
		if (!SourceEngine::Interfaces::Engine()->IsInGame())
			return;
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		if (!pLocal)
			return;
		float server_time = pLocal->GetTickBase() * SourceEngine::Interfaces::GlobalVars()->interval_per_tick;
		if (NextTime > server_time)
			return;

		NextTime = server_time + 0.5f;

		SourceEngine::Interfaces::Engine()->ClientCmd_Unrestricted(XorStr("say \"PROUD USER OF KICKFLIPCS.COM\""));
		
	}
	static void RunESP()
	{
		static float NextTime = 0.f;
		if (!GlobalVars.Settings.Misc.ChatESP.Enabled)
			return;
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		if (!pLocal)
			return;
		float server_time = pLocal->GetTickBase() * SourceEngine::Interfaces::GlobalVars()->interval_per_tick;
		if (NextTime > server_time)
			return;
		static std::string LastName;
		for (int i = 1; i < 65; i++)
		{
			if (i == SourceEngine::Interfaces::Engine()->GetLocalPlayer())
				continue;
			auto gEntity = static_cast<C_CSPlayer*>(SourceEngine::Interfaces::EntityList()->GetClientEntity(i));
			if (!gEntity)
				continue;
			if (!(gEntity->GetClientClass()->m_ClassID == SourceEngine::EClassIds::CCSPlayer))
				continue;
			if (!gEntity->IsAlive() || gEntity->IsDormant())
				continue;
			if (gEntity->GetTeamNum() == pLocal->GetTeamNum())
				continue;
			if (!(gEntity->GetHealth() > 0))
				continue;
			std::string Health = std::to_string(gEntity->GetHealth());
			SourceEngine::PlayerInfo pInfo;
			SourceEngine::Interfaces::Engine()->GetPlayerInfo(gEntity->EntIndex(), &pInfo);
			std::string Name = pInfo.m_szPlayerName;
			std::string LastPlace = gEntity->GetLastPlaceName(); 
			std::string ToPrint = XorStr("say ") + Name + XorStr(" | ") + Health + XorStr(" HP | ") + LastPlace;
			if (!strcmp(Name.c_str(), LastName.c_str()))
				SourceEngine::Interfaces::Engine()->ClientCmd_Unrestricted(ToPrint.c_str());
			LastName = Name;
		}

		NextTime = server_time + 0.5f;
	}
private:
	//const char* Insults[] = { XorStr("Sponsored by KickFlipCS.com"), XorStr("SELF CODED BY B5"), XorStr("If I wanted to commit, I'd just jump from your ego to your elo"), XorStr("I play Osu to train my aim"), XorStr("Kill yourself my man"),
					//		XorStr("Hey guys what's up 58Ben here"), XorStr("CSGOANI.ME"), "AIMWARE.NET", XorStr("416-439-0000"), XorStr("HACKERS RUIN GAMES"),"kek", XorStr("PROUD USER OF KICKFLIPCS.COM") };
};
#endif // !CHATSPAM


