#include "Aim.hpp"
SourceEngine::Vector LastAngle;
int spinState;
float oForward;
float oSide;
void Aim::SpinBot(SourceEngine::CUserCmd* pCmd)
{
	using namespace SourceEngine;

	if (!GlobalVars.Settings.Aim.Spinbot.Enabled) return;
	if (!Interfaces::Engine()->IsInGame()) return;
	if (pCmd->buttons & IN_USE) return;
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	//if(pLocal->) add movement check
	Vector LastLastAngle = LastAngle;
	Vector spinAngles;
	spinAngles.y += 45 * (spinState % 8);
	Utils::Clamp(spinAngles);
	pCmd->viewangles = spinAngles;
	
	CorrectMovement(spinAngles, pCmd, oForward, oSide);


	oForward = pCmd->forwardmove;
	oSide = pCmd->sidemove;
	LastAngle = pCmd->viewangles;
	Return = false;
	spinState++;
}

void Aim::CorrectMovement(SourceEngine::Vector vOldAngles, SourceEngine::CUserCmd* pCmd, float fOldForward, float fOldSidemove)
{
	//side/forward move correction
	float deltaView = pCmd->viewangles.y - vOldAngles.y;
	float f1;
	float f2;

	if (vOldAngles.y < 0.f)
		f1 = 360.0f + vOldAngles.y;
	else
		f1 = vOldAngles.y;

	if (pCmd->viewangles.y < 0.0f)
		f2 = 360.0f + pCmd->viewangles.y;
	else
		f2 = pCmd->viewangles.y;

	if (f2 < f1)
		deltaView = abs(f2 - f1);
	else
		deltaView = 360.0f - abs(f1 - f2);
	deltaView = 360.0f - deltaView;

	pCmd->forwardmove = cos(DEG2RAD(deltaView)) * fOldForward + cos(DEG2RAD(deltaView + 90.f)) * fOldSidemove;
	pCmd->sidemove = sin(DEG2RAD(deltaView)) * fOldForward + sin(DEG2RAD(deltaView + 90.f)) * fOldSidemove;
}
