#include "Hooks.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"

HRESULT __stdcall Hooks::Hooked_Reset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	if (!g_hasGuiInit) return g_fnOriginalReset(pDevice, pPresentationParameters);
	ImGui_ImplDX9_InvalidateDeviceObjects();

	auto hr = g_fnOriginalReset(pDevice, pPresentationParameters);
	ImGui_ImplDX9_CreateDeviceObjects();
	return hr;
}