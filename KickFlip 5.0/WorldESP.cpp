#include "WorldESP.hpp"
#include "CNadeTracer.hpp"


void WorldESP::WorldESPTick(int i)
{
	using namespace SourceEngine;
	
	if (!GlobalVars.Enabled.ESP) return; //check if esp is on

	
	
	auto pLocal = C_CSPlayer::GetLocalPlayer(); //get localplayer
	if (i == Interfaces::Engine()->GetLocalPlayer()) return; //ignore if local player
	auto gEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(i)); //Get ent
	if (!gEntity) return; //check if null
	Vector pos, pos3D;
	pos3D = gEntity->GetOrigin();

	if (pos3D.IsZero())
		return;

	if (!Utils::WorldToScreen(pos3D, pos))
		return;
	int owner = *(int*)((DWORD)gEntity + Offsets->m_hOwnerEntity);

	if (owner == -1)
	{
		if (GlobalVars.Settings.ESP.Weapons)
		{
			if (strstr(gEntity->GetClientClass()->m_pNetworkName, XorStr("CWeapon")))
			{
				auto wEntity = reinterpret_cast<C_BaseCombatWeapon*>(gEntity);

				Draw::DrawString(5, pos.x, pos.y, Color::White(), false, "%s", std::string(wEntity->GetClientClass()->m_pNetworkName).substr(7).c_str());
			}
			   
			if (gEntity->GetClientClass()->m_ClassID == 1)
			{
				Draw::DrawString(5, pos.x, pos.y, Color::White(), false, "%s", XorStr("AK-47"));
			}

			if (gEntity->GetClientClass()->m_ClassID == 39)
			{
				Draw::DrawString(5, pos.x, pos.y, Color::White(), false, "%s", XorStr("Deagle"));
			}
		}

		if (gEntity->GetClientClass()->m_ClassID == 29 && GlobalVars.Settings.ESP.BombTimer)
			Draw::DrawString(5, pos.x, pos.y, Color::Aquamarine(), false, "%s", XorStr("C4"));
	}
	
	if (strstr(gEntity->GetClientClass()->m_pNetworkName, XorStr("CPlantedC4") ) && GlobalVars.Settings.ESP.BombTimer) //get classid make sure its a playa
	{
		float bombTime = *(float*)((DWORD)gEntity + Offsets->m_flC4Blow);
		float returnValue = bombTime - Interfaces::GlobalVars()->curtime;
		float bombTimer = (returnValue < 0) ? 0.f : returnValue; 
		
		if(IsSomeoneDefusing)
			Draw::DrawString(5, pos.x, pos.y, Color::Blue(), true, "%.1f", bombTimer);
		else if(HasBeenDefused)
			Draw::DrawString(5, pos.x, pos.y, Color::Green(), true, "%s", XorStr("Defused"));
		else if (BombDetonated)
			Draw::DrawString(5, pos.x, pos.y, Color::White(), true, "%s", XorStr("Detonated"));
		else
			Draw::DrawString(5, pos.x, pos.y, Color::Red(), true, "%.1f", bombTimer);
		
	}

	if (std::string(gEntity->GetClientClass()->m_pNetworkName).find(XorStr("GrenadeProjectile")) != std::string::npos)
	{

		Draw::DrawString(5, pos.x, pos.y, Color::White(), true, "%s", XorStr("Nade")); //TODO ADD CASES FOR THIS AND STUFF 
		

	}
}