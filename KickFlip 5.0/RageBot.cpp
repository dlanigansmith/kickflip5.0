#include "Aim.hpp"
#include "Movefix.hpp"

float LowestFovRage;
int BestTargetRage;
float deltaTimeRage;
float curAimTimeRage;
C_CSPlayer* BestEntityRage;
SourceEngine::QAngle LastAngleRage;

FORCEINLINE void VectorSubtract(const SourceEngine::Vector& a, const SourceEngine::Vector& b, SourceEngine::Vector& c)
{
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
}
bool AutoWall(SourceEngine::Vector vPos, C_CSPlayer* gEntity)
{
	using namespace SourceEngine;
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	trace_t tr;
	Ray_t ray;
	Vector vStart, vEnd, vEndPos[3];
	vStart = pLocal->GetEyePos();
	vEnd = vPos;
	CTraceFilter filter;

	if (gEntity->IsVisible(ECSPlayerBones::head_0))
		return true;


	filter.pSkip = pLocal;
	ray.Init(vStart, vEnd);

	Interfaces::EngineTrace()->TraceRay(ray, MASK_SHOT, &filter, &tr);

	vEndPos[0] = tr.endpos;

	ray.Init(vEnd, vStart);
	filter.pSkip = gEntity;
	Interfaces::EngineTrace()->TraceRay(ray, MASK_SHOT, &filter, &tr);

	vEndPos[1] = tr.endpos;

	VectorSubtract(vEndPos[0], vEndPos[1], vEndPos[2]);

	return (vEndPos[2].Length() < 17.5f) ;  
}
void RageGetNewTarget()
{
	using namespace SourceEngine;
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	for (int i = 1; i < 65; i++)
	{

		if (i == Interfaces::Engine()->GetLocalPlayer())
			continue;
		auto gEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(i));
		if (!gEntity)
			continue;
		if (!(gEntity->GetClientClass()->m_ClassID == SourceEngine::EClassIds::CCSPlayer))
			continue;
		if (!gEntity->IsAlive() || gEntity->IsDormant() || !gEntity->GetImmune())
			continue;
		if (gEntity->GetTeamNum() == pLocal->GetTeamNum())
			continue;
		if (!(gEntity->GetHealth() > 0))
			continue;
		SourceEngine::Vector bone = Utils::GetEntityBone(gEntity, SourceEngine::ECSPlayerBones::head_0);
		if ((gEntity->IsVisible(SourceEngine::ECSPlayerBones::head_0)) && (GlobalVars.Settings.Aim.Rage.IsVisible))
		{
		float fov = M::GetFov(UserCMD->viewangles, M::CalcAngle(pLocal->GetEyePos(), bone));

			if (fov < LowestFovRage)
			{
				LowestFovRage = fov;
				BestTargetRage = i;
			}
		} 
			
		else if ((AutoWall(bone, gEntity) && (GlobalVars.Settings.Aim.Rage.AutoWall)))
		{
			float fov = M::GetFov(UserCMD->viewangles, M::CalcAngle(pLocal->GetEyePos(), bone));

			if (fov < LowestFovRage)
			{
				LowestFovRage = fov;
				BestTargetRage = i;
			}
		}
		
	}
}
void Aim::RageBot()
{

	
	using namespace SourceEngine;
	if (GlobalVars.Settings.Aim.Rage.Enabled && ((GlobalVars.Settings.Aim.Rage.AlwaysOn) || vecPressedKeys[GlobalVars.Settings.Aim.Rage.Key]))
	{
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		static float oldServerTime = pLocal->GetTickBase() * Interfaces::GlobalVars()->interval_per_tick;
		float serverTime = pLocal->GetTickBase() * Interfaces::GlobalVars()->interval_per_tick;
		deltaTimeRage = serverTime - oldServerTime;
		oldServerTime = serverTime;
		
		RageGetNewTarget();

		BestEntityRage = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(BestTargetRage));
		if (!BestEntityRage)
			return;
		if (BestTargetRage != -1 && BestEntityRage->GetHealth() > 0 && pLocal->IsAlive())
		{
			bool auto_shoot = false;
			bool can_shoot = true;
			bool reloading = false;
			auto weapon = pLocal->GetWeapon();
			float serverTime = pLocal->GetTickBase() * Interfaces::GlobalVars()->interval_per_tick;
			float next_shot = weapon->NextPrimaryAttack() - serverTime;
			if (next_shot > 0)
				can_shoot = false;
			
			
			
			Vector predicted = BestEntityRage->GetPredicted(Utils::GetEntityBone(BestEntityRage, SourceEngine::ECSPlayerBones::head_0)); //TODO:: switch to engine prediction for better hvh goodness
			
			QAngle dst = M::CalcAngle(pLocal->GetEyePos(), predicted);
			auto PunchAngles = *pLocal->AimPunch() * (2.0f);
			dst -= PunchAngles;
			Aimbotting = true;
			CFixMove aimFix;
			if (GlobalVars.Settings.Aim.Rage.Silent)
				aimFix.Start();


			if (GlobalVars.Settings.Aim.Rage.AimStep && can_shoot)
			{
				QAngle angNextAngle;
				bool finished = Utils::Aimstep(LastAngleRage, dst, angNextAngle, 16); 
				LastAngleRage = angNextAngle;
				dst = angNextAngle;
				UserCMD->viewangles = dst;
				if (finished) auto_shoot = true;
			}
			else if (can_shoot)
			{	
				UserCMD->viewangles = dst;
				auto_shoot = true;
			}
			if (GlobalVars.Settings.Aim.Rage.Silent)
				aimFix.End();

			if (GlobalVars.Settings.Aim.Rage.AutoStop && !weapon->IsEmpty() && weapon->IsGun())
			{
				UserCMD->forwardmove = 0;
				UserCMD->sidemove = 0;
				UserCMD->upmove = 0;
				UserCMD->buttons = 0;
			}
			if (GlobalVars.Settings.Aim.Rage.AutoCrouch && !weapon->IsEmpty() && weapon->IsGun())
			{
				UserCMD->buttons |= IN_DUCK;
			}
			

			if (auto_shoot && can_shoot && GlobalVars.Settings.Aim.Rage.AutoFire && !weapon->IsEmpty() && weapon->IsGun())
				UserCMD->buttons |= IN_ATTACK;

			
			
		} 
		else if (BestTargetRage != -1)
		{
			BestTargetRage = -1;
			curAimTimeRage = 0.f;
			LowestFovRage = GlobalVars.Settings.Aim.Rage.FOVLimit;
			BestEntityRage = nullptr;
		} 
	}
	else
	{
		BestTargetRage = -1;
		curAimTimeRage = 0.f;
		LowestFovRage = GlobalVars.Settings.Aim.Rage.FOVLimit;
		BestEntityRage = nullptr;
	}
	return;
}
