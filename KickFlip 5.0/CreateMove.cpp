#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "Aim.hpp"
#include "AntiAim.hpp"
#include "Misc.hpp"



bool __stdcall  Hooks::Hooked_CreateMove(float sample_input_frametime, SourceEngine::CUserCmd* pCmd)
{
	
	//Call original CreateMove
	auto bRet = g_fnOriginalCreateMove(SourceEngine::Interfaces::ClientMode(), sample_input_frametime, pCmd);
	if (pCmd->command_number == 0  || !SourceEngine::Interfaces::Engine()->IsInGame())
		return false;
	
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	UserCMD = pCmd;
	if (!pLocal || !pLocal->IsAlive())
		return bRet;

	DWORD* framePointer;
	__asm mov framePointer, ebp;
	*(bool*)(*framePointer - 0x1C) = bSendPacket;

	//learned how to BunnyHop today, do you see this?
	cMisc::BunnyHop(pLocal);
	
	
	if (!(pCmd->buttons & IN_ATTACK) && GlobalVars.Settings.Misc.Fakelag.Enabled)
	{
		static int Ticks = 0;
		int flagval = 12;
		if ((Ticks < flagval))
			bSendPacket = false;

		if (Ticks == (flagval * 2))
		{
			bSendPacket = true;

			Ticks = 0;
		}

		Ticks++;
	} 
	
	Aim::AutoPistol();
	Aim::LegitBot();
	Aim::RageBot();
	if(!Aimbotting)
		Aim::RCS();
	if(GlobalVars.Settings.Triggerbot.Enabled)
		Aim::Trigger();
	AntiAim::Run();
	
	cMisc::AirStuck(); //two best memes are run here
	cMisc::MemeWalk(pLocal);
	
	Utils::Clamp(pCmd->viewangles);
	if (!GlobalVars.Settings.Aim.Rage.Silent && Aimbotting)
		SourceEngine::Interfaces::Engine()->SetViewAngles(pCmd->viewangles);
	
	Aimbotting = false;
	return false; 
}