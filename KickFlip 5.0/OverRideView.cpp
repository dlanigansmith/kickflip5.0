#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"

void __fastcall Hooks::Hooked_OverrideView(SourceEngine::IClientMode* pThis, DWORD _EDX, SourceEngine::CViewSetup* pSetup)
{
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	if (GlobalVars.Settings.Visuals.FOVChanger)
	{
		//if()
		pSetup->fov += (GlobalVars.Settings.Visuals.FOV - 67);
	}
		
	
	g_fnOriginalOverRideView(pThis, pSetup);
}