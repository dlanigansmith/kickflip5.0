#pragma once
#include "./SourceEngine/Definitions.hpp"

#include "./SourceEngine/CRC.hpp"
#include "./SourceEngine/Vector.hpp"
#include "./SourceEngine/Vector2D.hpp"
#include "./SourceEngine/Vector4D.hpp"
#include "./SourceEngine/QAngle.hpp"
#include "./SourceEngine/CHandle.hpp"
#include "./SourceEngine/CGlobalVarsBase.hpp"
#include "./SourceEngine/ClientClass.hpp"
#include "./SourceEngine/Color.hpp"
#include "./SourceEngine/IBaseClientDll.hpp"
#include "./SourceEngine/IClientEntity.hpp"
#include "./SourceEngine/IClientEntityList.hpp"
#include "./SourceEngine/IClientNetworkable.hpp"
#include "./SourceEngine/IClientRenderable.hpp"
#include "./SourceEngine/IClientThinkable.hpp"
#include "./SourceEngine/IClientUnknown.hpp"
#include "./SourceEngine/IGameEvents.hpp"
#include "./SourceEngine/IPanel.hpp"
#include "./SourceEngine/ISurface.hpp"
#include "./SourceEngine/IVEngineClient.hpp"
#include "./SourceEngine/IVModelInfoClient.hpp"
#include "./SourceEngine/IEngineTrace.hpp"
#include "./SourceEngine/PlayerInfo.hpp"
#include "./SourceEngine/Recv.hpp"
#include "./SourceEngine/VMatrix.hpp"
#include "./SourceEngine/IClientMode.hpp"
#include "./SourceEngine/CInput.hpp"
#include "./SourceEngine/ICvar.hpp"
#include "./SourceEngine/Convar.h"

namespace SourceEngine
{
	struct ModelRenderInfo_t
	{
		Vector origin;
		QAngle angles;
		IClientRenderable *pRenderable;
		const model_t *pModel;
		const matrix3x4_t *pModelToWorld;
		const matrix3x4_t *pLightingOffset;
		const Vector *pLightingOrigin;
		int flags;
		int entity_index;
		int skin;
		int body;
		int hitboxset;
		ModelInstanceHandle_t instance;
		ModelRenderInfo_t()
		{
			pModelToWorld = NULL;
			pLightingOffset = NULL;
			pLightingOrigin = NULL;
		}
	};
	class IVModelRender
	{
	public:
		void ForcedMaterialOverride(IMaterial *mat);
		void DrawModelExecute(void* ctx, void *state, const ModelRenderInfo_t &pInfo, matrix3x4_t *pCustomBoneToWorld = NULL);
	};

}