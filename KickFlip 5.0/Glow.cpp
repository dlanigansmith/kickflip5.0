#include "Glow.hpp"

void Glow::GlowTick()
{
	using namespace SourceEngine;
	if (!GlobalVars.Enabled.Glow) return;
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	CGlowObjectManager* GlowObjectManager = (CGlowObjectManager*)Offsets->GlowManager;
	
	for (int i = 0; i < GlowObjectManager->size; ++i)
	{
		CGlowObjectManager::GlowObjectDefinition_t* glowEntity = &GlowObjectManager->m_GlowObjectDefinitions[i];
		IClientEntity* Entity = glowEntity->getEntity(); 

		if (glowEntity->IsEmpty() || !Entity)
			continue;
		static int m_iTeamNum = GET_NETVAR(XorStr("DT_BaseEntity"), XorStr("m_iTeamNum"));
		int EntTeamNum = *(DWORD*)((DWORD)Entity + m_iTeamNum);
		switch (Entity->GetClientClass()->m_ClassID)
		{

		case 1:
			if (GlobalVars.Settings.ESP.Glow.Weapons)
				glowEntity->set(Color(255, 138, 46, 250));

			break;

		case 29:
			if (GlobalVars.Settings.ESP.Glow.C4)
				glowEntity->set(Color(84, 147, 230, 250));

			break;

		case 35:      
			
				if (!GlobalVars.Settings.ESP.Glow.Teammates && EntTeamNum == pLocal->GetTeamNum())
					break;

				if (!GlobalVars.Settings.ESP.Glow.Enemies && EntTeamNum != pLocal->GetTeamNum())
					break;
			glowEntity->set(EntTeamNum == pLocal->GetTeamNum() ? Color(GlobalVars.Settings.ESP.Glow.Team.r, GlobalVars.Settings.ESP.Glow.Team.g, GlobalVars.Settings.ESP.Glow.Team.b, GlobalVars.Settings.ESP.Glow.Team.a) : Color(GlobalVars.Settings.ESP.Glow.Enemy.r, GlobalVars.Settings.ESP.Glow.Team.g, GlobalVars.Settings.ESP.Glow.Team.b, GlobalVars.Settings.ESP.Glow.Team.a));
			
			break;

		case 39:
			if (GlobalVars.Settings.ESP.Glow.Weapons)
				glowEntity->set(Color(255, 138, 46, 250));

			break;

		case 41:
			if (GlobalVars.Settings.ESP.Glow.Decoys)
				glowEntity->set(Color(230, 78, 255, 250));

			break;

		case 105:
			if (GlobalVars.Settings.ESP.Glow.C4)
				glowEntity->set(Color(255, 39, 33, 250));
			break;

		default:
			if (GlobalVars.Settings.ESP.Glow.Weapons)
			{
				if (strstr(Entity->GetClientClass()->m_pNetworkName, XorStr("CWeapon")))
					glowEntity->set(Color(255, 138, 46, 250));
			}
			break;
		}
	}
}
