#include "MainMenu.hpp"

Menu* MainMenu = new Menu();

void Menu::Setup()
{
	int iScrW, iScrH;
	int iMenuW = 800, iMenuH = 600; //size and width
	SourceEngine::Interfaces::MatSurface()->GetScreenSize(iScrW, iScrH);
	std::stringstream Pos;
	
	Utils::PrintMessage(Pos.str().c_str());
	Frame = new MElements::Frame;
	Frame->SetPos(iScrW / 2 - iMenuW / 2, iScrH / 2 - iMenuH / 2); //
	Frame->SetSize(iMenuW, iMenuH);
	Add(Frame);

	auto aimbotTab = new MElements::Tab;
	aimbotTab->SetTab(0);
	aimbotTab->SetTitle("Aimbot");

	Utils::PrintMessage("KickFlipCS GUI has been initialized \n");
}


void Menu::Render()
{
	Input->GetClicked();

	for (MElements::MenuElement* pElement : elements)
	{
		pElement->Tick();
	}

	int mpX, mpY;
	SourceEngine::Interfaces::InputSystem()->GetCursorPosition(&mpX, &mpY);
	if (GlobalVars.	Enabled.MainWinOpen)
	{
		static int mouseSize = 15;
		 
		static SourceEngine::Vertex_t mouseShape[3];
		mouseShape[0].Init(SourceEngine::Vector2D(mpX, mpY));
		mouseShape[1].Init(SourceEngine::Vector2D(mpX + mouseSize, mpY + mouseSize / 2));
		mouseShape[2].Init(SourceEngine::Vector2D(mpX + mouseSize / 2, mpY + mouseSize));

		Draw::DrawPolygon(3, mouseShape, SourceEngine::Color(255, 255, 255, 255));
	}
}

MElements::MenuElement* Menu::GetFrame()
{
	return Frame;
}

void Menu::Add(MElements::MenuElement* elem)
{
	elements.push_back(elem);
}
