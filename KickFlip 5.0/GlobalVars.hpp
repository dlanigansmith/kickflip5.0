#pragma once
#include "includes.hpp"
/*
Our GlobalVars Class, where all settings and such are stored

Add structs for new features like this:

struct
{
	
}Name;

Put the settings for each new feature inside 

struct
{
	bool Enabled = false; //default
}Name;

Structs can be nested:

struct
{
	bool Enabled;
	struct
	{
		bool Enabled;
	}SubName;
}SubName;

*/

struct Colour
{
	float r, g, b, a;
};
struct EconomyItemConfigs
{
	int iItemDefinitionIndex = 0;
	int nFallbackPaintKit = 0;
	int nFallbackSeed = 0;
	int nFallbackStatTrak = -1;
	int iEntityQuality = 4;
	char* szCustomName = nullptr;
	float flFallbackWear = 0.1f;
	int ModelIndex = 0;
};

struct tGlobalVars
{
	struct
	{
		bool BHop = true;
		bool Skins = true;
		bool MainWinOpen = true;
		bool ESP;
		bool Glow = false; 
		bool FOV = true;
		bool AutoAccept = true;
		bool ChatSpam = false;
	}Enabled;
	struct
	{
		std::unordered_map<int, EconomyItemConfigs> g_SkinChangerCfg;
		std::unordered_map<int, const char*> g_ViewModelCfg;
		std::unordered_map<const char*, const char*> g_KillIconCfg;
	}Skins;

	struct
	{
		struct
		{
			float g_bESPTeamColor[4] = { 0.0f, 0.0f, 255.0f, 255.0f }; //RGBA color
			float g_bESPEnemyColor[4] = { 255.0f, 0.0f, 0.0f, 255.0f };
			bool Box = false;
			int ESPType = 0;
			bool Weapon = true;
			bool HealthBar = false;
			bool HealthText = true;
			bool Name = true;
			bool BombTimer = true;
			bool Team = true;
			bool Corner = true;
			bool Enemy = false;
			bool Weapons = false;
			
			struct
			{
				bool Teammates = true;
				bool Enemies = true;
				bool Weapons = true;
				bool Decoys = true;
				bool C4 = true;
				Colour Team = { 96.f, 128.f, 192.f, 64.f };
				Colour Enemy = { 192.f, 160.f, 96.f, 64.f };
			}Glow;
			
		}ESP;
		struct
		{
			bool NoVisualRecoil = true;
			bool FOVChanger;
			bool Tracers = false;
			float FOV = 17;
			struct 
			{
				bool Enabled = false;
				bool Team = false;
				bool Enemy = true;
				int TeamColor = 0;
				int EnemyColor = 0;
			}CLRRendering;
			struct
			{
				bool NoHands = false;
				bool NoWeapon = false;
				bool NoFlash = false;
				bool Enabled = false;
			}Removals;
			struct
			{
				bool Enabled = true;
				Colour TeamColHidden = { 72, 219, 75, 255 };
				Colour EnemyColHidden = { 232, 209, 32, 255 };
				Colour TeamColVis = { 84, 167, 255, 255 };
				Colour EnemyColVis = { 200, 60, 60, 255 };
				bool XQZ = true;

			}Chams;
		}Visuals;
		struct
		{
			bool Spin;
			int SpinSpeed;
		}Anti;
		struct
		{
			struct
			{
				bool Enabled = true;
			}RCS;
			struct
			{
				bool Enabled = false;
			}Spinbot;
			struct
			{
				bool Enabled = false;
				bool AimStep = false;
				bool IsVisible = false;
				bool AutoStop = false;
				bool AutoCrouch = false;
				bool AutoFire = false;
				bool AutoWall = false;
				int AimStepAmt = 15;
				float FOVLimit = 180;
				int Key = 0x12;
				bool AlwaysOn = true;
				bool Silent = true;
				float Speed = 10.f;
				float RCSAmount = 100.f;
			}Rage;
			struct
			{
				struct
				{
					bool AutoPistol;
				}Misc;
			}Legit;
			struct
			{
				bool Enabled = false;
				float FOVLimit = 180;
				int Key = 0x12;
				bool AlwaysOn = true;
				bool Silent = true;
				float Speed = 10.f;
				float RCSAmount = 100.f; 
			}Aimlock;
		
		}Aim;
		struct
		{
			bool	Enabled;
			bool	AutoFire;
			int		Key;
			bool 	HitChance;
			float	HitChanceAmt;

			struct
			{
				bool Head;
				bool Chest;
				bool Stomach;
				bool Arms;
				bool Legs;
				bool Friendly;
			} Filter;

		} Triggerbot;
		struct
		{
			int Knife;
		}Skins;
		struct
		{
			bool RevealRanks;
			bool Airstuck;
			int AirStuckKey = 0x43;
			bool Memewalk = false;
			bool Teleport = false;
			struct //Chat ESP Settings, more filters need to be added
			{
				bool Enabled = false;
			}ChatESP;
			struct //Convar Spoofing
			{
				bool thirdperson = false;
				bool mat_grey = false;
				bool mat_lowres = false;
				bool cheats = false;
				bool matmip = false;
			}Spoof; 
			struct
			{
				bool Enabled = false;
				int threshold = 12;
			}Fakelag;
		}Misc; 
		bool NoUntrusted = true; 
	}Settings;
};

extern tGlobalVars GlobalVars;