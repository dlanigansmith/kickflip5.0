#include "Aim.hpp"
static bool WasFiring = false;

void Aim::AutoPistol()
{
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	if (GlobalVars.Settings.Aim.Legit.Misc.AutoPistol)
	{
		if (pLocal->GetWeapon()->IsAutoPistolWorthy()) //IS PISTOL
		{
			if (UserCMD->buttons & IN_ATTACK)
			{
				if (WasFiring)
				{
					UserCMD->buttons &= ~IN_ATTACK;
				}
			}
			WasFiring = UserCMD->buttons & IN_ATTACK ? true : false;
		}
	}
}