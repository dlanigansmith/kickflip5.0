#pragma once
#include "Hooks.hpp"
#include "Input.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"
#include "Menu.hpp"
extern int CurrentTab;

void DrawTabBar(const char* Label, int tabNum) 
{
	bool ChangedStyle = false;
	ImGui::SameLine(); 
	if (CurrentTab == tabNum)
	{ //styles for an active tab button
		ImGui::PushStyleColor(ImGuiCol_Button, ImColor(0.686f, 0.243f, 0.169f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImColor(0.7f, 0.3f, 0.2f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImColor(0.7f, 0.243f, 0.169f));
		ChangedStyle = true;
	}
	
	if (ImGui::Button(Label))
		CurrentTab = tabNum;
	if (ChangedStyle)
		ImGui::PopStyleColor(3); //make sure i get changed if more styles are added

}


void DefaultTab()
{
	ImGui::Text(XorStr("Welcome to Kickflip!"));
	ImGui::Separator();
	ImGui::Text(XorStr("Eventually a guide will go here"));
}

void RagebotTab()
{
	ImGui::Text(XorStr("Ragebot"));
	ImGui::SameLine();
	ImGui::Checkbox(XorStr("Enabled"), &GlobalVars.Settings.Aim.Rage.Enabled);
	ImGui::Checkbox(XorStr("Always On"), &GlobalVars.Settings.Aim.Rage.AlwaysOn);
	ImGui::Checkbox(XorStr("AimStep"), &GlobalVars.Settings.Aim.Rage.AimStep);
	ImGui::Checkbox(XorStr("AutoStop"), &GlobalVars.Settings.Aim.Rage.AutoStop);
	ImGui::Checkbox(XorStr("AutoCrouch"), &GlobalVars.Settings.Aim.Rage.AutoCrouch);
	ImGui::Checkbox(XorStr("Is Visible"), &GlobalVars.Settings.Aim.Rage.IsVisible);
	ImGui::Checkbox(XorStr("AutoFire"), &GlobalVars.Settings.Aim.Rage.AutoFire);
	ImGui::Checkbox(XorStr("AutoWall"), &GlobalVars.Settings.Aim.Rage.AutoWall);
	ImGui::Checkbox(XorStr("Silent"), &GlobalVars.Settings.Aim.Rage.Silent);
	ImGui::SliderFloat(XorStr("Field Of View"), &GlobalVars.Settings.Aim.Rage.FOVLimit, 0.1f, 360.f, XorStr("%.0f Degrees"));
	ImGui::Combo(XorStr("Ragebot Key"), &GlobalVars.Settings.Aim.Rage.Key, keyNames, ARRAYSIZE(keyNames));

	ImGui::Separator();
	ImGui::Text(XorStr("Anti Aim"));
	ImGui::Separator();
	ImGui::Checkbox(XorStr("Enable SpinBot"), &GlobalVars.Settings.Anti.Spin);
	ImGui::Combo(XorStr("Speed"), &GlobalVars.Settings.Anti.SpinSpeed, SpinSpeeds, ARRAYSIZE(SpinSpeeds));
	ImGui::Separator();
}
void VisualsTab()
{
	ImGui::Separator();
	ImGui::Text(XorStr("Visuals"));
	ImGui::Separator();
	ImGui::Text(XorStr("Chams"));
	ImGui::Checkbox(XorStr("Enable Chams"), &GlobalVars.Settings.Visuals.CLRRendering.Enabled);
	ImGui::Combo(XorStr("Team Color"), &GlobalVars.Settings.Visuals.CLRRendering.TeamColor, ChamColours, ARRAYSIZE(ChamColours));
	ImGui::Combo(XorStr("Enemy Color"), &GlobalVars.Settings.Visuals.CLRRendering.EnemyColor, ChamColours, ARRAYSIZE(ChamColours));
	ImGui::Separator();
	ImGui::BeginChild(XorStr("chamsfilter"), ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, 19 * 4));
	{
		ImGui::Selectable(XorStr(" Team"), &GlobalVars.Settings.Visuals.CLRRendering.Team);
		ImGui::Selectable(XorStr(" Enemy"), &GlobalVars.Settings.Visuals.CLRRendering.Enemy);
	}
	ImGui::EndChild();
	ImGui::Separator();
	ImGui::Text(XorStr("Removals"));
	ImGui::Checkbox(XorStr("Enable Removals"), &GlobalVars.Settings.Visuals.Removals.Enabled);
	ImGui::Checkbox(XorStr("No Visual Recoil"), &GlobalVars.Settings.Visuals.NoVisualRecoil);
	ImGui::Checkbox(XorStr("NoHands.TV"), &GlobalVars.Settings.Visuals.Removals.NoHands);
	ImGui::Checkbox(XorStr("No Flash"), &GlobalVars.Settings.Visuals.Removals.NoFlash);
	ImGui::Checkbox(XorStr("No Gun"), &GlobalVars.Settings.Visuals.Removals.NoWeapon);
	ImGui::Separator();
}

void ESPTab()
{
	
		ImGui::Checkbox(XorStr("Enable ESP"), &GlobalVars.Enabled.ESP);
		ImGui::Separator();
		ImGui::Combo(XorStr("ESP Style"), &GlobalVars.Settings.ESP.ESPType, ESPTypes, ARRAYSIZE(ESPTypes));

		ImGui::Checkbox(XorStr("Health Bar"), &GlobalVars.Settings.ESP.HealthBar);
		ImGui::Checkbox(XorStr("Health Text"), &GlobalVars.Settings.ESP.HealthText);
		ImGui::Checkbox(XorStr("Name"), &GlobalVars.Settings.ESP.Name);
		ImGui::Checkbox(XorStr("Weapon"), &GlobalVars.Settings.ESP.Weapon);
		ImGui::Text(XorStr("Filter ESP"));
		ImGui::Separator();
		ImGui::BeginChild(XorStr("espfilter"), ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, 19 * 4));
		{
			ImGui::Selectable(XorStr(" Team"), &GlobalVars.Settings.ESP.Team);
			ImGui::Selectable(XorStr(" Enemy"), &GlobalVars.Settings.ESP.Enemy);
			ImGui::Selectable(XorStr(" Weapons"), &GlobalVars.Settings.ESP.Weapons);
			ImGui::Selectable(XorStr(" Bomb"), &GlobalVars.Settings.ESP.BombTimer);
		}
		ImGui::EndChild();
	
	
		ImGui::Checkbox(XorStr("Enable Glow"), &GlobalVars.Enabled.Glow);
		ImGui::Separator();
		ImGui::Text(XorStr("Filter Glow"));
		ImGui::Separator();
		ImGui::BeginChild(XorStr("glowfilter"), ImVec2(ImGui::GetWindowContentRegionWidth() * 0.5f, 19 * 4));
		{
			ImGui::Selectable(XorStr(" Team"), &GlobalVars.Settings.ESP.Glow.Teammates);
			ImGui::Selectable(XorStr(" Enemy"), &GlobalVars.Settings.ESP.Glow.Enemies);
			ImGui::Selectable(XorStr(" Weapons"), &GlobalVars.Settings.ESP.Glow.Weapons);
			ImGui::Selectable(XorStr(" C4"), &GlobalVars.Settings.ESP.Glow.C4);
		}
		ImGui::EndChild();
	
}

void LegitBotTab()
{
	ImGui::Text(XorStr("LegitAim"));
	ImGui::Separator();
	ImGui::Checkbox(XorStr("Enable RCS"), &GlobalVars.Settings.Aim.RCS.Enabled);
	ImGui::Checkbox(XorStr("Enable AutoPistol"), &GlobalVars.Settings.Aim.Legit.Misc.AutoPistol);
	ImGui::Separator();
}

void MiscTab()
{
	ImGui::Separator();
	ImGui::Text(XorStr("Misc"));
	ImGui::Separator();
	ImGui::Checkbox(XorStr("Enable AutoAccept"), &GlobalVars.Enabled.AutoAccept);
	ImGui::Checkbox(XorStr("Enable BunnyHop"), &GlobalVars.Enabled.BHop);
	ImGui::Checkbox(XorStr("Enable Chat"), &GlobalVars.Enabled.ChatSpam);
	ImGui::Checkbox(XorStr("Reveal Ranks In Scoreboard"), &GlobalVars.Settings.Misc.RevealRanks);
	ImGui::Checkbox(XorStr("Enable AirStuck"), &GlobalVars.Settings.Misc.Airstuck);
	ImGui::Combo(XorStr("Airstuck Key"), &GlobalVars.Settings.Misc.AirStuckKey, keyNames, ARRAYSIZE(keyNames));
	ImGui::Checkbox(XorStr("Enable ChatESP"), &GlobalVars.Settings.Misc.ChatESP.Enabled);
	ImGui::Checkbox(XorStr("Enable Surfing"), &GlobalVars.Settings.Misc.Memewalk);

	ImGui::Separator();
	ImGui::Text(XorStr("Spoofing"));
	ImGui::Checkbox(XorStr("Allow ThirdPerson"), &GlobalVars.Settings.Misc.Spoof.thirdperson);
	ImGui::Checkbox(XorStr("Enable sv_cheats"), &GlobalVars.Settings.Misc.Spoof.cheats);
	ImGui::Checkbox(XorStr("Enable gray"), &GlobalVars.Settings.Misc.Spoof.mat_grey);
	ImGui::Checkbox(XorStr("Enable LSD"), &GlobalVars.Settings.Misc.Spoof.matmip);
	ImGui::Checkbox(XorStr("Enable lowres textures"), &GlobalVars.Settings.Misc.Spoof.mat_lowres);
	ImGui::Separator();
	ImGui::Text(XorStr("Fakelag"));
	ImGui::Checkbox(XorStr("Enabled"), &GlobalVars.Settings.Misc.Fakelag.Enabled);
	ImGui::Separator();
	if (ImGui::Button(XorStr("Break Killfeed"))) Utils::ChangeName(XorStr("\n"));
	ImGui::PushItemWidth(150);
	static char buf1[128] = ""; ImGui::InputText(XorStr("##Name"), buf1, 128);
	ImGui::SameLine();
	ImGui::PushItemWidth(50);
	if (ImGui::Button(XorStr("Change Name"))) Utils::ChangeName(buf1);
	ImGui::Separator();  
}

void SkinsTab()
{
	ImGui::Separator();
	ImGui::Text(XorStr("Skins"));
	ImGui::Checkbox(XorStr("Enabled"), &GlobalVars.Enabled.Skins);
	ImGui::Separator();
}

void RenderTabs()
{
	switch (CurrentTab)
	{
	case 0:
		DefaultTab(); break;
	case 1: 
		RagebotTab(); break;
	case 2:
		VisualsTab(); break;
	case 3:
		ESPTab(); break;
	case 4:
		LegitBotTab(); break;
	case 5:
		MiscTab(); break;
	case 6:
		SkinsTab(); break;
	}
}

