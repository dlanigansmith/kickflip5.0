#pragma once
#include "includes.hpp"
#include "SpoofedConvar.hpp"
#include "Utilities.hpp"

#ifndef cSpooooof
#define cSpooooof

class cSpoof
{
public:
	 SpoofedConvar* SpoofedCHEATS;
	 SourceEngine::ConVar* svCheats;

	 SourceEngine::ConVar* sv_allow_thirdperson;
	 SpoofedConvar* SpoofedALLOWTP;

	 SourceEngine::ConVar* mat_drawgray;
     SpoofedConvar* SpoofedMATGRAY;

	 SourceEngine::ConVar* mat_showmips;
	 SpoofedConvar* SpoofedMIPSMAPS;
	 
	 SpoofedConvar* SpoofedLOWRES;
	 SourceEngine::ConVar* mat_lowres;
	 //----//
	void Update()
	{
		svCheats = SourceEngine::Interfaces::CVar()->FindVar("sv_cheats"); 
		SpoofedCHEATS = new SpoofedConvar("sv_cheats");

		sv_allow_thirdperson = SourceEngine::Interfaces::CVar()->FindVar("sv_cheats");
		SpoofedALLOWTP = new SpoofedConvar("sv_allow_thirdperson");
		

		mat_drawgray = SourceEngine::Interfaces::CVar()->FindVar("mat_drawgray");
		SpoofedMATGRAY = new SpoofedConvar("mat_drawgray");

		mat_showmips = SourceEngine::Interfaces::CVar()->FindVar("mat_showmiplevels");
		SpoofedMIPSMAPS = new SpoofedConvar("mat_showmiplevels");
		
		mat_lowres = SourceEngine::Interfaces::CVar()->FindVar("mat_showlowresimage");
		SpoofedLOWRES = new SpoofedConvar("mat_showlowresimage");
	}
	void Run()
	{
		if (GlobalVars.Settings.Misc.Spoof.cheats && (SpoofedCHEATS->GetInt() == 0))
		{
			SpoofedCHEATS->SetInt(1);
		}
		else if (!GlobalVars.Settings.Misc.Spoof.cheats && (SpoofedCHEATS->GetInt() == 1))
		{
			SpoofedCHEATS->SetInt(0);
		}

		if (GlobalVars.Settings.Misc.Spoof.thirdperson && (SpoofedALLOWTP->GetInt() == 0))
		{
			SpoofedALLOWTP->SetInt(1);
		}
		else if (!GlobalVars.Settings.Misc.Spoof.thirdperson && (SpoofedALLOWTP->GetInt() == 1))
		{
			SpoofedALLOWTP->SetInt(0);
		}

		if (GlobalVars.Settings.Misc.Spoof.mat_grey && (SpoofedMATGRAY->GetInt() == 0))
		{
			SpoofedMATGRAY->SetInt(1);
		}
		else if (!GlobalVars.Settings.Misc.Spoof.mat_grey && (SpoofedMATGRAY->GetInt() == 1))
		{
			SpoofedMATGRAY->SetInt(0);
		}

		if (GlobalVars.Settings.Misc.Spoof.matmip && (SpoofedMIPSMAPS->GetInt() == 0))
		{
			SpoofedMIPSMAPS->SetInt(1);
		}
		else if (!GlobalVars.Settings.Misc.Spoof.matmip && (SpoofedMIPSMAPS->GetInt() == 1))
		{
			SpoofedMIPSMAPS->SetInt(0);
		}
		
		if (GlobalVars.Settings.Misc.Spoof.mat_lowres && (SpoofedLOWRES->GetInt() == 0))
		{
			SpoofedLOWRES->SetInt(1);
		}
		else if (!GlobalVars.Settings.Misc.Spoof.mat_lowres && (SpoofedLOWRES->GetInt() == 1))
		{
			SpoofedLOWRES->SetInt(0);
		}
	}


};
extern cSpoof* Spoof;
#endif // !cSpooooof
