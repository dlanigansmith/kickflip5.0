#include "AntiAim.hpp"
SourceEngine::QAngle temp;
SourceEngine::QAngle LastAngle;
void AntiAim::Run()
{
	
	
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	if (!pLocal || !pLocal->GetWeapon())
		return;
	if (pLocal->GetMoveType() == SourceEngine::MOVETYPE_LADDER || pLocal->GetMoveType() == SourceEngine::MOVETYPE_NOCLIP)
		return;
	static bool flip = false;
	flip = !flip;
	CFixMove AntiAimFix;
	AntiAimFix.Start();
	temp = UserCMD->viewangles;
	if (GlobalVars.Settings.Anti.Spin && !(UserCMD->buttons & IN_ATTACK))
	{

		
		float server_time = pLocal->GetTickBase() * SourceEngine::Interfaces::GlobalVars()->interval_per_tick;
		switch (GlobalVars.Settings.Anti.SpinSpeed)
		{
		case 0:
			temp.y = (float)(fmod(server_time / 2.f * 360.0f, 360.0f));
			break;
		case 1:
			temp.y = (float)(fmod(server_time / 1.5f * 360.0f, 360.0f));
			break;
		case 2:
			temp.y = (float)(fmod(server_time / 1.f * 360.0f, 360.0f));
			break;
		case 3:
			temp.y = (float)(fmod(server_time / 0.5f * 360.0f, 360.0f));
			break;
		case 4:
			temp.y = (float)(fmod(server_time / 0.1f * 360.0f, 360.0f));
			break;
		case 5:
			temp.y = (float)(fmod(server_time / 0.0001f * 360.0f, 360.0f));
			break;
		}
		
		if (GlobalVars.Settings.Aim.Rage.AimStep)
		{
			SourceEngine::QAngle angs;
			Utils::Aimstep(LastAngle, temp, angs, 32);
			LastAngle = angs;
			UserCMD->viewangles.y = angs.y;

		}
		else
		{
			UserCMD->viewangles.y = temp.y;
		}
		
		
	}
	
	AntiAimFix.End();
}