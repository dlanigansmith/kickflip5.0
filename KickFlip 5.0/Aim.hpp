#pragma once
#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"


#ifndef AIMDEF
#define AIMDEF
namespace Aim
{
	void RageBot();
	void LegitBot();
	void Trigger();
	void RCS();
	void AutoPistol();
};
#endif // !AIMDEF

