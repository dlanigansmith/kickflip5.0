#include "Aim.hpp"

float LowestFov;
int BestTarget;
float deltaTime;
float curAimTime;
C_CSPlayer* BestEntity;

void GetNewTarget()
{
	using namespace SourceEngine;
	auto pLocal = C_CSPlayer::GetLocalPlayer();
	for (int i = 1; i < 65; i++)
	{

		if (i == Interfaces::Engine()->GetLocalPlayer())
			continue;
		auto gEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(i));
		if (!gEntity)
			continue;
		if (!(gEntity->GetClientClass()->m_ClassID == SourceEngine::EClassIds::CCSPlayer))
			continue;
		if (!gEntity->IsAlive() || gEntity->IsDormant() || !gEntity->GetImmune())
			continue;
		if (gEntity->GetTeamNum() == pLocal->GetTeamNum())
			continue;
		if (!(gEntity->GetHealth() > 0))
			continue;
		SourceEngine::Vector bone = Utils::GetEntityBone(gEntity, SourceEngine::ECSPlayerBones::head_0);
		if (!gEntity->IsVisible(SourceEngine::ECSPlayerBones::head_0))
			continue;
		float fov = M::GetFov(UserCMD->viewangles, M::CalcAngle(pLocal->GetEyePos(), bone));

		if (fov < LowestFov)
		{
			LowestFov = fov;
			BestTarget = i;
		}
	}
}
void Aim::LegitBot()
{

	using namespace SourceEngine;
	if (GlobalVars.Settings.Aim.Aimlock.Enabled && ((GlobalVars.Settings.Aim.Aimlock.AlwaysOn) || vecPressedKeys[GlobalVars.Settings.Aim.Aimlock.Key]))
	{
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		static float oldServerTime = pLocal->GetTickBase() * Interfaces::GlobalVars()->interval_per_tick;
		float serverTime = pLocal->GetTickBase() * Interfaces::GlobalVars()->interval_per_tick;
		deltaTime = serverTime - oldServerTime;
		oldServerTime = serverTime;

		GetNewTarget();

		BestEntity = static_cast<C_CSPlayer*>(Interfaces::EntityList()->GetClientEntity(BestTarget));
		if (!BestEntity)
			return;
		if (BestTarget != -1 && BestEntity->GetHealth() > 0)
		{

			Aimbotting = true;
			Vector predicted = BestEntity->GetPredicted(Utils::GetEntityBone(BestEntity, SourceEngine::ECSPlayerBones::head_0));

			QAngle dst = M::CalcAngle(pLocal->GetEyePos(), predicted);
			QAngle src = UserCMD->viewangles;

			dst -= *pLocal->AimPunch() * (GlobalVars.Settings.Aim.Aimlock.RCSAmount / 50.f);

			QAngle delta = dst - src;

			Utils::Clamp(delta);

			if (!delta.IsZero())
			{
				float finalTime = delta.Length() / (GlobalVars.Settings.Aim.Aimlock.Speed / 10);

				curAimTime += deltaTime;

				if (curAimTime > finalTime)
					curAimTime = finalTime;

				float percent = curAimTime / finalTime;

				delta *= percent;
				dst = src + delta;
			}
			Utils::Clamp(dst);
			UserCMD->viewangles = dst;


		}
		else if (BestTarget != -1)
		{
			BestTarget = -1;
			curAimTime = 0.f;
			LowestFov = GlobalVars.Settings.Aim.Aimlock.FOVLimit;
			BestEntity = nullptr;
		}
	}
	else
	{
		BestTarget = -1;
		curAimTime = 0.f;
		LowestFov = GlobalVars.Settings.Aim.Aimlock.FOVLimit;
		BestEntity = nullptr;
	}
	return;
}
