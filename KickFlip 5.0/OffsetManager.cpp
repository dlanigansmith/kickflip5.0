#include "OffsetManager.hpp"
#include "Utilities.hpp"
cOffsets* Offsets = new cOffsets;

void cOffsets::Dump()
{
	/*
	m_iHealth = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_iHealth"));
	m_iTeamNum = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_iTeamNum"));
	m_ArmorValue = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_ArmorValue"));
	m_bHasHelmet = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_bHasHelmet"));
	m_iClass = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_iClass"));
	m_bDormant = 0xE9;
	m_bGunGameImmunity = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_bGunGameImmunity"));
	m_lifeState = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_lifeState"));
	m_fFlags = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_fFlags"));
	m_Local = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_Local"));
	m_nTickBase = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_nTickBase"));
	m_nForceBone = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_nForceBone"));
	m_angEyeAngles = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_angEyeAngles"));
	m_flFlashDuration = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_flFlashDuration"));
	m_iGlowIndex = m_flFlashDuration + 0x18;
	m_mBoneMatrix = m_nForceBone + 0x1C;
	m_nModelIndex = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_nModelIndex"));
	m_viewPunchAngle = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_viewPunchAngle"));
	m_aimPunchAngle = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_aimPunchAngle"));
	m_vecOrigin = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_vecOrigin"));
	m_vecViewOffset = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_vecViewOffset[0]"));
	m_vecVelocity = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_vecVelocity[0]"));
	m_szLastPlaceName = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_szLastPlaceName"));
	m_hActiveWeapon = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_hActiveWeapon"));
	m_fAccuracyPenalty = GET_NETVAR(XorStr("DT_WeaponCSBase"), XorStr("m_fAccuracyPenalty"));
	m_Collision = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_Collision"));
	m_CollisionGroup = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_CollisionGroup"));
	m_iShotsFired = GET_NETVAR(XorStr("DT_CSPlayer"), XorStr("m_iShotsFired"));
	m_iWeaponID = GET_NETVAR(XorStr("DT_WeaponCSBase"), XorStr("m_fAccuracyPenalty")) + 0x2C;


	m_nHitboxSet = GET_NETVAR(XorStr("DT_BasePlayer"), XorStr("m_nHitboxSet"));
	m_bIsBroken = GET_NETVAR(XorStr("DT_BreakableSurface"), XorStr("m_bIsBroken"));
	m_flC4Blow = GET_NETVAR(XorStr("DT_PlantedC4"), XorStr("m_flC4Blow"));

	m_bReloadVisuallyComplete = GET_NETVAR(XorStr("DT_WeaponCSBase"), XorStr("m_bReloadVisuallyComplete"));
	m_flNextPrimaryAttack = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_flNextPrimaryAttack"));
	m_nFallbackPaintKit = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_nFallbackPaintKit"));
	m_nFallbackSeed = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_nFallbackSeed"));
	m_flFallbackWear = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_flFallbackWear"));
	m_nFallbackStatTrak = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_nFallbackStatTrak"));
	m_AttributeManager = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_AttributeManager"));
	m_Item = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_Item"));
	m_iEntityLevel = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iEntityLevel"));
	m_iItemIDHigh = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iItemIDHigh"));
	m_iItemIDLow = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iItemIDLow"));
	m_iAccountID = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iAccountID"));
	m_iEntityQuality = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iEntityQuality"));
	m_iClip1 = GET_NETVAR(XorStr("DT_BaseCombatWeapon"), XorStr("m_iClip1"));
	m_OriginalOwnerXuidLow = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_OriginalOwnerXuidLow"));
	m_OriginalOwnerXuidHigh = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_OriginalOwnerXuidHigh"));
	m_iItemDefinitionIndex = GET_NETVAR(XorStr("DT_BaseAttributableItem"), XorStr("m_iItemDefinitionIndex"));
	*/
	m_hOwnerEntity = GET_NETVAR(XorStr("DT_BaseEntity"), XorStr("m_hOwnerEntity"));
	m_nMoveType = 0x258; //if I change stuff will break
	m_flC4Blow = GET_NETVAR(XorStr("DT_PlantedC4"), XorStr("m_flC4Blow"));
	//sigs
	GlowManager = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("F3 ?? ?? ?? ?? ?? ?? ?? 83 C8 01 C7 05")) + 0x4);
	d3d9Device = **(DWORD**)(Utils::FindSignature(XorStr("shaderapidx9.dll"), XorStr("A1 ? ? ? ? 8D 57 08 6A 00 52 FF 75 FC 8B 08")) + 1);
	ServerRankRevealAllEx = Utils::FindSignature(XorStr("client.dll"), XorStr("55 8B EC 8B 0D ? ? ? ? 68 ? ? ? ? "));

	InitKeyValuesEx = Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 51 33 C0 C7 45"));
	LoadFromBufferEx = Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 34 53 8B 5D 0C 89 4C 24 04"));
	m_vStickerMaterialReferences = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 56 8B F1 8B 4D 0C 57")) + 0x2);
	KeyValuesConstructor = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("E8 ? ? ? ? EB 02 33 C0 57 8B BE")) + 0x0);
	KeyValuesLoadFromBuffer = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 34 53 8B 5D 0C 89")) + 0x0);
	KeyValuesLoadFromFile = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 14 53 8B 5D 0C 56 8B 75 08 57 FF 75 10")) + 0x0);
	KeyValuesFindKey = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 83 EC 1C 53 8B D9 85 DB")) + 0x0);
	KeyValuesMergeFrom = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 53 8B D9 56 85 DB")) + 0x0);

	KeyValuesSystemOfst = (DWORD)GetProcAddress(GetModuleHandle("vstdlib.dll"), "KeyValuesSystem");
	GetIconItemView = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("8B 86 ? ? ? ? 53 8D 9E")) + 0x2);
	GetItemSchema = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("A1 ? ? ? ? 85 C0 75 43")) + 0x0);
	HashStringCaseless = *(DWORD*)(Utils::FindPattern(XorStr("client.dll"), XorStr("53 56 8B F1 33 DB 57 33 FF 0F B6 06")) + 0x0);


}

