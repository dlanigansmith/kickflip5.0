#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"

void __stdcall Hooks::Hooked_PlaySound(const char* szFileName)
{
	//Call original PlaySound
	g_fnOriginalPlaySound(SourceEngine::Interfaces::MatSurface(), szFileName);

	if (!GlobalVars.Enabled.AutoAccept || SourceEngine::Interfaces::Engine()->IsInGame()) return;

	//This is the beep sound that is played when we have found a game
	if (!strcmp(szFileName, "weapons/hegrenade/beep.wav")) 
	{
		//This is the function that is called when you press the big ACCEPT button
		static auto IsReady = reinterpret_cast<void(__cdecl*)()>(Utils::FindSignature(XorStr("client.dll"), XorStr("55 8B EC 51 56 8B 35 ? ? ? ? 8B 4E 58")));
		
		//Accept the game
		if(IsReady)
			IsReady();

		//This will flash the CSGO window on the taskbar
		//so we know a game was found (you cant hear the beep sometimes cause it auto-accepts too fast)
		//PlaySound(TEXT("SystemStart"), NULL, SND_ALIAS);
		FLASHWINFO fi;
		fi.cbSize = sizeof(FLASHWINFO);
		fi.hwnd = g_hWindow;
		fi.dwFlags = FLASHW_ALL | FLASHW_TIMERNOFG;
		fi.uCount = 0;
		fi.dwTimeout = 0;
		FlashWindowEx(&fi);
	}
} 