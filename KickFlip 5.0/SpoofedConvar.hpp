#pragma once

#include "../KickFlip 5.0/SourceEngine/SDK.hpp"

class SpoofedConvar 
{
public:
	SpoofedConvar(const char* szCVar);
	SpoofedConvar(SourceEngine::ConVar* pCVar);

	~SpoofedConvar();

	bool           IsSpoofed();
	void           Spoof();

	void           SetFlags(int flags);
	int            GetFlags();

	void           SetBool(bool bValue);
	void           SetInt(int iValue);
	void           SetFloat(float flValue);
	void           SetString(const char* szValue);

	int            GetInt();
	float          GetFloat();
	const char*    GetString();

private:
	SourceEngine::ConVar* m_pOriginalCVar = nullptr;
	SourceEngine::ConVar* m_pDummyCVar = nullptr;

	char m_szDummyName[128];
	char m_szDummyValue[128];
	char m_szOriginalName[128];
	char m_szOriginalValue[128];
	int m_iOriginalFlags;
};