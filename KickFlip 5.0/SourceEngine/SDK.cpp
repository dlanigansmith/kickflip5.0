#include "SDK.hpp"

#include "../Utilities.hpp"
#include "../XorStr.hpp"

namespace SourceEngine 
{
     
     IVEngineClient*     Interfaces::m_pEngine           = nullptr;
	 IVModelInfoClient*  Interfaces::m_pModelInfo        = nullptr;
     IBaseClientDLL*     Interfaces::m_pClient           = nullptr;
     IClientEntityList*  Interfaces::m_pEntityList       = nullptr;
     CGlobalVarsBase*    Interfaces::m_pGlobals          = nullptr;
     IPanel*             Interfaces::m_pVGuiPanel        = nullptr;
     ISurface*           Interfaces::m_pVGuiSurface      = nullptr;
     CInput*             Interfaces::m_pInput            = nullptr;
     IEngineTrace*       Interfaces::m_pEngineTrace      = nullptr;
     ICvar*              Interfaces::m_pCVar             = nullptr;
     IClientMode*        Interfaces::m_pClientMode       = nullptr;
	 IGameEventManager2* Interfaces::m_pGameEventManager = nullptr;
	 IInputSystem*       Interfaces::m_pInputSystem      = nullptr;
	 IVModelRender*		 Interfaces::m_IVModelRender     = nullptr;
	 IMaterialSystem*	 Interfaces::m_MaterialSystem    = nullptr;
	 IVRenderView*		 Interfaces::m_IVRenderView      = nullptr;
     CreateInterfaceFn GetFactory( HMODULE hMod ) {
          return( CreateInterfaceFn )GetProcAddress( hMod, XorStr("CreateInterface"));
     }

     template<typename T>
     T* CaptureInterface( CreateInterfaceFn f, const char* szInterfaceVersion ) {
          return (T*)f( szInterfaceVersion, NULL );
     }

     IVEngineClient* Interfaces::Engine() {
          if(!m_pEngine) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
               m_pEngine = CaptureInterface<IVEngineClient>(pfnFactory, XorStr("VEngineClient013"));
          }
          return m_pEngine;
     }
	 IVModelInfoClient* Interfaces::ModelInfo() {
		 if (!m_pModelInfo) {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
			 m_pModelInfo = CaptureInterface<IVModelInfoClient>(pfnFactory, XorStr("VModelInfoClient004"));
		 }
		 return m_pModelInfo;
	 }
     IBaseClientDLL* Interfaces::Client() {
          if(!m_pClient) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("client.dll")));
               m_pClient = CaptureInterface<IBaseClientDLL>(pfnFactory, XorStr("VClient017"));
          }
          return m_pClient;
     }
     IClientEntityList* Interfaces::EntityList() {
          if(!m_pEntityList) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("client.dll")));
               m_pEntityList = CaptureInterface<IClientEntityList>(pfnFactory, XorStr("VClientEntityList003"));
          }
          return m_pEntityList;
     }
	 IGameEventManager2* Interfaces::GameEventManager() {
		 if (!m_pGameEventManager) {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
			 m_pGameEventManager = CaptureInterface<IGameEventManager2>(pfnFactory, XorStr("GAMEEVENTSMANAGER002"));
		 }
		 return m_pGameEventManager;
	 }
     CGlobalVarsBase* Interfaces::GlobalVars() {
          if(!m_pGlobals) {
               auto uAddress = Utils::FindSignature(XorStr("client.dll"), XorStr("A1 ? ? ? ? 5F 8B 40 10"));
               uint32_t g_dwGlobalVarsBase = *(uint32_t*)(uAddress + 0x1);
               m_pGlobals = *(CGlobalVarsBase**)(g_dwGlobalVarsBase);

          }
          return m_pGlobals;
     }
     IPanel* Interfaces::VGUIPanel() {
          if(!m_pVGuiPanel) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("vgui2.dll")));
               m_pVGuiPanel = CaptureInterface<IPanel>(pfnFactory, XorStr("VGUI_Panel009"));
          }
          return m_pVGuiPanel;
     }
     ISurface* Interfaces::MatSurface() {
          if(!m_pVGuiSurface) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("vguimatsurface.dll")));
               m_pVGuiSurface = CaptureInterface<ISurface>(pfnFactory, XorStr("VGUI_Surface031"));
          }
          return m_pVGuiSurface;
     }
	 IMaterialSystem* Interfaces::MaterialSystem() {
		 if (!m_MaterialSystem) {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("materialsystem.dll")));
			 m_MaterialSystem = CaptureInterface<IMaterialSystem>(pfnFactory, XorStr("VMaterialSystem080"));
		 }
		 return m_MaterialSystem;
	 }
	 IVRenderView* Interfaces::RenderView()
	 {
		 if (!m_IVRenderView)
		 {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
			 m_IVRenderView = CaptureInterface<IVRenderView>(pfnFactory, XorStr("VEngineRenderView014"));
		 }
		 return m_IVRenderView;
	 }
     CInput* Interfaces::Input() {
          if(!m_pInput) {
               auto pClientVFTable = *(uint32_t**)Client();
               m_pInput = *(CInput**)(pClientVFTable[15] + 0x1);
          }
          return m_pInput;
     }
     IEngineTrace* Interfaces::EngineTrace() {
          if(!m_pEngineTrace) {
               CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
               m_pEngineTrace = CaptureInterface<IEngineTrace>(pfnFactory, XorStr("EngineTraceClient004"));
          }
          return m_pEngineTrace;
     }
	 IInputSystem* Interfaces::InputSystem() {
		 if (!m_pInputSystem) {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("inputsystem.dll")));
			 m_pInputSystem = CaptureInterface<IInputSystem>(pfnFactory, XorStr("InputSystemVersion001"));
		 }
		 return m_pInputSystem;
	 }
	 IVModelRender * Interfaces::ModelRender()
	 {
		 if (!m_IVModelRender)
		 {
			 CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("engine.dll")));
			 m_IVModelRender = CaptureInterface<IVModelRender>(pfnFactory, XorStr("VEngineModel016"));
		 }
		 return m_IVModelRender;
	 }
     ICvar* Interfaces::CVar() {
          if(!m_pCVar) {
               SourceEngine::CreateInterfaceFn pfnFactory = GetFactory(GetModuleHandleA(XorStr("vstdlib.dll")));
               m_pCVar = CaptureInterface<SourceEngine::ICvar>(pfnFactory, XorStr("VEngineCvar007"));
          }
          return m_pCVar;
     }
     IClientMode* Interfaces::ClientMode() {
          if(!m_pClientMode) {
               auto uAddress = *(DWORD*)(Utils::FindSignature(XorStr("client.dll"), XorStr("8B 35 ? ? ? ? 85 FF 74 73")) + 2);
               m_pClientMode = *(IClientMode**)(uAddress);
          }
          return m_pClientMode;
     }

}

SourceEngine::IMaterial* visible_tex = nullptr;
SourceEngine::IMaterial* hidden_tex = nullptr;
SourceEngine::IMaterial* wireframe_tex = nullptr;