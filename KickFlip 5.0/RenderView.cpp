#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "SourceEngine\SDK.hpp"

void __fastcall Hooks::Hooked_RenderView(void* thisptr, void*, CViewSetup &setup, CViewSetup &hudViewSetup, int nClearFlags, int whatToDraw)
{

	setup.fovViewmodel = 90.f; //Sets the viewmodel to 90
	g_fnOriginalRenderView(thisptr, setup, hudViewSetup, nClearFlags, whatToDraw); 
}