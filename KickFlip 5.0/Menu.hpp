#pragma once
const char* SpinSpeeds[] =
{
	XorStr("Incredibly Slow"),
	XorStr("Slow"),
	XorStr("Medium"),
	XorStr("Fast"),
	XorStr("Insane"),
	XorStr("Ludacris")
};
const char* ESPTypes[] =
{
	XorStr("Box"),
	XorStr("Corner"),
	XorStr("Bone")
};

const char* ChamColours[] =
{
	XorStr("Red"),
	XorStr("Green"),
	XorStr("Blue"),
	XorStr("Black"),
	XorStr("White")
};

const char* WeaponConditions[] =
{
	XorStr("Battle-Scared"),
	XorStr("Well-Worn"),
	XorStr("Field-Tested"),
	XorStr("Minimal Wear"),
	XorStr("Factory New")
};

const char* aimBones[] =
{
	XorStr("Pelvis"),
	XorStr("Lower Body"),
	XorStr("Crotch"),
	XorStr("Stomach"),
	XorStr("Chest"),
	XorStr("Neck"),
	XorStr("Head")
};
const char* keyNames[] =
{
	XorStr(""),
	XorStr("Mouse 1"),
	XorStr("Mouse 2"),
	XorStr("Cancel"),
	XorStr("Middle Mouse"),
	XorStr("Mouse 4"),
	XorStr("Mouse 5"),
	XorStr(""),
	XorStr("Backspace"),
	XorStr("Tab"),
	XorStr(""),
	XorStr(""),
	XorStr("Clear"),
	XorStr("Enter"),
	XorStr(""),
	XorStr(""),
	XorStr("Shift"),
	XorStr("Control"),
	XorStr("Alt"),
	XorStr("Pause"),
	XorStr("Caps"),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr("Escape"),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr("Space"),
	XorStr("Page Up"),
	XorStr("Page Down"),
	XorStr("End"),
	XorStr("Home"),
	XorStr("Left"),
	XorStr("Up"),
	XorStr("Right"),
	XorStr("Down"),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr("Print"),
	XorStr("Insert"),
	XorStr("Delete"),
	XorStr(""),
	XorStr("0"),
	XorStr("1"),
	XorStr("2"),
	XorStr("3"),
	XorStr("4"),
	XorStr("5"),
	XorStr("6"),
	XorStr("7"),
	XorStr("8"),
	XorStr("9"),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr("A"),
	XorStr("B"),
	XorStr("C"),
	XorStr("D"),
	XorStr("E"),
	XorStr("F"),
	XorStr("G"),
	XorStr("H"),
	XorStr("I"),
	XorStr("J"),
	XorStr("K"),
	XorStr("L"),
	XorStr("M"),
	XorStr("N"),
	XorStr("O"),
	XorStr("P"),
	XorStr("Q"),
	XorStr("R"),
	XorStr("S"),
	XorStr("T"),
	XorStr("U"),
	XorStr("V"),
	XorStr("W"),
	XorStr("X"),
	XorStr("Y"),
	XorStr("Z"),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr(""),
	XorStr("Numpad 0"),
	XorStr("Numpad 1"),
	XorStr("Numpad 2"),
	XorStr("Numpad 3"),
	XorStr("Numpad 4"),
	XorStr("Numpad 5"),
	XorStr("Numpad 6"),
	XorStr("Numpad 7"),
	XorStr("Numpad 8"),
	XorStr("Numpad 9"),
	XorStr("Multiply"),
	XorStr("Add"),
	XorStr(""),
	XorStr("Subtract"),
	XorStr("Decimal"),
	XorStr("Divide"),
	XorStr("F1"),
	XorStr("F2"),
	XorStr("F3"),
	XorStr("F4"),
	XorStr("F5"),
	XorStr("F6"),
	XorStr("F7"),
	XorStr("F8"),
	XorStr("F9"),
	XorStr("F10"),
	XorStr("F11"),
	XorStr("F12"),

};

//SKIN CRAP GOES HERE: THIS WILL BE FUN TO TYPE OUT! 


//GUNS
const char* Knives[]
{
	XorStr("T Knife"),
	XorStr("CT Knife"),
	XorStr("Flip Knife"),
	XorStr("Gut Knife"),
	XorStr("Karambit"),
	XorStr("M9 Bayonet"),
	XorStr("Bayonet"),
	XorStr("Huntsman"),
	XorStr("Falchion Knife"),
	XorStr("Bowie Knife"),
	XorStr("ButterFly"),
	XorStr("Shadow Daggers")
 };

const char* Rifles[]
{
	XorStr("Galil AR"), 
	XorStr("AK-47"),
	XorStr("M4A4"),
	XorStr("M4A1-S"),
	XorStr("SSG 08"),
	XorStr("SG 553"),
	XorStr("AUG"),
	XorStr("AWP"),
	XorStr("G3SG1"),
	XorStr("Scar 20")
};
const char* Pistols[]
{
	XorStr("P2000"),
	XorStr("USP-S"),
	XorStr("Dual Berettas"),
	XorStr("P250"),
	XorStr("CZ-75A"),
	XorStr("Five-SeveN"),
	XorStr("Tec-9"),
	XorStr("Desert Eagle"),
	XorStr("R8")
};
const char* SMGs[]
{
	XorStr("MP9"),
	XorStr("Mac-10"),
	XorStr("MP7"),
	XorStr("UMP-45"),
	XorStr("P90"),
	XorStr("PP-Bizon")
};
const char* Heavy[]
{
	XorStr("Nova"),
	XorStr("XM2014"),
	XorStr("Sawed-Off"),
	XorStr("Mag-7"),
	XorStr("M249"),
	XorStr("Negev")
};

//ACTUAL SKINS TY NICK


const char* M9Bayonetskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
	XorStr("Rust Coat"), //323
	XorStr("Tiger Tooth"), //409
	XorStr("Damascus Steel"), //410
	XorStr("Marble Fade "), //413
	XorStr("Doppler (Ruby)"), //415
	XorStr("Doppler (Sapphire)"), //416
	XorStr("Doppler (Blackpearl)"), //417
	XorStr("Doppler (Phase 1)"), //418
	XorStr("Doppler (Phase 2)"), //419
	XorStr("Doppler (Phase 3)"), //420
	XorStr("Doppler (Phase 4)") //421
};


const char* GutKnifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
	XorStr("Rust Coat"), //323
	XorStr("Tiger Tooth"), //409
	XorStr("Damascus Steel"), //410
	XorStr("Marble Fade "), //413
	XorStr("Doppler (Ruby)"), //415
	XorStr("Doppler (Sapphire)"), //416
	XorStr("Doppler (Blackpearl)"), //417
	XorStr("Doppler (Phase 1)"), //418
	XorStr("Doppler (Phase 2)"), //419
	XorStr("Doppler (Phase 3)"), //420
	XorStr("Doppler (Phase 4)") //421
};

const char* FlipKnifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
	XorStr("Rust Coat"), //323
	XorStr("Tiger Tooth"), //409
	XorStr("Damascus Steel"), //410
	XorStr("Marble Fade "), //413
	XorStr("Doppler (Ruby)"), //415
	XorStr("Doppler (Sapphire)"), //416
	XorStr("Doppler (Blackpearl)"), //417
	XorStr("Doppler (Phase 1)"), //418
	XorStr("Doppler (Phase 2)"), //419
	XorStr("Doppler (Phase 3)"), //420
	XorStr("Doppler (Phase 4)") //421
};


const char* Karambitskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
	XorStr("Rust Coat"), //323
	XorStr("Tiger Tooth"), //409
	XorStr("Damascus Steel"), //410
	XorStr("Marble Fade "), //413
	XorStr("Doppler (Ruby)"), //415
	XorStr("Doppler (Sapphire)"), //416
	XorStr("Doppler (Blackpearl)"), //417
	XorStr("Doppler (Phase 1)"), //418
	XorStr("Doppler (Phase 2)"), //419
	XorStr("Doppler (Phase 3)"), //420
	XorStr("Doppler (Phase 4)") //421
};

const char* Bayonetskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
	XorStr("Rust Coat"), //323
	XorStr("Tiger Tooth"), //409
	XorStr("Damascus Steel"), //410
	XorStr("Marble Fade "), //413
	XorStr("Doppler (Ruby)"), //415
	XorStr("Doppler (Sapphire)"), //416
	XorStr("Doppler (Blackpearl)"), //417
	XorStr("Doppler (Phase 1)"), //418
	XorStr("Doppler (Phase 2)"), //419
	XorStr("Doppler (Phase 3)"), //420
	XorStr("Doppler (Phase 4)") //421
};


const char* Butterflyknifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
};

const char* HuntsmanKnifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
};

const char* FalchionKnifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
};

const char* ShadowDaggerseskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
};

const char* BowieKnifeskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Blue Steel"), //42
	XorStr("Stained"), //43
	XorStr("Case Hardened"), //44
	XorStr("Slaughter"), //59
	XorStr("Safari Mesh"), //72
	XorStr("Boreal Forest"), //77
	XorStr("Ultraviolet"), //98
	XorStr("Urban Masked"), //143
	XorStr("Scorched"), //175
};

const char* AWPskins[]
{
	XorStr("Snake Camo"), //30
	XorStr("Lightning Strike"), //51
	XorStr("Pink DDPAT"), //84
	XorStr("Boom"), //174
	XorStr("Corticera"), //181
	XorStr("Graphite"), //212
	XorStr("Electric Hive"), //227
	XorStr("Pit Viper"), //251
	XorStr("Redline"), //259
	XorStr("Dragon Lore"), //344
	XorStr("Man-o'-war"), //395
	XorStr("Worm God"), //424
	XorStr("Medusa"), //446
	XorStr("Sun in Leo"), //451
	XorStr("Hyper Beast"), //475
	XorStr("Elite Build") //525
};

const char* AkSkins[]
{   XorStr("Red Laminate"), //14
	XorStr("Contrast Spray"),//22
	XorStr("Case Hardened"), //44
	XorStr("Safari Mesh"), //72
	XorStr("Jungle Spray"), //122
	XorStr("Predator"), //170
	XorStr("Black Laminate"), //172
	XorStr("Fire Serpent"), // 180
	XorStr("Blue Laminate"), //226 
	XorStr("RedLine"), //282
	XorStr("Jaguar"), //316
	XorStr("Jet Set"), //340
	XorStr("First Class"), //341
	XorStr("WasteLand Rebel"), //380
	XorStr("Elite Build"), //422
	XorStr("Hydroponic"), //456
	XorStr("Aquamarine Revenge"), //474
	XorStr("Frontside Misty"), //490
	XorStr("Fuel Injector") //524
};


const char* M4Skins[]
{	XorStr("Forest DDPAT"),//5
	XorStr("Desert Storm"), //8
	XorStr("Jungle Tiger"), //16
	XorStr("Urban DDPAT"), //17
	XorStr("Tornado"), //101
	XorStr("Bullet Rain"), //155
	XorStr("Modern Hunter"), //164
	XorStr("Radiation Hazard"), // 167
	XorStr("Predator"), //170
	XorStr("Faded Zebra"), //176
	XorStr("Zirka"), //187
	XorStr("X-Ray"), //215
	XorStr("Asiimov"), //255
	XorStr("Howl"), //309
	XorStr("Desert-Strike"), //336
	XorStr("Griffin"), //484
	XorStr("Dragon King"), //400
	XorStr("Poseidon"), //449
	XorStr("Daybreak"), //471
	XorStr("Evil Daimyo"), //480
	XorStr("Royal Paladin"), //512
	XorStr("The Battlestar") //533
};

const char* M4a1skins[]
{
	XorStr("Boreal Forest"), //77
	XorStr("Bright Water"), //189
	XorStr("VariCamo"), //235
	XorStr("Blood Tiger"), //217
	XorStr("Nitro"), //254
	XorStr("Guardian"), //257
	XorStr("Master Piece"), //321
	XorStr("Electric Hive"), //226
	XorStr("Knight"), //251
	XorStr("Atomic Alloy"), //301
	XorStr("Cyrex"), //360
	XorStr("Basilisk"), //383
	XorStr("Icarus Fell"), //440
	XorStr("Hot Rod"), //445
	XorStr("Golden Coil"), //497
	XorStr("Chantico's Fire"), //548
};


const char* FAMASskins[]
{
	XorStr("Contrast Spray"), //22
	XorStr("Colony"), //47
	XorStr("Cyanospatter"), //92
	XorStr("Afterimage"), //154
	XorStr("Doomkitty"), //178
	XorStr("Spitfire"), //194
	XorStr("Hexane"), //218
	XorStr("TearDown"), //244
	XorStr("Pulse"), //260
	XorStr("Sergeant"), //288
	XorStr("Styx"), //371
	XorStr("Djinn"), //429
	XorStr("Neural Net"), //477
	XorStr("Survivor Z"), //492
	XorStr("Valence"), //529
};

const char* Galilskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Contrast Spray"), //22
	XorStr("Winter Forest"), //76
	XorStr("Orange DDPAT"), //83
	XorStr("Sage Spray"), //119
	XorStr("Shattered"), //192
	XorStr("Blue Titanium"), //216
	XorStr("VariCamo"), //235
	XorStr("Urban Rubble"), //237
	XorStr("Hunting Blind"), //241
	XorStr("Sandstorm"), //264
	XorStr("Tuxedo"), //297
	XorStr("Cerberus"), //397
	XorStr("Chatterbox"), //398
	XorStr("Eco"), //428
	XorStr("Aqua Terrace"), //460
	XorStr("Rocket Pop"), //478
	XorStr("Stone Cold"), //494
	XorStr("Firefight"), //546
};

const char* SSG08skins[]
{
	XorStr("Lichen Dashed"), //26
	XorStr("Dark Water"), //60
	XorStr("Blue Spruce"), //96
	XorStr("Sand Dune"), //99
	XorStr("Palm"), //157
	XorStr("Mayan Dreams"), //200
	XorStr("Blood in the Water"), //222
	XorStr("Tropical Storm"), //233
	XorStr("Acid Fade"), //253
	XorStr("Slashed"), //304
	XorStr("Detour"), //319
	XorStr("Abyss"), //361
	XorStr("Necropos"), //538
	XorStr("Ghost Crusader"), //554
	XorStr("Big Iron") //503
};

const char* SG553skins[]
{
	XorStr("Contrast Spray"), //22
	XorStr("Bone Mask"), //27
	XorStr("Anodized Navy"), //28
	XorStr("Bulldozer"), //39
	XorStr("UltraViolet"), //98
	XorStr("Tornado"), //101
	XorStr("Waves Perforated"), //136
	XorStr("Fallout Warning"), //169
	XorStr("Wave Spray"), //186
	XorStr("Gator Mesh"), //243
	XorStr("Damascus Steel"), //247
	XorStr("Pulse"), //287
	XorStr("Army Sheen"), //298
	XorStr("Traveler"), //363
	XorStr("Fallout Warning"), //378
	XorStr("Cyrex"), //487
	XorStr("Tiger Moth"), //519
	XorStr("Atlas") //553

};

const char* Augskins[]
{
	XorStr("Bengal Tiger"), //9
	XorStr("Copperhead"), //10
	XorStr("Anodized Navy"), //28
	XorStr("Hot Rod"), //33
	XorStr("Contractor"), //46
	XorStr("Colony"), //47
	XorStr("Wings"), //73
	XorStr("Storm"), //100
	XorStr("Condemned"), //110
	XorStr("Radiation Hazard"), //167
	XorStr("Torque"), //305
	XorStr("Asterion"), //442
	XorStr("Daedalus"), //444
	XorStr("Akihabara Accept"), //455
	XorStr("Ricochet"), //504
	XorStr("Fleet Flock") //541
};

const char* Scar20skins[]
{
	XorStr("Contractor"), //46
	XorStr("Carbon Fiber"), //70
	XorStr("Storm"), //100
	XorStr("Sand Mesh"), //116
	XorStr("Palm"), //157
	XorStr("Splash Jam"), //165
	XorStr("Emerald"), //196
	XorStr("Crimson Web"), //232
	XorStr("Army Sheen"), //298
	XorStr("Cyrex"), //312
	XorStr("Cardiac"), //391
	XorStr("Grotto"), //406
	XorStr("Green Marine"), //502
	XorStr("Outbreak") //518
};


const char* G3SG1skins[]
{
	XorStr("Desert Storm"), //8
	XorStr("Carbon Fiber"), //6
	XorStr("Bone Mask"), //27
	XorStr("Contractor"), //46
	XorStr("Safari Mesh"), //72
	XorStr("Polar Camo"), //74
	XorStr("Jungle Dashed"), //147
	XorStr("Predator"), //170
	XorStr("Demeter"), //195
	XorStr("Azure Zebra"), //229
	XorStr("VariCamo"), //235
	XorStr("Green Apple"), //297
	XorStr("Murky"), //382
	XorStr("Chronos"), //438
	XorStr("The Executioner"), //511
	XorStr("Flux"), //493
	XorStr("Orange Crash") //545
};

const char* FiveSevenskins[]
{
	XorStr("Candy Apple"), //3
	XorStr("Bone Mask"), //27
	XorStr("Case Hardened"), //44
	XorStr("Contractor"), //46
	XorStr("Forest Night"), //78
	XorStr("Orange Peel"), //141
	XorStr("Jungle"), //151
	XorStr("Anodized Gunmetal"), //210
	XorStr("Nightshade"), //223
	XorStr("Red Quartz"), //248
	XorStr("Silver Quartz"), //252
	XorStr("Nitro"), //254
	XorStr("Kami"), //265
	XorStr("Copper Galaxy"), //274
	XorStr("Fowl Play"), //352
	XorStr("Hot Shot"), //377
	XorStr("Urban Hazard"), //387
	XorStr("Monkey Business"), //427
	XorStr("Retrobution"), //387
	XorStr("Triumvirate"), //530
};

const char* P250skins[]
{
	XorStr("Gunsmoke"), //15
	XorStr("Bone Mask"), //27
	XorStr("Metallic DDPAT"), //34
	XorStr("Boreal Forest"), //77
	XorStr("Sand Dune"), //99
	XorStr("Whiteout"), //102
	XorStr("Splash"), //162
	XorStr("Modern Hunter"), //164
	XorStr("Nuclear Threat"), //168
	XorStr("Facets"), //207
	XorStr("Hive"), //219
	XorStr("Steel Disruption"), //230
	XorStr("Undertow"), //271
	XorStr("Franklin"), //295
	XorStr("Supernova"), //358
	XorStr("Contamination"), //373
	XorStr("Cartel"), //388
	XorStr("Valence"), //426
	XorStr("Crimson Kimono"), //466
	XorStr("Mint Kimono"), //467
	XorStr("Wingshot"), //501
	XorStr("Asiimov") //551
};

const char* TEC9skins[]
{
	XorStr("Groundwater"), //2
	XorStr("Forest DDPAT"), //5
	XorStr("Urban DDPAT"), //17
	XorStr("Ossified"), //36
	XorStr("Brass"), //159
	XorStr("Nuclear Threat"), //179
	XorStr("Tornado"), //206
	XorStr("Blue Titanium"), //216
	XorStr("Army Mesh"), //242
	XorStr("Red Quartz"), //248
	XorStr("Titanium Bit"), //272
	XorStr("Sandstorm"), //289
	XorStr("Isaac"), //303
	XorStr("Toxic"), //374
	XorStr("Bamboo Forest"), //459
	XorStr("Avalanche"), //520
	XorStr("Jambiya"), //539
	XorStr("Re-Entry"), //555
};


const char* DualBerettasSkins[]
{
	XorStr("Anodized Navy"), //28
	XorStr("Ossified"), //36
	XorStr("Stained"), //43
	XorStr("Contractor"), //46
	XorStr("Colony"), //47
	XorStr("Demolition"), //153
	XorStr("Black Limba"), //190
	XorStr("Hemoglobin"), //220
	XorStr("Red Quartz"), //248
	XorStr("Cobalt Quartz"), //249
	XorStr("Marina"), //261
	XorStr("Panther"), //276
	XorStr("Retribution"), //307
	XorStr("Briar"), //330
	XorStr("Urban Shock"), //396
	XorStr("Duelist"), //447
	XorStr("Moon in Libra"), //450
	XorStr("Dualing Dragons"), //491
	XorStr("Cartel"), //528
	XorStr("Ventilators"), //544
};

const char* DeagleSkins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Crimson Web"), //12
	XorStr("Urban DDPAT"), //17
	XorStr("Blaze"), //37
	XorStr("Night"), //40
	XorStr("Hypnotic"), //61
	XorStr("Mudder"), //90
	XorStr("Golden Koi"), //185
	XorStr("Cobalt Disruption"), //231
	XorStr("VariCamo"), //235
	XorStr("Urban Rubblea"), //237
	XorStr("Heirloom"), //273
	XorStr("Meteorite"), //296
	XorStr("Hand Cannon"), //328
	XorStr("Pilot"), //347
	XorStr("Conspiracy"), //351
	XorStr("Naga"), //497
	XorStr("Bronze Deco"), //425
	XorStr("Midnight Storm"), //468
	XorStr("Sunset Storm �"), //469
	XorStr("Sunset Storm"), //470
	XorStr("Corinthian"), //509	
	XorStr("Kumicho Dragon"), //527
};

const char* Glock18Skins[]
{
	XorStr("Groundwater"), //2
	XorStr("Candy Apple"), //3
	XorStr("Fade"), //38
	XorStr("Night"), //40
	XorStr("Dragon Tattoo"), //48
	XorStr("Sand Dune"), //99
	XorStr("Brass"), //159
	XorStr("Sand Dune"), //208
	XorStr("Steel Disruption"), //230
	XorStr("Blue Fissure"), //278
	XorStr("Death Rattle"), //293
	XorStr("Water Elemental"), //353
	XorStr("Reactor"), //367
	XorStr("Grinder"), //381
	XorStr("Catacombs"), //399
	XorStr("Twilight Galaxy"), //437
	XorStr("Bunsen Burner"), //479
	XorStr("Royal Legion") //532
};


const char* USPSskins[]
{
	XorStr("Forest Leaves"), //25
	XorStr("Dark Water"), //60 
	XorStr("Overgrowth"), //183
	XorStr("Blood Tiger"), //217
	XorStr("Serum"), //221
	XorStr("VariCamo"), // 235
	XorStr("Night Ops"), //236
	XorStr("Stainless"), //277
	XorStr("Guardian"), //290
	XorStr("Orion"), //313
	XorStr("Road Rash"), //318
	XorStr("Royal Blue"), //332
	XorStr("Caiman"), //339
	XorStr("Business Class"), //364
	XorStr("Para Green"), //454
	XorStr("Torque"), //489
	XorStr("Kill Confirmed"), //504
};

const char* P2000skins[]
{
	XorStr("Granite Marbleized"), //21
	XorStr("Silver"), //32
	XorStr("Scorpion"), //71
	XorStr("Grassland"), //95
	XorStr("Grassland Leaves"), //104
	XorStr("Corticera"), //184
	XorStr("Ocean Foam"), //211
	XorStr("Amber Fade"), //246
	XorStr("Red FragCam"), //275
	XorStr("Chainmail"), //327
	XorStr("Pulse"), //338
	XorStr("Coach Class"), //346
	XorStr("Ivory"), //357
	XorStr("Fire Elemental"), //389
	XorStr("Pathfinder"), //443
	XorStr("Handgun"), //485
};

const char* CZ75skins[]
{
	XorStr("Crimson Web"), //12
	XorStr("Hexane"), //218
	XorStr("Nitro"), //254
	XorStr("Tread Plate"), //268
	XorStr("The Fuschia Is Now"), //269
	XorStr("Victoria"), //270
	XorStr("Tuxedo"), //297
	XorStr("Poison Dart"), //315
	XorStr("Chalice"), //325
	XorStr("Twist"), //334
	XorStr("Tigris"), //350
	XorStr("Green Plaid"), //366
	XorStr("Pole Position"), //435
	XorStr("Emerald"), //453
	XorStr("Yellow Jacket"), //476
	XorStr("Red Astor"), //543
};

const char* Revolvoskins[]
{
	XorStr("Fade"), //522
	XorStr("Amber Fade"), //523
	XorStr("Crimson Web"), //12
	XorStr("Bone Mask"), //27
};

//Jonathan is a queer
const char* P90skins[]
{
	XorStr("Virus"), //20
	XorStr("Contrast Spray"), //22	
	XorStr("Cold Blooded"), //67	
	XorStr("Storm"), //100			
	XorStr("Glacier Mesh"), //111
	XorStr("Sand Spray"), //124			
	XorStr("Death by Kitty"), //156	
	XorStr("Fallout Warning"), //169	
	XorStr("Scorched"), //175			
	XorStr("Emerald Dragon"), //182 //Haydo11198 fav skin	
	XorStr("Blind Spot"), //228	
	XorStr("Ash Wood"), //234
	XorStr("Teardown"), //244
	XorStr("Trigon"), //283
	XorStr("Desert Warfare"), //311
	XorStr("Module"), //335
	XorStr("Leather"), //342
	XorStr("Asiimov"), //359
	XorStr("Elite Build"), //486
	XorStr("Leather"), //543

};

const char* MP7skins[]
{
	XorStr("Groundwater"), //2
	XorStr("Forest DDPAT"), //5	
	XorStr("Skulls"), //11	
	XorStr("Gunsmoke"), //15			
	XorStr("Contrast Spray"), //22
	XorStr("Bone Mask"), //27				
	XorStr("Anodized Navy"), //28	
	XorStr("Ossified"), //36	
	XorStr("Whiteout"), //	102			
	XorStr("Orange Peel"), //182 //haydo11198 fav skin	
	XorStr("Ocean Foam"), //213	
	XorStr("VariCamo"), //235
	XorStr("Army Recon"), //245
	XorStr("Full Stop"), //250
	XorStr("Urban Hazard"), //354
	XorStr("Olive Plaid"), //365
	XorStr("Armor Core"), //423
	XorStr("Asterion"), //442
	XorStr("Nemesis"), //481
	XorStr("Special Delivery"), //500
};


const char* UMPskins[]
{
	XorStr("Forest DDPAT"), //5
	XorStr("Gunsmoke"), //15	
	XorStr("Urban DDPAT"), //17	
	XorStr("Blaze"), //37					
	XorStr("Carbon Fiber"), //70
	XorStr("Caramel"), //93				
	XorStr("Fallout Warning"), //169
	XorStr("Scorched"), //175	
	XorStr("Bone Pile"), //	193			
	XorStr("Corporal"), //281 
	XorStr("Grand Prix"), //436	
	XorStr("Indigo"), //333
	XorStr("Labyrinth"), //362
	XorStr("Delusion"), //392
	XorStr("Minotaur's Labyrinth"), //441
	XorStr("Riot"), //488
	XorStr("Primal Saber"), //556
};

const char* PPbizonSkins[]
{
	XorStr("Blue Streak"), //13		
	XorStr("Forest Leaves"), //25	
	XorStr("Bone Mask"), //27	
	XorStr("Carbon Fiber"), //70						
	XorStr("Sand Dashed"), //148
	XorStr("Urban Dashed"), //149					
	XorStr("Brass"), //159
	XorStr("Modern Hunter"), //164			
	XorStr("Irradiated Alert"), //171		
	XorStr("Rust Coat"), //203	
	XorStr("Water Sigil"), //224	
	XorStr("VariCamo"), //235
	XorStr("Night Ops"), //236
	XorStr("Cobalt Halftone"), //267
	XorStr("Antique"), //306
	XorStr("Rust Coat"), //323
	XorStr("Osiris"), //349			
	XorStr("Chemical Green"), //376
	XorStr("Bamboo Print"), //457	
	XorStr("Bamboo Forest"), //459
	XorStr("Fuel Rod"), //508
	XorStr("Judgement of Anubis"), //542
};

const char* MP9Skins[]
{
	XorStr("Bone Mask"), //27	
	XorStr("Hot Rod"), //33
	XorStr("Bulldozer"), //39	
	XorStr("Hypnotic"), //61						
	XorStr("Storm"), //100
	XorStr("Orange Peel"), //141					
	XorStr("Sand Dashed"), //148
	XorStr("Dry Season"), //199		
	XorStr("Rose Iron"), //262		
	XorStr("Dark Age"), //329	
	XorStr("Green Plaid"), //366	
	XorStr("Setting Sun"), //368
	XorStr("Dart"), //386
	XorStr("Ruby Poison Dart"), //482
	XorStr("Deadly Poison"), //403
	XorStr("Pandora's Box"), //448
	XorStr("Bioleak"), //449			
};


const char* MAC10Skins[]
{
	XorStr("Candy Apple"), //3	
	XorStr("Forest DDPAT"), //5
	XorStr("Urban DDPAT"), //17	
	XorStr("Silver"), //32						
	XorStr("Fade"), //38
	XorStr("Ultraviolet"), //98					
	XorStr("Tornado"), //101
	XorStr("Palm"), //157	
	XorStr("Graven"), //188
	XorStr("Amber Fade"), //246	
	XorStr("Heat"), //284	
	XorStr("Curse"), //310
	XorStr("Indigo"), //333
	XorStr("Tatter"), //337
	XorStr("Commuter"), //343
	XorStr("Nuclear Garden"), //372
	XorStr("Malachite"), //402			
	XorStr("Neon Rider"), //433
};


const char* NovaSkins[]
{
	XorStr("Candy Apple"), //3	
	XorStr("Forest Leaves "), //25
	XorStr("Bloomstick"), //62
	XorStr("Sand Dune"), //99				
	XorStr("Polar Mesh"), //107
	XorStr("Walnut"), //158					
	XorStr("Modern Hunter"), //164
	XorStr("Blaze Orange"), //166
	XorStr("Predator"), //170
	XorStr("Tempest"), //191	
	XorStr("Graphite"), //214
	XorStr("Ghost Camo"), //225
	XorStr("Rising Skull"), //263
	XorStr("Antique"), //286
	XorStr("Caged Steel"), //299
	XorStr("Koi"), //356
	XorStr("Moon in Libra"), //450	
	XorStr("Ranger"), //484
	XorStr("HyperBeast"), //537
};

const char* XM1014Skins[]
{
	XorStr("Bone Mask"), //27
	XorStr("Blue Steel "), //42
	XorStr("Grassland"), //95
	XorStr("Blue Spruce"), //96			
	XorStr("Urban Perforated"), //135
	XorStr("Jungle"), //151			
	XorStr("Blaze Orange"), //166
	XorStr("Fallout Warning"), //196
	XorStr("VariCamo"), //235
	XorStr("VariCamo Blue"), //238
	XorStr("CaliCamo"), //240
	XorStr("Heaven Guard"), //314
	XorStr("Red Python"), //320
	XorStr("Red Leather"), //348
	XorStr("Bone Machine"), //370
	XorStr("Tranquility"), //393
	XorStr("Quicksilver"), //407
	XorStr("Black Tie"), //557
};


const char* SawedOff[]
{
	XorStr("Forest DDPAT "), //5
	XorStr("Contrast Spray"), //22
	XorStr("Snake Camo"), //30
	XorStr("Copper"), //41	
	XorStr("Orange DDPAT"), //83
	XorStr("Sage Spray"), //119	
	XorStr("Irradiated Alert"), //171
	XorStr("Mosaico"), //204
	XorStr("VariCamo"), //235
	XorStr("Amber Fade"), //256
	XorStr("Full Stop"), //250
	XorStr("The Kraken"), //256
	XorStr("Rust Coat"), //323
	XorStr("First Class"), //345
	XorStr("Highwayman"), //390
	XorStr("Serenity"), //405
	XorStr("Origami"), //434
	XorStr("Bamboo Shadow"), //458
	XorStr("Bamboo Forest"), //459
};


const char* Mag7 []
{
	XorStr("Silver"), //32
	XorStr("Metallic DDPAT"), //34
	XorStr("Bulldozer"), //39
	XorStr("Sand Dune"), //99
	XorStr("Irradiated Alert"), //171
	XorStr("Memento"), //177		
	XorStr("Hazard"), //198
	XorStr("Heaven Guard"), //291
	XorStr("Firestarter"), //385	
	XorStr("Heat"), //431	
	XorStr("Counter Terrace"), //462
	XorStr("Seabird"), //473	
	XorStr("Cobalt Core"), //499	
	XorStr("Praetorian"), //535
};

const char* M429[]
{
	XorStr("Contrast Spray"), //22
	XorStr("Blizzard Marbleized"), //75
	XorStr("Jungle DDPAT"), //202
	XorStr("Gator Mesh "), //243
	XorStr("Magma"), //266
	XorStr("System Lock"), //401
	XorStr("Shipping Forecast"), //452			
	XorStr("Impact Drill"), //472
	XorStr("Nebula Crusader"), //496	
	XorStr("Spectre"), //547
};


const char* Negev[]
{
	XorStr("Anodized Navy"), //28
	XorStr("Palm"), //157
	XorStr("VariCamo"), //235
	XorStr("CaliCamo"), //240
	XorStr("Terrain"), //285
	XorStr("Army Sheen"), //298
	XorStr("Bratatat"), //317	
	XorStr("Desert-Strike"), //355
	XorStr("Nuclear Waste"), //369	
	XorStr("Man-o'-war"), //432
	XorStr("Loudmouth"), //483
	XorStr("Power Loader"), //514
};



