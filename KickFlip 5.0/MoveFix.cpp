#include "MoveFix.hpp"


void CFixMove::Start()
{
	m_oldangle = UserCMD->viewangles;
	m_oldforward = UserCMD->forwardmove;
	m_oldsidemove = UserCMD->sidemove;
}

void CFixMove::End()
{
	float yaw_delta = UserCMD->viewangles.y - m_oldangle.y;
	float f1;
	float f2;

	if (m_oldangle.y < 0.f)
		f1 = 360.0f + m_oldangle.y;
	else
		f1 = m_oldangle.y;

	if (UserCMD->viewangles.y < 0.0f)
		f2 = 360.0f + UserCMD->viewangles.y;
	else
		f2 = UserCMD->viewangles.y;

	if (f2 < f1)
		yaw_delta = abs(f2 - f1);
	else
		yaw_delta = 360.0f - abs(f1 - f2);
	yaw_delta = 360.0f - yaw_delta;

	UserCMD->forwardmove = cos(DEG2RAD(yaw_delta)) * m_oldforward + cos(DEG2RAD(yaw_delta + 90.f)) * m_oldsidemove;
	UserCMD->sidemove = sin(DEG2RAD(yaw_delta)) * m_oldforward + sin(DEG2RAD(yaw_delta + 90.f)) * m_oldsidemove;
}


