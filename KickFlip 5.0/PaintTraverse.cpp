#include "includes.hpp"
#include "Hooks.hpp"
#include "NetVarManager.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "ESP.hpp"
#include "CNadeTracer.hpp"

bool hasGUIInit;

void __fastcall Hooks::Hooked_PaintTraverse(void* _this, void*edx, unsigned int Panel, bool forceRepaint, bool allowForce)
{
	g_fnOriginalPaintTraverse(_this, Panel, forceRepaint, allowForce);
	static unsigned int drawPanel;
	if (!drawPanel)
	{
		const char* panelname = SourceEngine::Interfaces::VGUIPanel()->GetName(Panel);
		if (panelname[0] == 'M' && panelname[2] == 't')
			drawPanel = Panel;
	}
	if (Panel != drawPanel)
		return;
	Draw::DrawString(7, 16, 16, SourceEngine::Color::Red(), false, XorStr("KickFlipCS.com"));
	Draw::DrawPixel(1, 1, SourceEngine::Color(0, 0, 0));
	
	ESP::Run();
	
	
} 