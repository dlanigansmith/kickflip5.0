#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"

void ForceMaterial(SourceEngine::IMaterial* material, SourceEngine::Color color)
{
	if (material != NULL)
	{
		SourceEngine::Interfaces::RenderView()->SetColorModulation(color.Base());
		SourceEngine::Interfaces::ModelRender()->ForcedMaterialOverride(material);
	}
}

void __fastcall Hooks::Hooked_DrawModelExecute(void* thisptr, int edx, void* ctx, void* state, const SourceEngine::ModelRenderInfo_t &pInfo, SourceEngine::matrix3x4_t *pCustomBoneToWorld)
{
	if (SourceEngine::Interfaces::Engine()->IsInGame())
	{
		
		if (pInfo.pModel)
		{
			std::string pszModelName = SourceEngine::Interfaces::ModelInfo()->GetModelName(pInfo.pModel);
			if (GlobalVars.Settings.Visuals.Removals.Enabled)
			{
				if (pszModelName.find(XorStr("arms")) != std::string::npos && GlobalVars.Settings.Visuals.Removals.NoHands)
				{
					SourceEngine::IMaterial* Hands = SourceEngine::Interfaces::MaterialSystem()->FindMaterial(pszModelName.c_str(), TEXTURE_GROUP_MODEL);
					Hands->SetMaterialVarFlag(SourceEngine::MATERIAL_VAR_NO_DRAW, true);
					SourceEngine::Interfaces::ModelRender()->ForcedMaterialOverride(Hands);
				}
				else if (pszModelName.find(XorStr("weapon")) != std::string::npos && GlobalVars.Settings.Visuals.Removals.NoWeapon)
				{
					if (!(pszModelName.find(XorStr("arms")) != std::string::npos))
					{
						SourceEngine::IMaterial* Weapon = SourceEngine::Interfaces::MaterialSystem()->FindMaterial(pszModelName.c_str(), TEXTURE_GROUP_MODEL);
						Weapon->SetMaterialVarFlag(SourceEngine::MATERIAL_VAR_NO_DRAW, true);
						SourceEngine::Interfaces::ModelRender()->ForcedMaterialOverride(Weapon);
					}
				}
			}
			if (pInfo.pModel && (GlobalVars.Settings.Visuals.Chams.Enabled))
			{
				if (pszModelName.find(XorStr("models/player")) != std::string::npos)
				{
					auto pLocal = C_CSPlayer::GetLocalPlayer();
					C_CSPlayer* gModel = (C_CSPlayer*)SourceEngine::Interfaces::EntityList()->GetClientEntity(pInfo.entity_index);
					if (pLocal && gModel && gModel->IsAlive() && !gModel->IsDormant())
					{
						//extremely long line of code that sets the appropiate color for the ent x2
						SourceEngine::Color renderHidden = gModel->GetTeamNum() == pLocal->GetTeamNum() ? SourceEngine::Color(GlobalVars.Settings.Visuals.Chams.TeamColHidden.r, GlobalVars.Settings.Visuals.Chams.TeamColHidden.g, GlobalVars.Settings.Visuals.Chams.TeamColHidden.b) : SourceEngine::Color(GlobalVars.Settings.Visuals.Chams.EnemyColHidden.r, GlobalVars.Settings.Visuals.Chams.EnemyColHidden.g, GlobalVars.Settings.Visuals.Chams.EnemyColHidden.b);
						SourceEngine::Color renderVis = gModel->GetTeamNum() == pLocal->GetTeamNum() ? SourceEngine::Color(GlobalVars.Settings.Visuals.Chams.TeamColVis.r, GlobalVars.Settings.Visuals.Chams.TeamColVis.g, GlobalVars.Settings.Visuals.Chams.TeamColVis.b) : SourceEngine::Color(GlobalVars.Settings.Visuals.Chams.EnemyColVis.r, GlobalVars.Settings.Visuals.Chams.EnemyColVis.g, GlobalVars.Settings.Visuals.Chams.EnemyColVis.b);
						if (GlobalVars.Settings.Visuals.Chams.XQZ)
						{
							ForceMaterial(hidden_tex, renderHidden);
							g_fnOriginalDrawModelExecute(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
							hidden_tex->SetMaterialVarFlag(SourceEngine::MATERIAL_VAR_IGNOREZ, true);
						}
						visible_tex->SetMaterialVarFlag(SourceEngine::MATERIAL_VAR_IGNOREZ, false);

						ForceMaterial(visible_tex, renderVis);
						g_fnOriginalDrawModelExecute(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
					}
				}
			}
		}
		
	}
	
	g_fnOriginalDrawModelExecute(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
	SourceEngine::Interfaces::ModelRender()->ForcedMaterialOverride(NULL);
}