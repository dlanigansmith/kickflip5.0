#include "draw.hpp"
#include "SourceEngine\Color.hpp"
#include <vector>

HFont Fonts::Courier14;
HFont Fonts::Gothic12;
HFont Fonts::Gothic16;
HFont Fonts::Morningtype24;
HFont Fonts::Gothic30;
HFont Fonts::ComicSans;
void Draw::GrabFonts()
{
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::Courier14 = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Courier New", 14, FW_LIGHT, NULL, NULL, FONTFLAG_ANTIALIAS);
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::Gothic12 = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Franklin Gothic Medium", 12, FW_DONTCARE, NULL, NULL, FONTFLAG_ANTIALIAS);
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::Gothic16 = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Franklin Gothic Medium", 16, FW_DONTCARE, NULL, NULL, FONTFLAG_ANTIALIAS);
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::Morningtype24 = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Morningtype", 24, FW_LIGHT, NULL, NULL, FONTFLAG_ANTIALIAS);
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::Gothic30 = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Franklin Gothic Medium", 30, FW_DONTCARE, NULL, NULL, FONTFLAG_ANTIALIAS);
	SourceEngine::Interfaces::MatSurface()->SetFontGlyphSet(Fonts::ComicSans = SourceEngine::Interfaces::MatSurface()->CreateFont(), "Comic Sans MS", 30, FW_DONTCARE, NULL, NULL, FONTFLAG_ANTIALIAS);
}

void Draw::DrawString(HFont font, int x, int y, SourceEngine::Color color, bool bCenter, const char* msg, ...)
{
	va_list va_alist;
	char buf[1024];
	va_start(va_alist, msg);
	_vsnprintf(buf, sizeof(buf), msg, va_alist);
	va_end(va_alist);
	wchar_t wbuf[1024];
	MultiByteToWideChar(CP_UTF8, 0, buf, 256, wbuf, 256);

	uint8_t r = 255, g = 255, b = 255, a = 255;
	color.GetColor(r, g, b, a);

	int iWidth, iHeight;

	SourceEngine::Interfaces::MatSurface()->GetTextSize(font, wbuf, iWidth, iHeight);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextFont(font);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextColor(r, g, b, a);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextPos(!bCenter ? x : x - iWidth / 2, y - iHeight / 2);
	SourceEngine::Interfaces::MatSurface()->DrawPrintText(wbuf, wcslen(wbuf));
}

void Draw::DrawStringUnicode(HFont font, int x, int y, SourceEngine::Color color, bool bCenter, const wchar_t* msg, ...)
{
	uint8_t r = 255, g = 255, b = 255, a = 255;
	color.GetColor(r, g, b, a);

	int iWidth, iHeight;

	SourceEngine::Interfaces::MatSurface()->GetTextSize(font, msg, iWidth, iHeight);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextFont(font);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextColor(r, g, b, a);
	SourceEngine::Interfaces::MatSurface()->DrawSetTextPos(!bCenter ? x : x - iWidth / 2, y - iHeight / 2);
	SourceEngine::Interfaces::MatSurface()->DrawPrintText(msg, wcslen(msg));
}

void Draw::DrawRect(int x, int y, int w, int h, SourceEngine::Color col)
{
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(col); 
	SourceEngine::Interfaces::MatSurface()->DrawFilledRect(x, y, x + w, y + h);
}

void Draw::DrawRectRainbow(int x, int y, int width, int height, float flSpeed, float &flRainbow)
{
	SourceEngine::Color colColor(0, 0, 0);

	flRainbow += flSpeed;
	if (flRainbow > 1.f) flRainbow = 0.f;

	for (int i = 0; i < width; i++)
	{
		float hue = (1.f / (float)width) * i;
		hue -= flRainbow;
		if (hue < 0.f) hue += 1.f;

		SourceEngine::Color colRainbow = colColor.FromHSB(hue, 1.f, 1.f); //ADD HSB COMPATIBILITY 
		Draw::DrawRect(x + i, y, 1, height, colRainbow);
	}
}

void Draw::DrawRectGradientHorizontal(int x, int y, int width, int height, SourceEngine::Color color1, SourceEngine::Color color2)
{
	float flDifferenceR = (float)(color2.r() - color1.r()) / (float)width;
	float flDifferenceG = (float)(color2.g() - color1.g()) / (float)width;
	float flDifferenceB = (float)(color2.b() - color1.b()) / (float)width;

	

	for (float i = 0.f; i < width; i++)
	{
		SourceEngine::Color colGradient = SourceEngine::Color(color1.r() + (flDifferenceR * i), color1.g() + (flDifferenceG * i), color1.b() + (flDifferenceB * i), color1.a());
		Draw::DrawRect(x + i, y, 1, height, colGradient);
	}
}

void Draw::DrawPixel(int x, int y, SourceEngine::Color col)
{
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(col);
	SourceEngine::Interfaces::MatSurface()->DrawFilledRect(x, y, x + 1, y + 1);
}

void Draw::DrawOutlinedRect(int x, int y, int w, int h, SourceEngine::Color col)
{
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(col);
	SourceEngine::Interfaces::MatSurface()->DrawOutlinedRect(x, y, x + w, y + h);
}

void Draw::DrawOutlinedCircle(int x, int y, int r, SourceEngine::Color col)
{
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(col);
	SourceEngine::Interfaces::MatSurface()->DrawOutlinedCircle(x, y, r, 1);
}

void Draw::DrawLine(int x0, int y0, int x1, int y1, SourceEngine::Color col)
{
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(col);
	SourceEngine::Interfaces::MatSurface()->DrawLine(x0, y0, x1, y1);
}

void Draw::DrawCorner(int iX, int iY, int iWidth, int iHeight, bool bRight, bool bDown, SourceEngine::Color colDraw)
{
	int iRealX = bRight ? iX - iWidth : iX;
	int iRealY = bDown ? iY - iHeight : iY;

	if (bDown && bRight)
		iWidth = iWidth + 1;

	Draw::DrawRect(iRealX, iY, iWidth, 1, colDraw);
	Draw::DrawRect(iX, iRealY, 1, iHeight, colDraw);

	Draw::DrawRect(iRealX, bDown ? iY + 1 : iY - 1, !bDown && bRight ? iWidth + 1 : iWidth, 1, SourceEngine::Color(0, 0, 0, 255));
	Draw::DrawRect(bRight ? iX + 1 : iX - 1, bDown ? iRealY : iRealY - 1, 1, bDown ? iHeight + 2 : iHeight + 1, SourceEngine::Color(0, 0, 0, 255));
}

void Draw::DrawPolygon(int count, SourceEngine::Vertex_t* Vertexs, SourceEngine::Color color)
{
	static int Texture = SourceEngine::Interfaces::MatSurface()->CreateNewTextureID(true);
	unsigned char buffer[4] = { 255, 255, 255, 255 };

	SourceEngine::Interfaces::MatSurface()->DrawSetTextureRGBA(Texture, buffer, 1, 1);
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(color);
	SourceEngine::Interfaces::MatSurface()->DrawSetTexture(Texture);

	SourceEngine::Interfaces::MatSurface()->DrawTexturedPolygon(count, Vertexs);
}

void Draw::DrawRoundedBox(int x, int y, int w, int h, int r, int v, SourceEngine::Color col)
{
	std::vector<SourceEngine::Vertex_t> p;
	for (int _i = 0; _i < 3; _i++)
	{
		int _x = x + (_i < 2 && r || w - r);
		int _y = y + (_i % 3 > 0 && r || h - r);
		for (int i = 0; i < v; i++)
		{
			int a = RAD2DEG((i / v) * -90 - _i * 90);
			SourceEngine::Vector2D vec2d;
			vec2d.x = (_x + sin(a) * r);
			vec2d.y = (_y + cos(a) * r);
		    p.push_back(SourceEngine::Vertex_t(vec2d));
		}
	}

	Draw::DrawPolygon(4 * (v + 1), &p[0], col);
	/*
	function DrawRoundedBox(x, y, w, h, r, v, col)
	local p = {};
	for _i = 0, 3 do
	local _x = x + (_i < 2 && r || w - r)
	local _y = y + (_i%3 > 0 && r || h - r)
	for i = 0, v do
	local a = math.rad((i / v) * - 90 - _i * 90)
	table.insert(p, {x = _x + math.sin(a) * r, y = _y + math.cos(a) * r})
	end
	end

	surface.SetDrawSourceEngine::Color(col.r, col.g, col.b, 255)
	draw.NoTexture()
	surface.DrawPoly(p)
	end
	*/

	// Notes: amount of vertexes is 4(v + 1) where v is the number of vertices on each corner bit.
	// I did it in lua cause I have no idea how the vertex_t struct works and i'm still aids at C++
}

void Draw::DrawCornerEsp(int X, int Y, int W, int H, SourceEngine::Color Color)
{
	float lineW = (W / 6);
	float lineH = (H / 7);
	float lineT = 1;

	//outline
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(SourceEngine::Color(0, 0, 0, 255));
	SourceEngine::Interfaces::MatSurface()->DrawLine(X - lineT, Y - lineT, X + lineW, Y - lineT); //top left
	SourceEngine::Interfaces::MatSurface()->DrawLine(X - lineT, Y - lineT, X - lineT, Y + lineH);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X - lineT, Y + H - lineH, X - lineT, Y + H + lineT); //bot left
	SourceEngine::Interfaces::MatSurface()->DrawLine(X - lineT, Y + H + lineT, X + lineW, Y + H + lineT);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W - lineW, Y - lineT, X + W + lineT, Y - lineT); // top right
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W + lineT, Y - lineT, X + W + lineT, Y + lineH);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W + lineT, Y + H - lineH, X + W + lineT, Y + H + lineT); // bot right
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W - lineW, Y + H + lineT, X + W + lineT, Y + H + lineT);

	//inline
	SourceEngine::Interfaces::MatSurface()->DrawSetColor(Color);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X, Y, X, Y + lineH);//top left
	SourceEngine::Interfaces::MatSurface()->DrawLine(X, Y, X + lineW, Y);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W - lineW, Y, X + W, Y); //top right
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W, Y, X + W, Y + lineH);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X, Y + H - lineH, X, Y + H); //bot left
	SourceEngine::Interfaces::MatSurface()->DrawLine(X, Y + H, X + lineW, Y + H);
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W - lineW, Y + H, X + W, Y + H);//bot right
	SourceEngine::Interfaces::MatSurface()->DrawLine(X + W, Y + H - lineH, X + W, Y + H);

}

int Draw::GetStringWidth(HFont font, const char* msg, ...)
{
	va_list va_alist;
	char buf[1024];
	va_start(va_alist, msg);
	_vsnprintf(buf, sizeof(buf), msg, va_alist);
	va_end(va_alist);
	wchar_t wbuf[1024];
	MultiByteToWideChar(CP_UTF8, 0, buf, 256, wbuf, 256);

	int iWidth, iHeight;

	SourceEngine::Interfaces::MatSurface()->GetTextSize(font, wbuf, iWidth, iHeight);

	return iWidth;
}