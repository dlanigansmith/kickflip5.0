#pragma once
struct WeaponSkin
{
	int FallBackPaintKit = 0;
	float FallbackWear = 0.1f;
	int FallbackSeed = 1;
	char* CustomName = nullptr;

};

/*
ADDING NEW STRUCTS: THE GUIDE

struct  <-if you put the name here it is a datatype, which we only want if the struct is outside of another struct.
{
	variables go here

}Name Goes here; <- the name goes here to make the definition


*/
struct tSkinCfg
{
	struct
	{
		int RifleMenu;
		int PistolMenu;

	}Menu;
	struct
	{

		WeaponSkin GalilAR;
		WeaponSkin FAMAS;
		WeaponSkin Scout;
		WeaponSkin AK47;
		WeaponSkin M4A4;
		WeaponSkin M4A1s;
		WeaponSkin SG553;
		WeaponSkin AUG;
		WeaponSkin AWP;
		WeaponSkin Scar20;
		WeaponSkin G3SG1;
	}Rifles;


	struct
	{
		WeaponSkin Usps;
		WeaponSkin Glock;
		WeaponSkin FiveSeven;
		WeaponSkin P250;
		WeaponSkin Tec9;
		WeaponSkin Elite;
		WeaponSkin Deagle;
		WeaponSkin Cz75a;
		WeaponSkin Revolver;
		WeaponSkin P200;
	}Pistols;

	struct
	{
		WeaponSkin Mp9;
		WeaponSkin Mac10;
		WeaponSkin Mp7;
		WeaponSkin Ump45;
		WeaponSkin P90;
		WeaponSkin ppBizon;
	}Smg;



};

extern tSkinCfg Skins;

