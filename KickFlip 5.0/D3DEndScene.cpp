#include "Hooks.hpp"
#include "Input.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"
#include "Menu.hpp"
#include "SkinConfig.hpp"
#include "SkinGUI.hpp"
#include "ImGuiTabs.hpp"
#include "Config.hpp"


void GUI_Init(IDirect3DDevice9* pDevice);
int CurrentTab = 0;

bool ESPWin = false;
bool MiscWin = false;
bool AimWin = false;
bool AntiWin = false;
bool TrigWin = false;
bool SkinzWin = false;
bool VisWin = false;


HRESULT __stdcall Hooks::Hooked_EndScene(IDirect3DDevice9* pDevice)
{
	if (!g_hasGuiInit)
		GUI_Init(pDevice);
	ImGui::GetIO().MouseDrawCursor = GlobalVars.Enabled.MainWinOpen;
	
	ImGui_ImplDX9_NewFrame();
	
	if (GlobalVars.Enabled.MainWinOpen)
	{
		ImGui::Begin(XorStr("KickFlip CSGO"), &GlobalVars.Enabled.MainWinOpen, ImVec2(600, 400), 0.7f, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);
		{
			DrawTabBar(XorStr("Rage"), 1);
			DrawTabBar(XorStr("Visuals"), 2);
			DrawTabBar(XorStr("ESP"), 3);
			DrawTabBar(XorStr("Legit"), 4);
			DrawTabBar(XorStr("Misc"), 5);
			DrawTabBar(XorStr("Skins"), 6);
			ImGui::SameLine();
			ImGui::Text(XorStr(" | "));
			ImGui::SameLine();
			if (ImGui::Button(XorStr("Save Config"))) Config->Save();
			ImGui::SameLine();
			if (ImGui::Button(XorStr("Load Config"))) Config->Load();
			RenderTabs();

		}
		ImGui::End();
	}
	
	ImGui::Render();
	return g_fnOriginalEndScene(pDevice);
}

void GUI_Init(IDirect3DDevice9 * pDevice)
{
	ImGui_ImplDX9_Init(g_hWindow, pDevice);
	ImGuiStyle& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Text] =			ImVec4(0.1f, 0.1f, 0.1f, 1.00f);
	style.Colors[ImGuiCol_TextDisabled] =   ImVec4(0.6f, 0.6f, 0.6f, 1.00f);
	style.Colors[ImGuiCol_WindowBg] =		ImVec4(0.502, 0.502, 0.502, 1.00f);
	style.Colors[ImGuiCol_ComboBg] =		ImVec4(0.402, 0.402, 0.402, 1.00f);
	style.Colors[ImGuiCol_ChildWindowBg] =  ImVec4(0.402, 0.402, 0.402, 1.00f);
	style.Colors[ImGuiCol_Border] =			ImVec4(0.61f, 0.61f, 0.61f, 0.80f);
	style.Colors[ImGuiCol_FrameBg] =		ImVec4(0.80f, 0.80f, 0.80f, 0.09f);
	style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_FrameBgActive] =  ImVec4(0.04f, 0.04f, 0.04f, 0.88f);
	style.Colors[ImGuiCol_TitleBg] =		ImVec4(0.686f, 0.243f, 0.169f, 0.90f); //0.686f, 0.243f, 0.169f
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.20f);
	style.Colors[ImGuiCol_TitleBgActive] =  ImVec4(0.686f, 0.243f, 0.169f, 1.00f);
	style.Colors[ImGuiCol_MenuBarBg] =	    ImVec4(0.35f, 0.35f, 0.35f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarBg] =    ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
	style.Colors[ImGuiCol_CheckMark] =      ImVec4(0.31, 0.31, 0.425, 1.0f);
	
	style.Colors[ImGuiCol_ScrollbarGrab] =			ImVec4(0.984f, 0.82f, 0.345f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered] =	ImVec4(0.984f, 0.82f, 0.345f, 0.59f); 
	style.Colors[ImGuiCol_ScrollbarGrabActive] =	ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_SliderGrab] =				ImVec4(1.00f, 1.00f, 1.00f, 0.59f);
	style.Colors[ImGuiCol_SliderGrabActive] =		ImVec4(0.984f, 0.82f, 0.345f, 1.00f);
	style.Colors[ImGuiCol_Button] =				    ImVec4(0.984f, 0.82f, 0.345f, 1.00f);
	style.Colors[ImGuiCol_ButtonHovered] =			ImVec4(0.667f, 0.506f, 0.016f, 0.59f);
	style.Colors[ImGuiCol_ButtonActive] =			ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_Header] =					ImVec4(0.984f, 0.82f, 0.345f, 1.00f);
	style.Colors[ImGuiCol_HeaderHovered] =			ImVec4(0.99f, 0.9f, 0.5f, 0.59f);
	style.Colors[ImGuiCol_HeaderActive] =			ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_ColumnHovered] =			ImVec4(0.10f, 0.72f, 0.60f, 0.22f);
	style.Colors[ImGuiCol_CloseButton] =			ImVec4(0.071f, 0.024f, 0.016f, 1.00f);
	style.Colors[ImGuiCol_CloseButtonHovered] =		ImVec4(0.071, 0.024, 0.016, 0.59f);

	style.WindowRounding = 0.75f;
	style.FramePadding = ImVec2(8, 1);
	style.ScrollbarSize = 10.f;
	style.ScrollbarRounding = 0.1f;
	style.GrabMinSize = 5.f;
	Utils::PrintMessage(XorStr("KickFlipCS GUI Has Successfully Initialized \n"));
	g_hasGuiInit = true;
}
