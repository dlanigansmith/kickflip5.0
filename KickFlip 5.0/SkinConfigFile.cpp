#include "includes.hpp"
#include "Hooks.hpp"
#include "SkinConfigFile.hpp"
/*
struct EconomyItemConfigs
{
int iItemDefinitionIndex = 0;
int nFallbackPaintKit = 0;
int nFallbackSeed = 0;
int nFallbackStatTrak = -1;
int iEntityQuality = 4;
char* szCustomName = nullptr;
float flFallbackWear = 0.1f;
int ModelIndex = 0;
};
*/
void SCConfig::Setup()
{
	using namespace SourceEngine;
	//RIFLES
	{
		//ak
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackPaintKit, 0, XorStr("AK47"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackSeed, 0, XorStr("AK47"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].nFallbackStatTrak, 0, XorStr("AK47"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].iEntityQuality, 0, XorStr("AK47"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].szCustomName, "\0", XorStr("AK47"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AK47].flFallbackWear, 0.0001f, XorStr("AK47"), XorStr("Wear"));

		//m4a4
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackPaintKit, 0, XorStr("M4A1"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackSeed, 0, XorStr("M4A1"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].nFallbackStatTrak, 0, XorStr("M4A1"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].iEntityQuality, 0, XorStr("M4A1"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].szCustomName, "\0", XorStr("M4A1"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1].flFallbackWear, 0.0001f, XorStr("M4A1"), XorStr("Wear"));

		//m4a1
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].nFallbackPaintKit, 0, XorStr("M4A1_SILENCER"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].nFallbackSeed, 0, XorStr("M4A1_SILENCER"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].nFallbackStatTrak, 0, XorStr("M4A1_SILENCER"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].iEntityQuality, 0, XorStr("M4A1_SILENCER"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].szCustomName, "\0", XorStr("M4A1_SILENCER"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M4A1_SILENCER].flFallbackWear, 0.0001f, XorStr("M4A1_SILENCER"), XorStr("Wear"));


		//GALIL
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackPaintKit, 0, XorStr("GALILAR"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackSeed, 0, XorStr("GALILAR"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].nFallbackStatTrak, 0, XorStr("GALILAR"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].iEntityQuality, 0, XorStr("GALILAR"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].szCustomName, "\0", XorStr("GALILAR"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_GALILAR].flFallbackWear, 0.0001f, XorStr("GALILAR"), XorStr("Wear"));

		//FAMAS
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackPaintKit, 0, XorStr("FAMAS"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackSeed, 0, XorStr("FAMAS"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].nFallbackStatTrak, 0, XorStr("FAMAS"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].iEntityQuality, 0, XorStr("FAMAS"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].szCustomName, "\0", XorStr("FAMAS"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FAMAS].flFallbackWear, 0.0001f, XorStr("FAMAS"), XorStr("Wear"));

		//AUG
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackPaintKit, 0, XorStr("AUG"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackSeed, 0, XorStr("AUG"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].nFallbackStatTrak, 0, XorStr("AUG"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].iEntityQuality, 0, XorStr("AUG"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].szCustomName, "\0", XorStr("AUG"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AUG].flFallbackWear, 0.0001f, XorStr("AUG"), XorStr("Wear"));

		//SG553
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackPaintKit, 0, XorStr("SG553"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackSeed, 0, XorStr("SG553"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].nFallbackStatTrak, 0, XorStr("SG553"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].iEntityQuality, 0, XorStr("SG553"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].szCustomName, "\0", XorStr("SG553"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SG556].flFallbackWear, 0.0001f, XorStr("SG553"), XorStr("Wear"));

		//AWP
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackPaintKit, 0, XorStr("AWP"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackSeed, 0, XorStr("AWP"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].nFallbackStatTrak, 0, XorStr("AWP"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].iEntityQuality, 0, XorStr("AWP"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].szCustomName, "\0", XorStr("AWP"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_AWP].flFallbackWear, 0.0001f, XorStr("AWP"), XorStr("Wear"));


		//Scout
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackPaintKit, 0, XorStr("SSG08"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackSeed, 0, XorStr("SSG08"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].nFallbackStatTrak, 0, XorStr("SSG08"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].iEntityQuality, 0, XorStr("SSG08"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].szCustomName, "\0", XorStr("SSG08"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SSG08].flFallbackWear, 0.0001f, XorStr("SSG08"), XorStr("Wear"));

		//Scar 20
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackPaintKit, 0, XorStr("SCAR20"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackSeed, 0, XorStr("SCAR20"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].nFallbackStatTrak, 0, XorStr("SCAR20"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].iEntityQuality, 0, XorStr("SCAR20"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].szCustomName, "\0", XorStr("SCAR20"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SCAR20].flFallbackWear, 0.0001f, XorStr("SCAR20"), XorStr("Wear"));


		//G3SG1
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackPaintKit, 0, XorStr("G3SG1"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackSeed, 0, XorStr("G3SG1"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].nFallbackStatTrak, 0, XorStr("G3SG1"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].iEntityQuality, 0, XorStr("G3SG1"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].szCustomName, "\0", XorStr("G3SG1"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_G3SG1].flFallbackWear, 0.0001f, XorStr("G3SG1"), XorStr("Wear"));
	}
	//Pistols
	{
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackPaintKit, 0, XorStr("USP_SILENCER"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackSeed, 0, XorStr("USP_SILENCER"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].nFallbackStatTrak, 0, XorStr("USP_SILENCER"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].iEntityQuality, 0, XorStr("USP_SILENCER"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].szCustomName, "\0", XorStr("USP_SILENCER"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_USP_SILENCER].flFallbackWear, 0.0001f, XorStr("USP_SILENCER"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].nFallbackPaintKit, 0, XorStr("HKP2000"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].nFallbackSeed, 0, XorStr("HKP2000"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].nFallbackStatTrak, 0, XorStr("HKP2000"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].iEntityQuality, 0, XorStr("HKP2000"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].szCustomName, "\0", XorStr("HKP2000"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_HKP2000].flFallbackWear, 0.0001f, XorStr("HKP2000"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackPaintKit, 0, XorStr("ELITE"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackSeed, 0, XorStr("ELITE"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].nFallbackStatTrak, 0, XorStr("ELITE"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].iEntityQuality, 0, XorStr("ELITE"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].szCustomName, "\0", XorStr("ELITE"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_ELITE].flFallbackWear, 0.0001f, XorStr("ELITE"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackPaintKit, 0, XorStr("P250"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackSeed, 0, XorStr("P250"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].nFallbackStatTrak, 0, XorStr("P250"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].iEntityQuality, 0, XorStr("P250"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].szCustomName, "\0", XorStr("P250"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P250].flFallbackWear, 0.0001f, XorStr("P250"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackPaintKit, 0, XorStr("TEC9"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackSeed, 0, XorStr("TEC9"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].nFallbackStatTrak, 0, XorStr("TEC9"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].iEntityQuality, 0, XorStr("TEC9"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].szCustomName, "\0", XorStr("TEC9"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_TEC9].flFallbackWear, 0.0001f, XorStr("TEC9"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackPaintKit, 0, XorStr("FIVESEVEN"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackSeed, 0, XorStr("FIVESEVEN"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].nFallbackStatTrak, 0, XorStr("FIVESEVEN"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].iEntityQuality, 0, XorStr("FIVESEVEN"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].szCustomName, "\0", XorStr("FIVESEVEN"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_FIVESEVEN].flFallbackWear, 0.0001f, XorStr("FIVESEVEN"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackPaintKit, 0, XorStr("CZ75A"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackSeed, 0, XorStr("CZ75A"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].nFallbackStatTrak, 0, XorStr("CZ75A"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].iEntityQuality, 0, XorStr("CZ75A"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].szCustomName, "\0", XorStr("CZ75A"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_CZ75A].flFallbackWear, 0.0001f, XorStr("CZ75A"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackPaintKit, 0, XorStr("DEAGLE"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackSeed, 0, XorStr("DEAGLE"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].nFallbackStatTrak, 0, XorStr("DEAGLE"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].iEntityQuality, 0, XorStr("DEAGLE"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].szCustomName, "\0", XorStr("DEAGLE"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_DEAGLE].flFallbackWear, 0.0001f, XorStr("DEAGLE"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].nFallbackPaintKit, 0, XorStr("REVOLVER"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].nFallbackSeed, 0, XorStr("REVOLVER"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].nFallbackStatTrak, 0, XorStr("REVOLVER"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].iEntityQuality, 0, XorStr("REVOLVER"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].szCustomName, "\0", XorStr("REVOLVER"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_REVOLVER].flFallbackWear, 0.0001f, XorStr("REVOLVER"), XorStr("Wear"));
	}
	//SMGs
	{
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackPaintKit, 0, XorStr("MAC10"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackSeed, 0, XorStr("MAC10"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].nFallbackStatTrak, 0, XorStr("MAC10"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].iEntityQuality, 0, XorStr("MAC10"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].szCustomName, "\0", XorStr("MAC10"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAC10].flFallbackWear, 0.0001f, XorStr("MAC10"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackPaintKit, 0, XorStr("MP9"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackSeed, 0, XorStr("MP9"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].nFallbackStatTrak, 0, XorStr("MP9"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].iEntityQuality, 0, XorStr("MP9"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].szCustomName, "\0", XorStr("MP9"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP9].flFallbackWear, 0.0001f, XorStr("MP9"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackPaintKit, 0, XorStr("MP7"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackSeed, 0, XorStr("MP7"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].nFallbackStatTrak, 0, XorStr("MP7"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].iEntityQuality, 0, XorStr("MP7"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].szCustomName, "\0", XorStr("MP7"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MP7].flFallbackWear, 0.0001f, XorStr("MP7"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackPaintKit, 0, XorStr("P90"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackSeed, 0, XorStr("P90"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].nFallbackStatTrak, 0, XorStr("P90"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].iEntityQuality, 0, XorStr("P90"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].szCustomName, "\0", XorStr("P90"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_P90].flFallbackWear, 0.0001f, XorStr("P90"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackPaintKit, 0, XorStr("BIZON"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackSeed, 0, XorStr("BIZON"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].nFallbackStatTrak, 0, XorStr("BIZON"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].iEntityQuality, 0, XorStr("BIZON"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].szCustomName, "\0", XorStr("BIZON"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_BIZON].flFallbackWear, 0.0001f, XorStr("BIZON"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackPaintKit, 0, XorStr("UMP45"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackSeed, 0, XorStr("UMP45"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].nFallbackStatTrak, 0, XorStr("UMP45"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].iEntityQuality, 0, XorStr("UMP45"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].szCustomName, "\0", XorStr("UMP45"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_UMP45].flFallbackWear, 0.0001f, XorStr("UMP45"), XorStr("Wear"));
	}
	//HEAVY
	{
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackPaintKit, 0, XorStr("NOVA"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackSeed, 0, XorStr("NOVA"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].nFallbackStatTrak, 0, XorStr("NOVA"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].iEntityQuality, 0, XorStr("NOVA"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].szCustomName, "\0", XorStr("NOVA"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NOVA].flFallbackWear, 0.0001f, XorStr("NOVA"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackPaintKit, 0, XorStr("XM1014"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackSeed, 0, XorStr("XM1014"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].nFallbackStatTrak, 0, XorStr("XM1014"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].iEntityQuality, 0, XorStr("XM1014"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].szCustomName, "\0", XorStr("XM1014"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_XM1014].flFallbackWear, 0.0001f, XorStr("XM1014"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackPaintKit, 0, XorStr("SAWEDOFF"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackSeed, 0, XorStr("SAWEDOFF"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].nFallbackStatTrak, 0, XorStr("SAWEDOFF"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].iEntityQuality, 0, XorStr("SAWEDOFF"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].szCustomName, "\0", XorStr("SAWEDOFF"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_SAWEDOFF].flFallbackWear, 0.0001f, XorStr("SAWEDOFF"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].nFallbackPaintKit, 0, XorStr("MAG7"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].nFallbackSeed, 0, XorStr("MAG7"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].nFallbackStatTrak, 0, XorStr("MAG7"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].iEntityQuality, 0, XorStr("MAG7"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].szCustomName, "\0", XorStr("MAG7"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_MAG7].flFallbackWear, 0.0001f, XorStr("MAG7"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackPaintKit, 0, XorStr("M249"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackSeed, 0, XorStr("M249"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].nFallbackStatTrak, 0, XorStr("M249"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].iEntityQuality, 0, XorStr("M249"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].szCustomName, "\0", XorStr("M249"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_M249].flFallbackWear, 0.0001f, XorStr("M249"), XorStr("Wear"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackPaintKit, 0, XorStr("NEGEV"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackSeed, 0, XorStr("NEGEV"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].nFallbackStatTrak, 0, XorStr("NEGEV"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].iEntityQuality, 0, XorStr("NEGEV"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].szCustomName, "\0", XorStr("NEGEV"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_NEGEV].flFallbackWear, 0.0001f, XorStr("NEGEV"), XorStr("Wear"));
	}
	//KNIVES
	{
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackPaintKit, 0, XorStr("KNIFE_T"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackSeed, 0, XorStr("KNIFE_T"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].nFallbackStatTrak, 0, XorStr("KNIFE_T"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].iEntityQuality, 0, XorStr("KNIFE_T"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].szCustomName, "\0", XorStr("KNIFE_T"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].flFallbackWear, 0.0001f, XorStr("KNIFE_T"), XorStr("Wear"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE_T].iItemDefinitionIndex, 0.0001f, XorStr("KNIFE_T"), XorStr("ItemDefinitionIndex"));

		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackPaintKit, 0, XorStr("KNIFE"), XorStr("FallbackPaintKit"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackSeed, 0, XorStr("KNIFE"), XorStr("Seed"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].nFallbackStatTrak, 0, XorStr("KNIFE"), XorStr("Stattrak"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].iEntityQuality, 0, XorStr("KNIFE"), XorStr("EntityQuality"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].szCustomName, "\0", XorStr("KNIFE"), XorStr("Name"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].flFallbackWear, 0.0001f, XorStr("KNIFE"), XorStr("Wear"));
		SetupValue(GlobalVars.Skins.g_SkinChangerCfg[WEAPON_KNIFE].iItemDefinitionIndex, 0.0001f, XorStr("KNIFE"), XorStr("ItemDefinitionIndex"));

	}
	 
}

void SCConfig::SetupValue(int &value, int def, std::string category, std::string name)
{
	value = def;
	ints.push_back(new SConfigValue<int>(category, name, &value));
}

void SCConfig::SetupValue(float &value, float def, std::string category, std::string name)
{
	value = def;
	floats.push_back(new SConfigValue<float>(category, name, &value));
}

void SCConfig::SetupValue(bool &value, bool def, std::string category, std::string name)
{
	value = def;
	bools.push_back(new SConfigValue<bool>(category, name, &value));
}

void SCConfig::SetupValue(char* &value, char* def, std::string category, std::string name)
{
	value = def;
	strings.push_back(new SConfigValue<char*>(category, name, &value));
}


void SCConfig::Save()
{
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + XorStr("\\kickflipcs\\");
		file = std::string(path) + XorStr("\\kickflipcs\\skinconfig.ini");
	}

	CreateDirectory(folder.c_str(), NULL);

	for (auto value : ints)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : floats)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(), file.c_str());

	for (auto value : bools)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), *value->value ? "true" : "false", file.c_str());
	for (auto value : strings)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), (*value->value), file.c_str());
}

void SCConfig::Load()
{
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + XorStr("\\kickflipcs\\");
		file = std::string(path) + XorStr("\\kickflipcs\\skinconfig.ini");
	}

	CreateDirectory(folder.c_str(), NULL);

	char value_l[32] = { "\0" };

	for (auto value : ints)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atoi(value_l);
	}

	for (auto value : floats)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atof(value_l);
	}

	for (auto value : bools)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = !strcmp(value_l, "true");
	}
	for (auto value : strings)
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
}

SCConfig* SkinsConfig = new SCConfig();
