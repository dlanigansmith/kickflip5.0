#pragma once
#include "includes.hpp"
#include "Hooks.hpp"
#include "SpoofedConvar.hpp"

#ifndef MISC
#define MISC


class cMisc
{
public:
	static void Run()
	{
		if (GlobalVars.Settings.Misc.RevealRanks && vecPressedKeys[VK_TAB])
		{
			static float fArray[3] = { 0.f, 0.f, 0.f };

			ServerRankRevealAllEx = (ServerRankRevealAllFn)(Offsets->ServerRankRevealAllEx);
			if (!ServerRankRevealAllEx)
				return;
			ServerRankRevealAllEx(fArray);
		}
	
			

	}
	static void MemeWalk(C_CSPlayer* pLocal)
	{

		if (GlobalVars.Settings.Misc.Memewalk && pLocal->GetMoveType() != SourceEngine::MOVETYPE_LADDER)
		{
			if (UserCMD->buttons & IN_FORWARD)
			{
				UserCMD->forwardmove = 450;
				UserCMD->buttons &= ~IN_FORWARD;
				UserCMD->buttons |= IN_BACK;
			}
			else if (UserCMD->buttons & IN_BACK)
			{
				UserCMD->forwardmove = -450;
				UserCMD->buttons &= ~IN_BACK;
				UserCMD->buttons |= IN_FORWARD;
			}

			if (UserCMD->buttons & IN_MOVELEFT)
			{
				UserCMD->sidemove = -450;
				UserCMD->buttons &= ~IN_MOVELEFT;
				UserCMD->buttons |= IN_MOVERIGHT;
			}
			else if (UserCMD->buttons & IN_MOVERIGHT)
			{
				UserCMD->sidemove = 450;
				UserCMD->buttons &= ~IN_MOVERIGHT;
				UserCMD->buttons |= IN_MOVELEFT;
			}
		}
	}
	static void AirStuck()
	{
		if (GlobalVars.Settings.Misc.Airstuck && vecPressedKeys[GlobalVars.Settings.Misc.AirStuckKey])
		{
			if (!(UserCMD->buttons & IN_ATTACK))
				UserCMD->tick_count = INT_MAX;
		}
	}
	static void BunnyHop(C_CSPlayer* pLocal)
	{
		
		if ((GlobalVars.Enabled.BHop) && (UserCMD->buttons & IN_JUMP) && !(pLocal->GetFlags() & (int)SourceEngine::EntityFlags::FL_ONGROUND))
		{
			UserCMD->buttons &= ~IN_JUMP; //Release the JUMP button
		}
	}


};
#endif // !CHATSPAM
