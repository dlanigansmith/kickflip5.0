#include "includes.hpp"
#include "SkinFunctions.hpp"
#include "Hooks.hpp"
#include "SkinConfig.hpp"
tGlobalVars GlobalVars;
tSkinCfg Skins;

BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		DisableThreadLibraryCalls(hModule);
		while (!(FindWindowA(XorStr("Valve001"), NULL))) Sleep(200);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)Hooks::Initialize, hModule, 0, 0);
	}
	if(dwReason == DLL_PROCESS_DETACH)
	{
		KillAll = true;
		Sleep(50);
		return TRUE;
	}
	return true;
}
	