#pragma once
#include "includes.hpp"
#include "Hooks.hpp"
#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "Aim.hpp"

class CFixMove
{
public:
	void Start(SourceEngine::CUserCmd* pCmd);
	void End();
private:
	float forward, right, up;
	SourceEngine::CUserCmd* pCmd;
	SourceEngine::Vector viewforward, viewright, viewup, aimforward, aimright, aimup, vForwardNorm, vRightNorm, vUpNorm;
};
extern CFixMove* FixMove;