#pragma once
#include "Hooks.hpp"
#include "Input.hpp"
#include "MenuElements.hpp"
#include "draw.hpp"
#include "Input.hpp"

class Menu
{
public:
	void Setup();
	void Render();
	MElements::MenuElement* GetFrame();
private:
	void Add(MElements::MenuElement* elem);
	MElements::MenuElement* Frame;
	std::vector<MElements::MenuElement*> elements;
};

extern Menu* MainMenu;