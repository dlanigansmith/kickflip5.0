#pragma once
#include <Windows.h>
#include <memory> //unique_ptr & make_unique
#include <unordered_map>
#include <climits>
#include "XorStr.hpp"
#include "GlobalVars.hpp"

#include <iostream>
#include <shlobj.h>
#define M_RADPI 57.295779513082f
#define M_PI 3.14159265358979323846
#define M_PI_F		((float)(M_PI))

#define SQUARE( a ) a*a
#define DEG2RAD( x  )  ( (float)(x) * (float)( M_PI_F / 180.f ) )
#define RAD2DEG( x  )  ( (float)(x) * (float)( 180.f/M_PI_F ) )

#pragma warning(disable : 4244) //disable the conversion warning, because I like to live on /the edge/ 

#pragma warning(disable : 4018) //disable the other conversion warning
#pragma warning(disable : 4305) //disable the other conversion warning
