#pragma once
#include "SkinConfig.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"
#include "Menu.hpp"



//MISC FUNCS
int RifleCondition;
void RifleConditionGUI(float &con)
{
	
	ImGui::Combo(NULL, &RifleCondition, WeaponConditions, ARRAYSIZE(WeaponConditions));
	switch (RifleCondition)
	{
	case 0:
		con = 0.9; break;
	case 2:
		con = 0.6; break;
	case 3:
		con = 0.3; break;
	case 4:
		con = 0.1; break;
	case 5:
		con = 0.01; break;
	}
}



//Rifle
int GalilSkin;
int AkSkin;
int M4A4Skin; 
int M4A1Skin;
int Ssg08Skin;
int Sg553Skin;
int AUGSkin;
int AWPSkin;
int SCAR20Skin;
int G3SG1Skin;
//Pistols
int UspsSkin;
int	p200Skin;
int EliteSkin;
int P250skin;
int cz75Askin;
int FiveSeveNskin;
int Tec9skin;
int Deagleskin;
int R8skin;
//Smgs
int Mp9skin;
int Mp7skin;
int Mac10skin;
int Ump45skin;
int P90skin;
int Ppbizonskin;


//
//THIS IS A LOT OF FUN TO DO TRUST ME
void RifleGUI(int menu)
{
	switch (menu)
	{
	case 0:
		//GALIL
		ImGui::Combo(NULL, &GalilSkin, Galilskins, ARRAYSIZE(Galilskins));
		switch (GalilSkin) //handler for skin assigner drop down
		{
		case 0: //forest ddpat
			Skins.Rifles.GalilAR.FallBackPaintKit = 5; break;
		case 1: //contrast spray
			Skins.Rifles.GalilAR.FallBackPaintKit = 22; break;
		case 2: //winter forest
			Skins.Rifles.GalilAR.FallBackPaintKit = 76; break;
		case 3: //ornge ddpat
			Skins.Rifles.GalilAR.FallBackPaintKit = 83; break;
		case 4: //Sage Spray
			Skins.Rifles.GalilAR.FallBackPaintKit = 119; break;
		case 5: //Shattered
			Skins.Rifles.GalilAR.FallBackPaintKit = 192; break;
		case 6: //blue titan
			Skins.Rifles.GalilAR.FallBackPaintKit = 216; break;
		case 7: //varicamo
			Skins.Rifles.GalilAR.FallBackPaintKit = 235; break;
		case 8: //urban rubble
			Skins.Rifles.GalilAR.FallBackPaintKit = 237; break;
		case 9: //hunting blind
			Skins.Rifles.GalilAR.FallBackPaintKit = 241; break;
		case 10: //du du du. DU dududu. DUdududu (sandstorm)
			Skins.Rifles.GalilAR.FallBackPaintKit = 264; break;
		case 11: //tux
			Skins.Rifles.GalilAR.FallBackPaintKit = 297; break;
		case 12: //Cerberus
			Skins.Rifles.GalilAR.FallBackPaintKit = 397; break;
		case 13: //chatterbox.
			Skins.Rifles.GalilAR.FallBackPaintKit = 398; break;
		case 14: //eco
			Skins.Rifles.GalilAR.FallBackPaintKit = 428; break;
		case 15: //aqua terrace (why are all these names so shit)
			Skins.Rifles.GalilAR.FallBackPaintKit = 460; break;
		case 16: //rocketpop
			Skins.Rifles.GalilAR.FallBackPaintKit = 478; break;
		case 17: //steve austin (stone cold)
			Skins.Rifles.GalilAR.FallBackPaintKit = 494; break;
		case 18: //firefight
			Skins.Rifles.GalilAR.FallBackPaintKit = 546; break;
		}
		RifleConditionGUI(Skins.Rifles.GalilAR.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("GALILNAME"), Skins.Rifles.GalilAR.CustomName, 160);
		
		break;
	case 1:
		
	//AK
		ImGui::Combo(NULL, &AkSkin, AkSkins, ARRAYSIZE(AkSkins));
		switch (AkSkin) //handler for skin assigner drop down
		{
		case 0: //Red Laminate
			Skins.Rifles.AK47.FallBackPaintKit = 14; break;
		case 1: //Contrast Spray
			Skins.Rifles.AK47.FallBackPaintKit = 22; break;
		case 2: //Case Hardened
			Skins.Rifles.AK47.FallBackPaintKit = 44; break;
		case 3: //Safari Mesh
			Skins.Rifles.AK47.FallBackPaintKit = 72; break;
		case 4: //Jungle Spray
			Skins.Rifles.AK47.FallBackPaintKit = 122; break;
		case 5: //Predator
			Skins.Rifles.AK47.FallBackPaintKit = 170; break;
		case 6: //Black Laminate
			Skins.Rifles.AK47.FallBackPaintKit = 172; break;
		case 7: //Fire Serpent
			Skins.Rifles.AK47.FallBackPaintKit = 180; break;
		case 8: //Blue Laminate
			Skins.Rifles.AK47.FallBackPaintKit = 226; break;
		case 9: //RedLine
			Skins.Rifles.AK47.FallBackPaintKit = 282; break;
		case 10: //Jaguar
			Skins.Rifles.AK47.FallBackPaintKit = 316; break;
		case 11: //Jet Set
			Skins.Rifles.AK47.FallBackPaintKit = 430; break;
		case 12: //First Class							
			Skins.Rifles.AK47.FallBackPaintKit = 341; break;
		case 13: //WasteLand Rebel						
			Skins.Rifles.AK47.FallBackPaintKit = 380; break;
		case 14: //Elite Build							
			Skins.Rifles.AK47.FallBackPaintKit = 422; break;
		case 15: //Hydroponic							
			Skins.Rifles.AK47.FallBackPaintKit = 456; break;
		case 16: //Aquamarine Revenge				
			Skins.Rifles.AK47.FallBackPaintKit = 474; break;
		case 17: //Frontside Misty						
			Skins.Rifles.AK47.FallBackPaintKit = 490; break;
		case 18: //Fuel injector						
			Skins.Rifles.AK47.FallBackPaintKit = 524; break;
		}
		RifleConditionGUI(Skins.Rifles.AK47.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("AkNAME"), Skins.Rifles.AK47.CustomName, 160);
		break;
	case 2:
		
		// M4A4
			ImGui::Combo(NULL, &M4A4Skin, M4Skins, ARRAYSIZE(M4Skins));
		switch (M4A4Skin) //handler for skin assigner drop down
		{
		case 0: //Forest DDPAT
			Skins.Rifles.M4A4.FallBackPaintKit = 5; break;
		case 1: //Desert Storm						
			Skins.Rifles.M4A4.FallBackPaintKit = 8; break;
		case 2: //Jungle Tiger												
			Skins.Rifles.M4A4.FallBackPaintKit = 16; break;
		case 3: //Urban DDPAT						
			Skins.Rifles.M4A4.FallBackPaintKit = 17; break;
		case 4: //Tornado							
			Skins.Rifles.M4A4.FallBackPaintKit = 101; break;
		case 5: //Bullet Rain						
			Skins.Rifles.M4A4.FallBackPaintKit = 155; break;
		case 6: //Modern Hunter						
			Skins.Rifles.M4A4.FallBackPaintKit = 164; break;
		case 7: //Radiation Hazard					
			Skins.Rifles.M4A4.FallBackPaintKit = 167; break;
		case 8: //Predator						
			Skins.Rifles.M4A4.FallBackPaintKit = 170; break;
		case 9: //Faded Zebra						
			Skins.Rifles.M4A4.FallBackPaintKit = 176; break;
		case 10: //Zirka							
			Skins.Rifles.M4A4.FallBackPaintKit = 187; break;
		case 11: //X-Ray							
			Skins.Rifles.M4A4.FallBackPaintKit = 215; break;
		case 12: //Howl															
			Skins.Rifles.M4A4.FallBackPaintKit = 309; break;
		case 13: //Desert-Strike											
			Skins.Rifles.M4A4.FallBackPaintKit = 336; break;
		case 14: //Griffin								
			Skins.Rifles.M4A4.FallBackPaintKit = 484; break;
		case 15: //Dragon King								
			Skins.Rifles.M4A4.FallBackPaintKit = 400; break;
		case 16: //Poseidon							
			Skins.Rifles.M4A4.FallBackPaintKit = 449; break;
		case 17: //Daybreak								
			Skins.Rifles.M4A4.FallBackPaintKit = 471; break;
		case 18: //Evil Daimyo								
			Skins.Rifles.M4A4.FallBackPaintKit = 480; break;
		case 19: //Royal Paladin												
			Skins.Rifles.M4A4.FallBackPaintKit = 512; break;
		case 20: //The BattleStar													
			Skins.Rifles.M4A4.FallBackPaintKit = 533; break;
		}
		RifleConditionGUI(Skins.Rifles.M4A4.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("M4A4NAME"), Skins.Rifles.M4A4.CustomName, 160);
		break;
	case 3:

		//M4A1
		ImGui::Combo(NULL, &M4A1Skin, M4a1skins, ARRAYSIZE(M4a1skins));
		switch (M4A1Skin) //handler for skin assigner drop down
		{
		case 0: //Boreal Forest
			Skins.Rifles.M4A1s.FallBackPaintKit = 77; break;
		case 1: //Bright Water						
			Skins.Rifles.M4A1s.FallBackPaintKit = 189; break;
		case 2: //Varicamo							
			Skins.Rifles.M4A1s.FallBackPaintKit = 235; break;
		case 3: //Blood Tiger						
			Skins.Rifles.M4A1s.FallBackPaintKit = 217; break;
		case 4: //Nitro								
			Skins.Rifles.M4A1s.FallBackPaintKit = 254; break;
		case 5: //Knight														
			Skins.Rifles.M4A1s.FallBackPaintKit = 251; break;
		case 6: //Guardian							
			Skins.Rifles.M4A1s.FallBackPaintKit = 257; break;
		case 7: //Master pieces						
			Skins.Rifles.M4A1s.FallBackPaintKit = 321; break;
		case 8: //Atomic Alloy						
			Skins.Rifles.M4A1s.FallBackPaintKit = 301; break;
		case 9: //Cyrex								
			Skins.Rifles.M4A1s.FallBackPaintKit = 360; break;
		case 10: //Basilisk							
			Skins.Rifles.M4A1s.FallBackPaintKit = 383; break;
		case 11: //Icarus Fell						
			Skins.Rifles.M4A1s.FallBackPaintKit = 440; break;
		case 12: //Hot Rod								
			Skins.Rifles.M4A1s.FallBackPaintKit = 445; break;
		case 13: //Golden Coil							
			Skins.Rifles.M4A1s.FallBackPaintKit = 497; break;
		case 14: // Chantico's Fire						
			Skins.Rifles.M4A1s.FallBackPaintKit = 548; break;
		}
		RifleConditionGUI(Skins.Rifles.M4A1s.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("M4A1sNAME"), Skins.Rifles.M4A1s.CustomName, 160);

		break;
	case 4:

		//SSG08
		ImGui::Combo(NULL, &Ssg08Skin, SSG08skins, ARRAYSIZE(SSG08skins));
		switch (Ssg08Skin) //handler for skin assigner drop down
		{
		case 0: //Lichen Dashed
			Skins.Rifles.Scout.FallBackPaintKit = 26; break;
		case 1: //Dark Water												
			Skins.Rifles.Scout.FallBackPaintKit = 60; break;
		case 2: //Blue Spruce														
			Skins.Rifles.Scout.FallBackPaintKit = 96; break;
		case 3: //Sand Dude														
			Skins.Rifles.Scout.FallBackPaintKit = 99; break;
		case 4: //Palm																
			Skins.Rifles.Scout.FallBackPaintKit = 157; break;
		case 5: //Mayan Dreams																					
			Skins.Rifles.Scout.FallBackPaintKit = 200; break;
		case 6: //Blood in the Water											
			Skins.Rifles.Scout.FallBackPaintKit = 222; break;
		case 7: //Tropical Storm											
			Skins.Rifles.Scout.FallBackPaintKit = 233; break;
		case 8: //Acid Faded													
			Skins.Rifles.Scout.FallBackPaintKit = 253; break;
		case 9: //Slashed																
			Skins.Rifles.Scout.FallBackPaintKit = 304; break;
		case 10: //Detour														
			Skins.Rifles.Scout.FallBackPaintKit = 319; break;
		case 11: //Abyss													
			Skins.Rifles.Scout.FallBackPaintKit = 361; break;
		case 12: //Big Iron															
			Skins.Rifles.Scout.FallBackPaintKit = 503; break;
		case 13: //Necropos							
			Skins.Rifles.Scout.FallBackPaintKit = 538; break;
		case 14: //Ghost Crusader											
			Skins.Rifles.Scout.FallBackPaintKit = 554; break;
		}
		RifleConditionGUI(Skins.Rifles.Scout.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("ScoutNAME"), Skins.Rifles.Scout.CustomName, 160);

		break;
	case 5:

		//SG553
		ImGui::Combo(NULL, &Sg553Skin, SG553skins, ARRAYSIZE(SG553skins));
		switch (Sg553Skin) //handler for skin assigner drop down
		{
		case 0: //Contrast Spray					
			Skins.Rifles.SG553.FallBackPaintKit = 22; break;
		case 1: //Bone Mask													
			Skins.Rifles.SG553.FallBackPaintKit = 27; break;
		case 2: //Anodized Navy																					
			Skins.Rifles.SG553.FallBackPaintKit = 28; break;
		case 3: //Bulldozer														
			Skins.Rifles.SG553.FallBackPaintKit = 39; break;
		case 4: //Ultra Violet														
			Skins.Rifles.SG553.FallBackPaintKit = 98; break;
		case 5: //Tornado																							
			Skins.Rifles.SG553.FallBackPaintKit = 101; break;
		case 6: //Wave Perforated 													
			Skins.Rifles.SG553.FallBackPaintKit = 136; break;
		case 7: //Fallout Warning											
			Skins.Rifles.SG553.FallBackPaintKit = 169; break;
		case 8: //Wave Spray														
			Skins.Rifles.SG553.FallBackPaintKit = 186; break;
		case 9: //Gator Mesh																						
			Skins.Rifles.SG553.FallBackPaintKit = 243; break;
		case 10: //Pulse														
			Skins.Rifles.SG553.FallBackPaintKit = 287; break;
		case 11: //Army Sheen												
			Skins.Rifles.SG553.FallBackPaintKit = 289; break;
		case 12: //Traveler															
			Skins.Rifles.SG553.FallBackPaintKit = 363; break;
		case 13: //Cyrex														
			Skins.Rifles.SG553.FallBackPaintKit = 487; break;
		case 14: //Tiger Moth																		
			Skins.Rifles.SG553.FallBackPaintKit = 519; break;
		case 15: //Atlas																								
			Skins.Rifles.SG553.FallBackPaintKit = 553; break;

		}
		RifleConditionGUI(Skins.Rifles.SG553.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("SG553NAME"), Skins.Rifles.SG553.CustomName, 160);

		break;
	case 6:

		//AUG
		ImGui::Combo(NULL, &AUGSkin, Augskins, ARRAYSIZE(Augskins));
		switch (AUGSkin) //handler for skin assigner drop down
		{
		case 0: //Bengal Tiger					
			Skins.Rifles.AUG.FallBackPaintKit = 9; break;
		case 1: //CopperHead													
			Skins.Rifles.AUG.FallBackPaintKit = 10; break;
		case 2: //Anodized Navy																					
			Skins.Rifles.AUG.FallBackPaintKit = 28; break;
		case 3: //Hot Rod																				
			Skins.Rifles.AUG.FallBackPaintKit = 33; break;
		case 4: //Contractor															
			Skins.Rifles.AUG.FallBackPaintKit = 46; break;
		case 5: //Colony																						
			Skins.Rifles.AUG.FallBackPaintKit = 47; break;
		case 6: //Wings																
			Skins.Rifles.AUG.FallBackPaintKit = 73; break;
		case 7: //Storm															
			Skins.Rifles.AUG.FallBackPaintKit = 100; break;
		case 8: //Condemned 													
			Skins.Rifles.AUG.FallBackPaintKit = 110; break;
		case 9: //Radiation Hazard																					
			Skins.Rifles.AUG.FallBackPaintKit = 167; break;
		case 10: //Torque									
			Skins.Rifles.AUG.FallBackPaintKit = 305; break;
		case 11: //Asterion													
			Skins.Rifles.AUG.FallBackPaintKit = 442; break;
		case 12: //Daedalus																							
			Skins.Rifles.AUG.FallBackPaintKit = 444; break;
		case 13: //Akihabara Accept												
			Skins.Rifles.AUG.FallBackPaintKit = 455; break;
		case 14: //Ricochet																			
			Skins.Rifles.AUG.FallBackPaintKit = 504; break;
		case 15: //Fleet Flock																														
			Skins.Rifles.AUG.FallBackPaintKit = 541; break;

		}
		RifleConditionGUI(Skins.Rifles.AUG.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("AUGNAME"), Skins.Rifles.AUG.CustomName, 160);

		break;
	case 7:

		//AWP
		ImGui::Combo(NULL, &AWPSkin, AWPskins, ARRAYSIZE(AWPskins));
		switch (AWPSkin) //handler for skin assigner drop down
		{
		case 0: //Snake Camo					
			Skins.Rifles.AWP.FallBackPaintKit = 40; break;
		case 1: //Lightning Strike													
			Skins.Rifles.AWP.FallBackPaintKit = 51; break;
		case 2: //Pink DDpat																					
			Skins.Rifles.AWP.FallBackPaintKit = 84; break;
		case 3: //Boom																							
			Skins.Rifles.AWP.FallBackPaintKit = 174; break;
		case 4: //Corticera																					
			Skins.Rifles.AWP.FallBackPaintKit = 181; break;
		case 5: //Graphite																					
			Skins.Rifles.AWP.FallBackPaintKit = 212; break;
		case 6: //Electric Hive														
			Skins.Rifles.AWP.FallBackPaintKit = 227; break;
		case 7: //Pit Viper																					
			Skins.Rifles.AWP.FallBackPaintKit = 251; break;
		case 8: //RedLine														
			Skins.Rifles.AWP.FallBackPaintKit = 259; break;
		case 9: //Dragon Lore																					
			Skins.Rifles.AWP.FallBackPaintKit = 344; break;
		case 10: //Man-o'-war						
			Skins.Rifles.AWP.FallBackPaintKit = 395; break;
		case 11: //Worm God													
			Skins.Rifles.AWP.FallBackPaintKit = 424; break;
		case 12: //Medusa																							
			Skins.Rifles.AWP.FallBackPaintKit = 446; break;
		case 13: //Sun in Leo													
			Skins.Rifles.AWP.FallBackPaintKit = 451; break;
		case 14: //Hyper Beast																		
			Skins.Rifles.AWP.FallBackPaintKit = 475; break;
		case 15: //Elite Build																														
			Skins.Rifles.AWP.FallBackPaintKit = 525; break;

		}
		RifleConditionGUI(Skins.Rifles.AWP.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("AWPNAME"), Skins.Rifles.AWP.CustomName, 160);

		break;
	case 8:
		
		//G3SG1
		ImGui::Combo(NULL, &G3SG1Skin, G3SG1skins, ARRAYSIZE(G3SG1skins));
		switch (G3SG1Skin) //handler for skin assigner drop down
		{
		case 0: //Desert Storm				
			Skins.Rifles.G3SG1.FallBackPaintKit = 8; break;
		case 1: //Carbon Fiber																			
			Skins.Rifles.G3SG1.FallBackPaintKit = 6; break;
		case 2: //BoneMask																						
			Skins.Rifles.G3SG1.FallBackPaintKit = 27; break;
		case 3: //Contractor																					
			Skins.Rifles.G3SG1.FallBackPaintKit = 46; break;
		case 4: //Safari Mesh																				
			Skins.Rifles.G3SG1.FallBackPaintKit = 72; break;
		case 5: //Polar Camo																				
			Skins.Rifles.G3SG1.FallBackPaintKit = 74; break;
		case 6: //Jungle Dash														
			Skins.Rifles.G3SG1.FallBackPaintKit = 147; break;
		case 7: //Predator																						
			Skins.Rifles.G3SG1.FallBackPaintKit = 170; break;
		case 8: //Demeter							
			Skins.Rifles.G3SG1.FallBackPaintKit = 195; break;
		case 9: //Azure Zebra																					
			Skins.Rifles.G3SG1.FallBackPaintKit = 229; break;
		case 10: //VariCamo													
			Skins.Rifles.G3SG1.FallBackPaintKit = 235; break;
		case 11: //Green Apple													
			Skins.Rifles.G3SG1.FallBackPaintKit = 297; break;
		case 12: //Murky																
			Skins.Rifles.G3SG1.FallBackPaintKit = 382; break;
		case 13: //Chronos 													
			Skins.Rifles.G3SG1.FallBackPaintKit = 438; break;
		case 14: //The Executioner																		
			Skins.Rifles.G3SG1.FallBackPaintKit = 511; break;
		case 15: //Flux																												
			Skins.Rifles.G3SG1.FallBackPaintKit = 493; break;
		case 16: //Orange Crash																																	
			Skins.Rifles.G3SG1.FallBackPaintKit = 545; break;

		}
		RifleConditionGUI(Skins.Rifles.G3SG1.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("G3SG1NAME"), Skins.Rifles.G3SG1.CustomName, 160);

		break;
	case 9:

		//SCAR20
		ImGui::Combo(NULL, &SCAR20Skin, Scar20skins, ARRAYSIZE(Scar20skins));
		switch (SCAR20Skin) //handler for skin assigner drop down
		{
		case 0: //Contractor			
			Skins.Rifles.Scar20.FallBackPaintKit = 46; break;
		case 1: //Carbon Fiber																			
			Skins.Rifles.Scar20.FallBackPaintKit = 6; break;
		case 2: //Storm																														
			Skins.Rifles.Scar20.FallBackPaintKit = 100; break;
		case 3: //SandMesh																			
			Skins.Rifles.Scar20.FallBackPaintKit = 116; break;
		case 4: //Palm																
			Skins.Rifles.Scar20.FallBackPaintKit = 157; break;
		case 5: //Splash Jam																					
			Skins.Rifles.Scar20.FallBackPaintKit = 165; break;
		case 6: //Emerald											
			Skins.Rifles.Scar20.FallBackPaintKit = 196; break;
		case 7: //Crimson Web																											
			Skins.Rifles.Scar20.FallBackPaintKit = 232; break;
		case 8: //Army Sheen														
			Skins.Rifles.Scar20.FallBackPaintKit = 298; break;
		case 9: //Cyrex																	
			Skins.Rifles.Scar20.FallBackPaintKit = 312; break;
		case 10: //Cardiac																					
			Skins.Rifles.Scar20.FallBackPaintKit = 391; break;
		case 11: //Grotto														
			Skins.Rifles.Scar20.FallBackPaintKit = 406; break;
		case 12: //Green Marine								
			Skins.Rifles.Scar20.FallBackPaintKit = 502; break;
		case 13: //OutBreak																	
			Skins.Rifles.Scar20.FallBackPaintKit = 518; break;
		}
		RifleConditionGUI(Skins.Rifles.Scar20.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Scar20NAME"), Skins.Rifles.Scar20.CustomName, 160);

		break;
	case 10:
		break;

	}

}

void PistolGUI(int menu)
{
	switch (menu)
	{
	case 0:
		
		//p2000
		ImGui::Combo(NULL, &p200Skin, P2000skins, ARRAYSIZE(P2000skins));
		switch (p200Skin) //handler for skin assigner drop down
		{
		case 0: //Granite Marbleized
			Skins.Pistols.P200.FallBackPaintKit = 21; break;
		case 1: //Silver
			Skins.Pistols.P200.FallBackPaintKit = 32; break;
		case 2: //Scorpion							
			Skins.Pistols.P200.FallBackPaintKit = 71; break;
		case 3: //Grassland							
			Skins.Pistols.P200.FallBackPaintKit = 95; break;
		case 4: //Grassland Leaves
			Skins.Pistols.P200.FallBackPaintKit = 104; break;
		case 5: //Corticera
			Skins.Pistols.P200.FallBackPaintKit = 184; break;
		case 6: //Ocean Foam
			Skins.Pistols.P200.FallBackPaintKit = 211; break;
		case 7: //Amber Fade
			Skins.Pistols.P200.FallBackPaintKit = 246; break;
		case 8: //Red FragCam
			Skins.Pistols.P200.FallBackPaintKit = 275; break;
		case 9: //Chainmail
			Skins.Pistols.P200.FallBackPaintKit = 327; break;
		case 10: //Pulse
			Skins.Pistols.P200.FallBackPaintKit = 338; break;
		case 11: //Coach Class
			Skins.Pistols.P200.FallBackPaintKit = 346; break;
		case 12: //Ivory
			Skins.Pistols.P200.FallBackPaintKit = 357; break;
		case 13: //Fire Elemental.
			Skins.Pistols.P200.FallBackPaintKit = 389; break;
		case 14: //Pathfinder
			Skins.Pistols.P200.FallBackPaintKit = 443; break;
		case 15: //Handgun
			Skins.Pistols.P200.FallBackPaintKit = 485; break;
		}
		RifleConditionGUI(Skins.Pistols.P200.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("P200NAME"), Skins.Pistols.P200.CustomName, 160);

		break;
	case 1:


		//Usps
		ImGui::Combo(NULL, &UspsSkin, USPSskins, ARRAYSIZE(USPSskins));
		switch (UspsSkin) //handler for skin assigner drop down
		{
		case 0: //Forest Leaves
			Skins.Pistols.Usps.FallBackPaintKit = 25; break;
		case 1: //Dark Water
			Skins.Pistols.Usps.FallBackPaintKit = 60; break;
		case 2: //Overgrowth							
			Skins.Pistols.Usps.FallBackPaintKit = 183; break;
		case 3: //Blood Tiger							
			Skins.Pistols.Usps.FallBackPaintKit = 217; break;
		case 4: //Serum
			Skins.Pistols.Usps.FallBackPaintKit = 221; break;
		case 5: //VariCamo
			Skins.Pistols.Usps.FallBackPaintKit = 235; break;
		case 6: //Night Ops
			Skins.Pistols.Usps.FallBackPaintKit = 236	; break;
		case 7: //Stainless
			Skins.Pistols.Usps.FallBackPaintKit = 277; break;
		case 8: //Guardian
			Skins.Pistols.Usps.FallBackPaintKit = 290; break;
		case 9: //Orion
			Skins.Pistols.Usps.FallBackPaintKit = 313; break;
		case 10: //Road Rash
			Skins.Pistols.Usps.FallBackPaintKit = 318; break;
		case 11: //Royal Blue
			Skins.Pistols.Usps.FallBackPaintKit = 332; break;
		case 12: //Caiman
			Skins.Pistols.Usps.FallBackPaintKit = 339; break;
		case 13: //Business Class.
			Skins.Pistols.Usps.FallBackPaintKit = 364; break;
		case 14: //Para Green
			Skins.Pistols.Usps.FallBackPaintKit = 454; break;
		case 15: //Torque
			Skins.Pistols.Usps.FallBackPaintKit = 489; break;
		case 16: //Kill Confirmed
			Skins.Pistols.Usps.FallBackPaintKit = 504; break;
}
		RifleConditionGUI(Skins.Pistols.Usps.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("UspNAME"), Skins.Pistols.Usps.CustomName, 160);

		break;
	case 2:



		//Elite
		ImGui::Combo(NULL, &EliteSkin, DualBerettasSkins, ARRAYSIZE(DualBerettasSkins));
		switch (EliteSkin) //handler for skin assigner drop down
		{
		case 0: //Anodized Navy
			Skins.Pistols.Elite.FallBackPaintKit = 28; break;
		case 1: //Ossified
			Skins.Pistols.Elite.FallBackPaintKit = 36; break;
		case 2: //Stained							
			Skins.Pistols.Elite.FallBackPaintKit = 43; break;
		case 3: //Contractor							
			Skins.Pistols.Elite.FallBackPaintKit = 46; break;
		case 4: //Colony
			Skins.Pistols.Elite.FallBackPaintKit = 47; break;
		case 5: //Demolition
			Skins.Pistols.Elite.FallBackPaintKit = 153; break;
		case 6: //Black Limba
			Skins.Pistols.Elite.FallBackPaintKit = 190; break;
		case 7: //Hemoglobin
			Skins.Pistols.Elite.FallBackPaintKit = 220; break;
		case 8: //Red Quartz
			Skins.Pistols.Elite.FallBackPaintKit = 248; break;
		case 9: //Cobalt Quartz
			Skins.Pistols.Elite.FallBackPaintKit = 249; break;
		case 10: //Marina
			Skins.Pistols.Elite.FallBackPaintKit = 261; break;
		case 11: //Panther
			Skins.Pistols.Elite.FallBackPaintKit = 276; break;
		case 12: //Retribution
			Skins.Pistols.Elite.FallBackPaintKit = 307; break;
		case 13: //Briar
			Skins.Pistols.Elite.FallBackPaintKit = 330; break;
		case 14: //Urban Shock
			Skins.Pistols.Elite.FallBackPaintKit = 369; break;
		case 15: //Duelist
			Skins.Pistols.Elite.FallBackPaintKit = 447; break;
		case 16: //Moon in Libra
			Skins.Pistols.Elite.FallBackPaintKit = 450; break;
		case 17: //Dualing Dragons
			Skins.Pistols.Elite.FallBackPaintKit = 491; break;
		case 18: //Cartel
			Skins.Pistols.Elite.FallBackPaintKit = 528; break;
		case 19: //Ventilators
			Skins.Pistols.Elite.FallBackPaintKit = 544; break;
}
		RifleConditionGUI(Skins.Pistols.Elite.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("EliteNAME"), Skins.Pistols.Elite.CustomName, 160);

		break;
	case 3:

		//p250
		ImGui::Combo(NULL, &EliteSkin, DualBerettasSkins, ARRAYSIZE(DualBerettasSkins));
		switch (EliteSkin) //handler for skin assigner drop down
		{
		case 0: //Gunsmoke
			Skins.Pistols.P250.FallBackPaintKit = 15; break;
		case 1: //Bone Mask
			Skins.Pistols.P250.FallBackPaintKit = 27; break;
		case 2: //Metallic DDPAT							
			Skins.Pistols.P250.FallBackPaintKit = 34; break;
		case 3: //Boreal Forest							
			Skins.Pistols.P250.FallBackPaintKit = 77; break;
		case 4: //Sand Dune
			Skins.Pistols.P250.FallBackPaintKit = 99; break;
		case 5: //Whiteout
			Skins.Pistols.P250.FallBackPaintKit = 102; break;
		case 6: //Splash
			Skins.Pistols.P250.FallBackPaintKit = 162; break;
		case 7: //Modern Hunter
			Skins.Pistols.P250.FallBackPaintKit = 164; break;
		case 8: //Nuclear Threat
			Skins.Pistols.P250.FallBackPaintKit = 168; break;
		case 9: //Facets
			Skins.Pistols.P250.FallBackPaintKit = 207; break;
		case 10: //Hive
			Skins.Pistols.P250.FallBackPaintKit = 219; break;
		case 11: //Steel Disruption
			Skins.Pistols.P250.FallBackPaintKit = 230; break;
		case 12: //Undertow
			Skins.Pistols.P250.FallBackPaintKit = 271; break;
		case 13: //Franklin
			Skins.Pistols.P250.FallBackPaintKit = 295; break;
		case 14: //Supernova
			Skins.Pistols.P250.FallBackPaintKit = 358; break;
		case 15: //Contamination
			Skins.Pistols.P250.FallBackPaintKit = 373; break;
		case 16: //Cartel 
			Skins.Pistols.P250.FallBackPaintKit = 388; break;
		case 17: //Valence
			Skins.Pistols.P250.FallBackPaintKit = 426; break;
		case 18: //Criamson Kimono
			Skins.Pistols.P250.FallBackPaintKit = 466; break;
		case 19: //Mint Kimono
			Skins.Pistols.P250.FallBackPaintKit = 467; break;
		case 20: //Wingshot
			Skins.Pistols.P250.FallBackPaintKit = 501; break;
		case 21: //Asiimov
			Skins.Pistols.P250.FallBackPaintKit = 551; break;
}
		RifleConditionGUI(Skins.Pistols.P250.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("P250NAME"), Skins.Pistols.P250.CustomName, 160);

		break;
	case 4:

		//Cz75
		ImGui::Combo(NULL, &cz75Askin, CZ75skins, ARRAYSIZE(CZ75skins));
		switch (cz75Askin) //handler for skin assigner drop down
		{
		case 0: //Crimson Web
			Skins.Pistols.Cz75a.FallBackPaintKit = 12; break;
		case 1: //Hexane
			Skins.Pistols.Cz75a.FallBackPaintKit = 218; break;
		case 2: //Nitro							
			Skins.Pistols.Cz75a.FallBackPaintKit = 254; break;
		case 3: //Tread Plate							
			Skins.Pistols.Cz75a.FallBackPaintKit = 268; break;
		case 4: //The Fuschia Is Now
			Skins.Pistols.Cz75a.FallBackPaintKit = 269; break;
		case 5: //Victoria
			Skins.Pistols.Cz75a.FallBackPaintKit = 270; break;
		case 6: //Tuxedo
			Skins.Pistols.Cz75a.FallBackPaintKit = 297; break;
		case 7: //Poison Dart
			Skins.Pistols.Cz75a.FallBackPaintKit = 315; break;
		case 8: //Chalice
			Skins.Pistols.Cz75a.FallBackPaintKit = 325; break;
		case 9: //Twist
			Skins.Pistols.Cz75a.FallBackPaintKit = 334; break;
		case 10: //Tigris
			Skins.Pistols.Cz75a.FallBackPaintKit = 350; break;
		case 11: //Green Plaid
			Skins.Pistols.Cz75a.FallBackPaintKit = 366; break;
		case 12: //Pole Position
			Skins.Pistols.Cz75a.FallBackPaintKit = 435; break;
		case 13: //Emerald
			Skins.Pistols.Cz75a.FallBackPaintKit = 453; break;
		case 14: //Yellow Jacket
			Skins.Pistols.Cz75a.FallBackPaintKit = 476; break;
		case 15: //Red Astor
			Skins.Pistols.Cz75a.FallBackPaintKit = 543; break;
		}
		RifleConditionGUI(Skins.Pistols.Cz75a.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("cz75ANAME"), Skins.Pistols.Cz75a.CustomName, 160);

		break;
	case 5:

		//FiveSeveN
		ImGui::Combo(NULL, &FiveSeveNskin, FiveSevenskins, ARRAYSIZE(FiveSevenskins));
		switch (FiveSeveNskin) //handler for skin assigner drop down
		{
		case 0: //Candy Apple
			Skins.Pistols.FiveSeven.FallBackPaintKit = 3; break;
		case 1: //Bone Mask
			Skins.Pistols.FiveSeven.FallBackPaintKit = 27; break;
		case 2: //Case Hardened							
			Skins.Pistols.FiveSeven.FallBackPaintKit = 44; break;
		case 3: //Contractor							
			Skins.Pistols.FiveSeven.FallBackPaintKit = 46; break;
		case 4: //Forest Night 
			Skins.Pistols.FiveSeven.FallBackPaintKit = 78; break;
		case 5: //Orange Peel
			Skins.Pistols.FiveSeven.FallBackPaintKit = 141; break;
		case 6: //Jungle
			Skins.Pistols.FiveSeven.FallBackPaintKit = 151; break;
		case 7: //Anodized Gunmetal
			Skins.Pistols.FiveSeven.FallBackPaintKit = 210; break;
		case 8: //Nightshade
			Skins.Pistols.FiveSeven.FallBackPaintKit = 223; break;
		case 9: //Red Quartz
			Skins.Pistols.FiveSeven.FallBackPaintKit = 248; break;
		case 10: //Silver Quartz
			Skins.Pistols.FiveSeven.FallBackPaintKit = 252; break;
		case 11: //Nitro
			Skins.Pistols.FiveSeven.FallBackPaintKit = 254; break;
		case 12: //Kami
			Skins.Pistols.FiveSeven.FallBackPaintKit = 265; break;
		case 13: //Copper Galaxy
			Skins.Pistols.FiveSeven.FallBackPaintKit = 274; break;
		case 14: //Fowl Play
			Skins.Pistols.FiveSeven.FallBackPaintKit = 352; break;
		case 15: //Hot Shot
			Skins.Pistols.FiveSeven.FallBackPaintKit = 377; break;
		case 16: //Urban Hazard
			Skins.Pistols.FiveSeven.FallBackPaintKit = 387; break;
		case 17: //Monkey Business
			Skins.Pistols.FiveSeven.FallBackPaintKit = 427; break;
		case 18: //Retrobution
			Skins.Pistols.FiveSeven.FallBackPaintKit = 510; break;
		case 19: //Triumvirate
			Skins.Pistols.FiveSeven.FallBackPaintKit = 530; break;
}
		RifleConditionGUI(Skins.Pistols.FiveSeven.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("FiveSevenNAME"), Skins.Pistols.FiveSeven.CustomName, 160);

		break;
	case 6:

		//Tec-9
		ImGui::Combo(NULL, &Tec9skin, TEC9skins, ARRAYSIZE(TEC9skins));
		switch (Tec9skin) //handler for skin assigner drop down
		{
		case 0: //Groundwater
			Skins.Pistols.Tec9.FallBackPaintKit = 2; break;
		case 1: //Forest DDPAT
			Skins.Pistols.Tec9.FallBackPaintKit = 5; break;
		case 2: //Urban DDPAT							
			Skins.Pistols.Tec9.FallBackPaintKit = 17; break;
		case 3: //Ossified							
			Skins.Pistols.Tec9.FallBackPaintKit = 36; break;
		case 4: //Brass
			Skins.Pistols.Tec9.FallBackPaintKit = 159; break;
		case 5: //Nuclear Threat
			Skins.Pistols.Tec9.FallBackPaintKit = 179; break;
		case 6: //Tornado
			Skins.Pistols.Tec9.FallBackPaintKit = 206; break;
		case 7: //Army Mesh
			Skins.Pistols.Tec9.FallBackPaintKit = 242; break;
		case 8: //Red Quartz
			Skins.Pistols.Tec9.FallBackPaintKit = 248; break;
		case 9: //Titanium Bit
			Skins.Pistols.Tec9.FallBackPaintKit = 272; break;
		case 10: //Sandstorm
			Skins.Pistols.Tec9.FallBackPaintKit = 289; break;
		case 11: //Isaac
			Skins.Pistols.Tec9.FallBackPaintKit = 300; break;
		case 12: //Toxic
			Skins.Pistols.Tec9.FallBackPaintKit = 374; break;
		case 13: //Bamboo Forest
			Skins.Pistols.Tec9.FallBackPaintKit = 459; break;
		case 14: //Avalanche
			Skins.Pistols.Tec9.FallBackPaintKit = 520; break;
		case 15: //Jambiya
			Skins.Pistols.Tec9.FallBackPaintKit = 539; break;
		case 16: //Re-Entry
			Skins.Pistols.Tec9.FallBackPaintKit = 555; break;
		case 17: //Monkey Business
			Skins.Pistols.Tec9.FallBackPaintKit = 427; break;
		case 18: //Retrobution
			Skins.Pistols.Tec9.FallBackPaintKit = 510; break;
		case 19: //Triumvirate
			Skins.Pistols.Tec9.FallBackPaintKit = 530; break;
		}
		RifleConditionGUI(Skins.Pistols.Tec9.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Tec9NAME"), Skins.Pistols.Tec9.CustomName, 160);

		break;
	case 7:


		//Desert Eagle
		ImGui::Combo(NULL, &Deagleskin, DeagleSkins, ARRAYSIZE(DeagleSkins));
		switch (Deagleskin) //handler for skin assigner drop down
		{
		case 0: //Forest DDPAT
			Skins.Pistols.Deagle.FallBackPaintKit = 5; break;
		case 1: //Crimson Web 
			Skins.Pistols.Deagle.FallBackPaintKit = 12; break;
		case 2: //Urban DDPAT							
			Skins.Pistols.Deagle.FallBackPaintKit = 17; break;
		case 3: //Blaze							
			Skins.Pistols.Deagle.FallBackPaintKit = 36; break;
		case 4: //Night
			Skins.Pistols.Deagle.FallBackPaintKit = 40; break;
		case 5: //Hypnotic 
			Skins.Pistols.Deagle.FallBackPaintKit = 61; break;
		case 6: //Mudder
			Skins.Pistols.Deagle.FallBackPaintKit = 90; break;
		case 7: //Golden Koi
			Skins.Pistols.Deagle.FallBackPaintKit = 185; break;
		case 8: //Cobalt Disruption
			Skins.Pistols.Deagle.FallBackPaintKit = 231; break;
		case 9: //VariCamo
			Skins.Pistols.Deagle.FallBackPaintKit = 235; break;
		case 10: //Urban Rubble
			Skins.Pistols.Deagle.FallBackPaintKit = 237; break;
		case 11: //Heirloom
			Skins.Pistols.Deagle.FallBackPaintKit = 273; break;
		case 12: //Meteorite
			Skins.Pistols.Deagle.FallBackPaintKit = 296; break;
		case 13: //Hand Cannon
			Skins.Pistols.Deagle.FallBackPaintKit = 328; break;
		case 14: //Pilot
			Skins.Pistols.Deagle.FallBackPaintKit = 347; break;
		case 15: //Conspiracy
			Skins.Pistols.Deagle.FallBackPaintKit = 351; break;
		case 16: //Naga
			Skins.Pistols.Deagle.FallBackPaintKit = 497; break;
		case 17: //Bronze Deco
			Skins.Pistols.Deagle.FallBackPaintKit = 425; break;
		case 18: //Midnight Storm
			Skins.Pistols.Deagle.FallBackPaintKit = 468; break;
		case 19: //Sunset Storm �
			Skins.Pistols.Deagle.FallBackPaintKit = 469; break;
		case 20: //Sunset Storm
			Skins.Pistols.Deagle.FallBackPaintKit = 470; break;
		case 21: //Corinthian 
			Skins.Pistols.Deagle.FallBackPaintKit = 509; break;
		case 22: //Kumicho Dragon 
			Skins.Pistols.Deagle.FallBackPaintKit = 527; break;

}
		RifleConditionGUI(Skins.Pistols.Deagle.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("DeagleNAME"), Skins.Pistols.Deagle.CustomName, 160);

		break;
	case 8:

		//R8
		ImGui::Combo(NULL, &R8skin, Revolvoskins, ARRAYSIZE(Revolvoskins));
		switch (R8skin) //handler for skin assigner drop down
		{
		case 0: //Crimson Web
			Skins.Pistols.Revolver.FallBackPaintKit = 12; break;
		case 1: //Bone Mask 
			Skins.Pistols.Revolver.FallBackPaintKit = 27; break;
		case 2: //Fade							
			Skins.Pistols.Revolver.FallBackPaintKit = 522; break;
		case 3: //Amber Fade							
			Skins.Pistols.Revolver.FallBackPaintKit = 523; break;
		}
		RifleConditionGUI(Skins.Pistols.Revolver.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("RevolverNAME"), Skins.Pistols.Revolver.CustomName, 160);

		break;
	

	}

}

void SmgGUI(int menu)
{
	switch (menu)
	{
	case 0:

		//Mp9
		ImGui::Combo(NULL, &Mp9skin, MP9Skins, ARRAYSIZE(MP9Skins));
		switch (Mp9skin) //handler for skin assigner drop down
		{
		case 0: //Bone Mask
			Skins.Smg.Mp9.FallBackPaintKit = 27; break;
		case 1: //Hot Rod
			Skins.Smg.Mp9.FallBackPaintKit = 33; break;
		case 2: //Bulldozer							
			Skins.Smg.Mp9.FallBackPaintKit = 39; break;
		case 3: //Hypnotic							
			Skins.Smg.Mp9.FallBackPaintKit = 61; break;
		case 4: //Storm
			Skins.Smg.Mp9.FallBackPaintKit = 100; break;
		case 5: //Orange Peel
			Skins.Smg.Mp9.FallBackPaintKit = 141; break;
		case 6: //Sand Dashed
			Skins.Smg.Mp9.FallBackPaintKit = 148; break;
		case 7: //Dry Season
			Skins.Smg.Mp9.FallBackPaintKit = 199; break;
		case 8: //Rose Iron
			Skins.Smg.Mp9.FallBackPaintKit = 262; break;
		case 9: //Dark Age
			Skins.Smg.Mp9.FallBackPaintKit = 329; break;
		case 10: //Green Plaid
			Skins.Smg.Mp9.FallBackPaintKit = 366; break;
		case 11: //Setting Sun
			Skins.Smg.Mp9.FallBackPaintKit = 386; break;
		case 12: //Dart
			Skins.Smg.Mp9.FallBackPaintKit = 386; break;
		case 13: //Deadly Poison
			Skins.Smg.Mp9.FallBackPaintKit = 403; break;
		case 14: //Pandora's Box
			Skins.Smg.Mp9.FallBackPaintKit = 448; break;
		case 15: //Bioleak
			Skins.Smg.Mp9.FallBackPaintKit = 449; break;
		case 16: //Ruby Poison Dart.
			Skins.Smg.Mp9.FallBackPaintKit = 482; break;

		}
		RifleConditionGUI(Skins.Smg.Mp9.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Mp9NAME"), Skins.Smg.Mp9.CustomName, 160);

		break;
	case 1:

		//mac10
		ImGui::Combo(NULL, &Mac10skin, MAC10Skins, ARRAYSIZE(MAC10Skins));
		switch (Mac10skin) //handler for skin assigner drop down
		{
		case 0: //Candy Apple
			Skins.Smg.Mp9.FallBackPaintKit = 3; break;
		case 1: //Forest DDPAT
			Skins.Smg.Mp9.FallBackPaintKit = 5; break;
		case 2: //Urban DDPAT							
			Skins.Smg.Mp9.FallBackPaintKit = 17; break;
		case 3: //Silver							
			Skins.Smg.Mp9.FallBackPaintKit = 32; break;
		case 4: //Fade
			Skins.Smg.Mp9.FallBackPaintKit = 38; break;
		case 5: //Ultraviolet
			Skins.Smg.Mp9.FallBackPaintKit = 98; break;
		case 6: //Tornado
			Skins.Smg.Mp9.FallBackPaintKit = 101; break;
		case 7: //Palm
			Skins.Smg.Mp9.FallBackPaintKit = 157; break;
		case 8: //Graven 
			Skins.Smg.Mp9.FallBackPaintKit = 188; break;
		case 9: //Amber Fade
			Skins.Smg.Mp9.FallBackPaintKit = 246; break;
		case 10: //Heat
			Skins.Smg.Mp9.FallBackPaintKit = 284; break;
		case 11: //Curse
			Skins.Smg.Mp9.FallBackPaintKit = 310; break;
		case 12: //Indigo
			Skins.Smg.Mp9.FallBackPaintKit = 333; break;
		case 13: //Tatter
			Skins.Smg.Mp9.FallBackPaintKit = 337; break;
		case 14: //Commuter
			Skins.Smg.Mp9.FallBackPaintKit = 343; break;
		case 15: //Nuclear Garden
			Skins.Smg.Mp9.FallBackPaintKit = 372; break;
		case 16: //Malachite
			Skins.Smg.Mp9.FallBackPaintKit = 402; break;
		case 17: //Neon Rider
			Skins.Smg.Mp9.FallBackPaintKit = 433; break;
		}
		RifleConditionGUI(Skins.Smg.Mac10.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Mac10NAME"), Skins.Smg.Mac10.CustomName, 160);

		break;
	case 2:

		//mp7
		ImGui::Combo(NULL, &Mp7skin, MP7skins, ARRAYSIZE(MP7skins));
		switch (Mp7skin) //handler for skin assigner drop down
		{
		case 0: //Groundwater
			Skins.Smg.Mp7.FallBackPaintKit = 27; break;
		case 1: //Forest DDPAT
			Skins.Smg.Mp7.FallBackPaintKit = 5; break;
		case 2: //Skulls						
			Skins.Smg.Mp7.FallBackPaintKit = 11; break;
		case 3: //Gunsmoke							
			Skins.Smg.Mp7.FallBackPaintKit = 15; break;
		case 4: //Contrast Spray
			Skins.Smg.Mp7.FallBackPaintKit = 22; break;
		case 5: //Bone Mask
			Skins.Smg.Mp7.FallBackPaintKit = 27; break;
		case 6: //Anodized Navy
			Skins.Smg.Mp7.FallBackPaintKit = 28; break;
		case 7: //Ossified
			Skins.Smg.Mp7.FallBackPaintKit = 36; break;
		case 8: //Whiteout
			Skins.Smg.Mp7.FallBackPaintKit = 102; break;
		case 9: //Orange Peel
			Skins.Smg.Mp7.FallBackPaintKit = 182; break;
		case 10: //Ocean Foam
			Skins.Smg.Mp7.FallBackPaintKit = 213; break;
		case 11: //VariCamo
			Skins.Smg.Mp7.FallBackPaintKit = 235; break;
		case 12: //Army Recon
			Skins.Smg.Mp7.FallBackPaintKit = 245; break;
		case 13: //Full Stop
			Skins.Smg.Mp7.FallBackPaintKit = 250; break;
		case 14: //Urban Hazard
			Skins.Smg.Mp7.FallBackPaintKit = 354; break;
		case 15: //Olive Plaid
			Skins.Smg.Mp7.FallBackPaintKit = 365; break;
		case 16: //Armor Core
			Skins.Smg.Mp7.FallBackPaintKit = 423; break;
		case 17: //Asterion
			Skins.Smg.Mp7.FallBackPaintKit = 442; break;
		case 18: //Nemesis
			Skins.Smg.Mp7.FallBackPaintKit = 481; break;
		case 19: //Special Delivery
			Skins.Smg.Mp7.FallBackPaintKit = 500; break;

		}
		RifleConditionGUI(Skins.Smg.Mp7.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Mp7NAME"), Skins.Smg.Mp7.CustomName, 160);

		break;
	case 3:

		//UMP-45
		ImGui::Combo(NULL, &Ump45skin, UMPskins, ARRAYSIZE(UMPskins));
		switch (Ump45skin) //handler for skin assigner drop down
		{
		case 0: //Forest DDPAT
			Skins.Smg.Ump45.FallBackPaintKit = 5; break;
		case 1: //Gunsmoke
			Skins.Smg.Ump45.FallBackPaintKit = 15; break;
		case 2: //Urban DDPAT						
			Skins.Smg.Ump45.FallBackPaintKit = 17; break;
		case 3: //Blaze							
			Skins.Smg.Ump45.FallBackPaintKit = 37; break;
		case 4: //Carbon Fiber
			Skins.Smg.Ump45.FallBackPaintKit = 70; break;
		case 5: //Caramel
			Skins.Smg.Ump45.FallBackPaintKit = 93; break;
		case 6: //Fallout Warning
			Skins.Smg.Ump45.FallBackPaintKit = 169; break;
		case 7: //Scorched
			Skins.Smg.Ump45.FallBackPaintKit = 175; break;
		case 8: //Bone Pile
			Skins.Smg.Ump45.FallBackPaintKit = 193; break;
		case 9: //Corporal
			Skins.Smg.Ump45.FallBackPaintKit = 281; break;
		case 10: //Indigo
			Skins.Smg.Ump45.FallBackPaintKit = 333; break;
		case 11: //Labyrinth
			Skins.Smg.Ump45.FallBackPaintKit = 362; break;
		case 12: //Delusion
			Skins.Smg.Ump45.FallBackPaintKit = 392; break;
		case 13: //Grand Prix
			Skins.Smg.Ump45.FallBackPaintKit = 436; break;
		case 14: //Minotaur's Labyrinth
			Skins.Smg.Ump45.FallBackPaintKit = 441; break;
		case 15: //Riot
			Skins.Smg.Ump45.FallBackPaintKit = 448; break;
		case 16: //Primal Saber
			Skins.Smg.Ump45.FallBackPaintKit = 556; break;

		}
		RifleConditionGUI(Skins.Smg.Ump45.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("Ump45NAME"), Skins.Smg.Ump45.CustomName, 160);

		break;
	case 4:

		//P90
		ImGui::Combo(NULL, &P90skin, P90skins, ARRAYSIZE(P90skins));
		switch (P90skin) //handler for skin assigner drop down
		{
		case 0: //Virus
			Skins.Smg.P90.FallBackPaintKit = 20; break;
		case 1: //Contrast Spray
			Skins.Smg.P90.FallBackPaintKit = 22; break;
		case 2: //Cold Blooded							
			Skins.Smg.P90.FallBackPaintKit = 67; break;
		case 3: //Storm							
			Skins.Smg.P90.FallBackPaintKit = 100; break;
		case 4: //Glacier Mesh
			Skins.Smg.P90.FallBackPaintKit = 111; break;
		case 5: //Sand Spray
			Skins.Smg.P90.FallBackPaintKit = 124; break;
		case 6: //Death by Kitty
			Skins.Smg.P90.FallBackPaintKit = 156; break;
		case 7: //Fallout Warning
			Skins.Smg.P90.FallBackPaintKit = 169; break;
		case 8: //Scorched
			Skins.Smg.P90.FallBackPaintKit = 175; break;
		case 9: //Emerald Dragon
			Skins.Smg.P90.FallBackPaintKit = 182; break;
		case 10: //Blind Spot
			Skins.Smg.P90.FallBackPaintKit = 228; break;
		case 11: //Ash Wood
			Skins.Smg.P90.FallBackPaintKit = 234; break;
		case 12: //Teardown
			Skins.Smg.P90.FallBackPaintKit = 244; break;
		case 13: //Trigon
			Skins.Smg.P90.FallBackPaintKit = 283; break;
		case 14: //Desert Warfare
			Skins.Smg.P90.FallBackPaintKit = 311; break;
		case 15: //Module
			Skins.Smg.P90.FallBackPaintKit = 335; break;
		case 16: //Leather
			Skins.Smg.P90.FallBackPaintKit = 342; break;
		case 17: //Asiimov
			Skins.Smg.P90.FallBackPaintKit = 359; break;
		case 18: //Elite Build
			Skins.Smg.P90.FallBackPaintKit = 486; break;

		}
		RifleConditionGUI(Skins.Smg.P90.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("P90NAME"), Skins.Smg.P90.CustomName, 160);

		break;
	case 5:

		//PPbizon
		ImGui::Combo(NULL, &Ppbizonskin, PPbizonSkins, ARRAYSIZE(PPbizonSkins));
		switch (Ppbizonskin) //handler for skin assigner drop down
		{
		case 0: //Blue Streak
			Skins.Smg.ppBizon.FallBackPaintKit = 13; break;
		case 1: //Forest Leaves
			Skins.Smg.ppBizon.FallBackPaintKit = 25; break;
		case 2: //Bone Mask						
			Skins.Smg.ppBizon.FallBackPaintKit = 27; break;
		case 3: //Carbon Fiber							
			Skins.Smg.ppBizon.FallBackPaintKit = 70; break;
		case 4: //Sand Dashed
			Skins.Smg.ppBizon.FallBackPaintKit = 148; break;
		case 5: //Urban Dashed
			Skins.Smg.ppBizon.FallBackPaintKit = 149; break;
		case 6: //Brass
			Skins.Smg.ppBizon.FallBackPaintKit = 159; break;
		case 7: //Modern Hunter
			Skins.Smg.ppBizon.FallBackPaintKit = 164; break;
		case 8: //Irradiated Alert
			Skins.Smg.ppBizon.FallBackPaintKit = 171; break;
		case 9: //Rust Coat
			Skins.Smg.ppBizon.FallBackPaintKit = 203; break;
		case 10: //Water Sigil
			Skins.Smg.ppBizon.FallBackPaintKit = 224; break;
		case 11: //VariCamo
			Skins.Smg.ppBizon.FallBackPaintKit = 235; break;
		case 12: //Night Ops
			Skins.Smg.ppBizon.FallBackPaintKit = 236; break;
		case 13: //Cobalt Halftone
			Skins.Smg.ppBizon.FallBackPaintKit = 267; break;
		case 14: //Antique
			Skins.Smg.ppBizon.FallBackPaintKit = 306; break;
		case 15: //Osiris
			Skins.Smg.ppBizon.FallBackPaintKit = 349; break;
		case 16: //Chemical Green
			Skins.Smg.ppBizon.FallBackPaintKit = 376; break;
		case 17: //Bamboo Print
			Skins.Smg.ppBizon.FallBackPaintKit = 457; break;
		case 18: //Bamboo Forest
			Skins.Smg.ppBizon.FallBackPaintKit = 459; break;
		case 19: //Fuel Rod
			Skins.Smg.ppBizon.FallBackPaintKit = 508; break;
		case 20: //Judgement of Anubis
			Skins.Smg.ppBizon.FallBackPaintKit = 542; break;

		}
		RifleConditionGUI(Skins.Smg.ppBizon.FallbackWear);
		ImGui::PushItemWidth(50);
		ImGui::Text(XorStr("Name "));
		ImGui::SameLine();
		ImGui::PushItemWidth(180);
		ImGui::InputText(XorStr("ppBizonNAME"), Skins.Smg.ppBizon.CustomName, 160);

		break;

	}
}