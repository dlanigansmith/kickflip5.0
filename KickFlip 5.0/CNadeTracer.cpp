#include "CNadeTracer.hpp"

CNadeTracer::CNadeTracer() { }

void CNadeTracer::AddTracer(C_CSPlayer* pGrenade, SourceEngine::Color colorTrace, unsigned int iMaxLength)
{
	m_setGrenadeExists.insert(pGrenade);
	if (m_mapGrenades.find(pGrenade) == m_mapGrenades.end())
		m_mapGrenades[pGrenade] = CTraceObj(colorTrace, iMaxLength);

	if (m_mapGrenades[pGrenade].m_timerPointLock.diff() > 0.025f) //25 ms
	{
		m_mapGrenades[pGrenade].m_vecTracePoints.push_back(pGrenade->GetOrigin());
		if (m_mapGrenades[pGrenade].m_vecTracePoints.size() > m_mapGrenades[pGrenade].m_iMaxLength)
			m_mapGrenades[pGrenade].m_vecTracePoints.erase(m_mapGrenades[pGrenade].m_vecTracePoints.begin());
		
		m_mapGrenades[pGrenade].m_timerPointLock.init();
	}
}

void CNadeTracer::Draw()
{
	SourceEngine::Vector vecLastScreenPos;
	bool bInit = false;

	for (auto& traceObj : m_mapGrenades)
	{
		for (auto& vecPos : traceObj.second.m_vecTracePoints)
		{
			if (vecPos.IsZero())
				return;
			SourceEngine::Vector vecScreenPos;
			Utils::WorldToScreen(vecPos, vecScreenPos);
			if (!vecScreenPos.z)
			{
				if (bInit)
				{
					Draw::DrawLine(vecLastScreenPos.x, vecLastScreenPos.y, vecScreenPos.x, vecScreenPos.y, traceObj.second.m_colorTrace);
				}
				vecLastScreenPos = vecScreenPos;
				bInit = true;
			}
		}
		bInit = false;
	}
}

void CNadeTracer::Clear()
{
	for (auto it = m_mapGrenades.begin(); it != m_mapGrenades.end(); ++it)
	{
		if (m_setGrenadeExists.find(((*it).first)) == m_setGrenadeExists.end())
		{
			it = m_mapGrenades.erase(it);
			
			if (it == m_mapGrenades.end())
				break;
		}
		
	}
}

CTraceObj::CTraceObj(SourceEngine::Color colorTrace, unsigned int maxLength)
{
	m_colorTrace = colorTrace;
	m_iMaxLength = maxLength;
	m_timerPointLock.init();
}

void CNadeTracer::Run(int i)
{
	if (GlobalVars.Settings.Visuals.Tracers)
	{
		auto pLocal = C_CSPlayer::GetLocalPlayer(); //get localplayer
		if (i == SourceEngine::Interfaces::Engine()->GetLocalPlayer()) return; //ignore if local player
		auto gEntity = static_cast<C_CSPlayer*>(SourceEngine::Interfaces::EntityList()->GetClientEntity(i)); //Get ent
		if (!gEntity) return; //check if null
		NadeTracer->AddTracer(gEntity, SourceEngine::Color::Red(), 150);
	}
		
}

CNadeTracer* NadeTracer = new CNadeTracer();