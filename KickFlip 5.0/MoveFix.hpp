#pragma once
#include "Hooks.hpp"
#ifndef FIXMOVE
#define FIXMOVE
class CFixMove
{
public:
	void Start();
	void End();
	CFixMove() {}
	~CFixMove() {}
private:
	float m_oldforward, m_oldsidemove;
	SourceEngine::QAngle m_oldangle;

};

#endif