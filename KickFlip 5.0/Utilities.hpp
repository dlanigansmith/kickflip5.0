#pragma once

#include <Windows.h>
#include <Psapi.h>
#include "XorStr.hpp"
#include "SourceEngine/SDK.hpp"

#include "KeyValuesShit.hpp"
using InitKeyValuesFn = void(__thiscall*)(void* thisptr, const char* name);
using LoadFromBufferFn = void(__thiscall*)(void* thisptr, const char* resourceName, const char* pBuffer, void* pFileSystem, const char* pPathID, void* pfnEvaluateSymbolProc);
class Utils
{
public:
	
	static SourceEngine::Vector GetEntityBone(SourceEngine::IClientEntity* pEntity, SourceEngine::ECSPlayerBones Bone) 
	{
		SourceEngine::matrix3x4_t boneMatrix[128];

		if (!pEntity->SetupBones(boneMatrix, 128, 0x00000100, SourceEngine::Interfaces::Engine()->GetLastTimeStamp()))
			return SourceEngine::Vector();

		SourceEngine::matrix3x4_t hitbox = boneMatrix[Bone];

		return SourceEngine::Vector(hitbox[0][3], hitbox[1][3], hitbox[2][3]);
	}

	static bool ScreenTransform(const SourceEngine::Vector& point, SourceEngine::Vector& screen) 
	{
		const SourceEngine::VMatrix& w2sMatrix = SourceEngine::Interfaces::Engine()->WorldToScreenMatrix();
		screen.x = w2sMatrix.m[0][0] * point.x + w2sMatrix.m[0][1] * point.y + w2sMatrix.m[0][2] * point.z + w2sMatrix.m[0][3];
		screen.y = w2sMatrix.m[1][0] * point.x + w2sMatrix.m[1][1] * point.y + w2sMatrix.m[1][2] * point.z + w2sMatrix.m[1][3];
		screen.z = 0.0f;

		float w = w2sMatrix.m[3][0] * point.x + w2sMatrix.m[3][1] * point.y + w2sMatrix.m[3][2] * point.z + w2sMatrix.m[3][3];

		if (w < 0.001f) {
			screen.x *= 100000;
			screen.y *= 100000;
			return true;
		}

		float invw = 1.0f / w;
		screen.x *= invw;
		screen.y *= invw;

		return false;
	}

	static bool WorldToScreen(const SourceEngine::Vector &origin, SourceEngine::Vector &screen) 
	{
		if (!ScreenTransform(origin, screen)) {
			int iScreenWidth, iScreenHeight;
			SourceEngine::Interfaces::Engine()->GetScreenSize(iScreenWidth, iScreenHeight);

			screen.x = (iScreenWidth / 2.0f) + (screen.x * iScreenWidth) / 2;
			screen.y = (iScreenHeight / 2.0f) - (screen.y * iScreenHeight) / 2;

			return true;
		}
		return false;
	}

	static uint64_t FindSignature(const char* szModule, const char* szSignature) 
	{
		//CREDITS: learn_more
		#define INRANGE(x,a,b)  (x >= a && x <= b) 
		#define getBits( x )    (INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
		#define getByte( x )    (getBits(x[0]) << 4 | getBits(x[1]))

		MODULEINFO modInfo;
		GetModuleInformation(GetCurrentProcess(), GetModuleHandleA(szModule), &modInfo, sizeof(MODULEINFO));
		DWORD startAddress = (DWORD)modInfo.lpBaseOfDll;
		DWORD endAddress = startAddress + modInfo.SizeOfImage;
		const char* pat = szSignature;
		DWORD firstMatch = 0;
		for (DWORD pCur = startAddress; pCur < endAddress; pCur++) 
		{
			if (!*pat) return firstMatch;
			if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat)) 
			{
				if (!firstMatch) firstMatch = pCur;
				if (!pat[2]) return firstMatch;
				if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?') pat += 3;
				else pat += 2;    //one ?
			}
			else 
			{
				pat = szSignature;
				firstMatch = 0;
			}
		}
		return NULL;
	}
	static DWORD FindPattern(std::string moduleName, std::string pattern)
	{
		const char* pat = pattern.c_str();
		DWORD firstMatch = 0;
		DWORD rangeStart = (DWORD)GetModuleHandleA(moduleName.c_str());
		MODULEINFO miModInfo; GetModuleInformation(GetCurrentProcess(), (HMODULE)rangeStart, &miModInfo, sizeof(MODULEINFO));
		DWORD rangeEnd = rangeStart + miModInfo.SizeOfImage;
		for (DWORD pCur = rangeStart; pCur < rangeEnd; pCur++)
		{
			if (!*pat)
				return firstMatch;

			if (*(PBYTE)pat == '\?' || *(BYTE*)pCur == getByte(pat))
			{
				if (!firstMatch)
					firstMatch = pCur;

				if (!pat[2])
					return firstMatch;

				if (*(PWORD)pat == '\?\?' || *(PBYTE)pat != '\?')
					pat += 3;

				else
					pat += 2;    //one ?
			}
			else
			{
				pat = pattern.c_str();
				firstMatch = 0;
			}
		}
		return NULL;
	}

	static bool Clamp(SourceEngine::QAngle &angles)
	{
		SourceEngine::QAngle a = angles;
		Normalize(a);
		ClampAngles(a);

		if (isnan(a.x) || isinf(a.x) ||
			isnan(a.y) || isinf(a.y) ||
			isnan(a.z) || isinf(a.z)) {
			return false;
		}
		else {
			angles = a;
			return true;
		}
	}
	static bool Aimstep(SourceEngine::QAngle src, SourceEngine::QAngle dst, SourceEngine::QAngle& out, int steps)
	{
		SourceEngine::QAngle delta_angle = (dst - src).Normalized();;
		bool x_finished = false;
		bool y_finished = false;

		if (delta_angle.x > steps)
			src.x += steps;

		else if (delta_angle.x < -steps)
			src.x -= steps;

		else
		{
			x_finished = true;
			src.x = dst.x;
		}

		if (delta_angle.y > steps)
			src.y += steps;

		else if (delta_angle.y < -steps)
			src.y -= steps;

		else
		{
			y_finished = true;
			src.y = dst.y;
		}

		src.Normalized();

		out = src;

		return x_finished && y_finished;
	}

	static void PrintMessage(const char* szMsg, ...) 
	{
		typedef void(__cdecl* MsgFn)(const char* szMsg, va_list);
		static MsgFn Msg = (MsgFn)GetProcAddress(GetModuleHandleA("tier0.dll"), "Msg");

		char buf[989];
		va_list vlist;
		va_start(vlist, szMsg);
		_vsnprintf(buf, sizeof(buf), szMsg, vlist);
		va_end(vlist);
		sprintf(buf, "%s", buf);

		Msg(buf, vlist);
	}
	static void PrintError(const char* szMsg, ...)
	{
		typedef void(__cdecl* MsgFn)(const char* szMsg, va_list);
		static MsgFn Msg = (MsgFn)GetProcAddress(GetModuleHandleA("tier0.dll"), "Error");

		char buf[989];
		va_list vlist;
		va_start(vlist, szMsg);
		_vsnprintf(buf, sizeof(buf), szMsg, vlist);
		va_end(vlist);
		sprintf(buf, "%s", buf);

		Msg(buf, vlist);
	}
	static void PrintWarning(const char* szMsg, ...)
	{
		typedef void(__cdecl* MsgFn)(const char* szMsg, va_list);
		static MsgFn Msg = (MsgFn)GetProcAddress(GetModuleHandleA("tier0.dll"), "Warning");

		char buf[989];
		va_list vlist;
		va_start(vlist, szMsg);
		_vsnprintf(buf, sizeof(buf), szMsg, vlist);
		va_end(vlist);
		sprintf(buf, "%s", buf);

		Msg(buf, vlist);
	}
	static void PrintColorMsg(SourceEngine::Color &col, const char* szMsg, ...)
	{
		typedef void(__cdecl* ConColorMsgFn)(const SourceEngine::Color& clr, const char* msg, va_list);

		static ConColorMsgFn fn = (ConColorMsgFn)GetProcAddress(GetModuleHandle("tier0.dll"), ("?ConColorMsg@@YAXABVColor@@PBDZZ"));
		char buf[989];
		va_list vlist;
		va_start(vlist, szMsg);
		_vsnprintf(buf, sizeof(buf), szMsg, vlist);
		va_end(vlist);
		sprintf(buf, "%s", buf);

		fn(col, buf, vlist);
	}
	static void ChangeName(const char* name)
	{
		SourceEngine::ConVar* nameCVar = SourceEngine::Interfaces::CVar()->FindVar(XorStr("name"));
		if(*(int*)((DWORD)&nameCVar->m_fnChangeCallback + 0xC) != NULL)
			*(int*)((DWORD)&nameCVar->m_fnChangeCallback + 0xC) = NULL;
		nameCVar->SetValue(name);  

	}
	static void InitKeyValues(KeyValsShit* pKeyValues, const char* name)
	{
		static InitKeyValuesFn KeyValsEx = (InitKeyValuesFn)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 51 33 C0 C7 45")));
		KeyValsEx(pKeyValues, name);
	}
	static void LoadFromBuffer(KeyValsShit* pKeyValues, const char* resourceName, const char* pBuffer, void* pFileSystem, const char* pPathID, void* pfnEvaluateSymbolProc)
	{
		static LoadFromBufferFn LoadFromBufferEx = (LoadFromBufferFn)(Utils::FindPattern(XorStr("client.dll"), XorStr("55 8B EC 83 E4 F8 83 EC 34 53 8B 5D 0C 89 4C 24 04")));
		LoadFromBufferEx(pKeyValues, resourceName, pBuffer, pFileSystem, pPathID, pfnEvaluateSymbolProc);
	} 
	static void SetUpTextures()
	{
		visible_tex = SourceEngine::Interfaces::MaterialSystem()->CreateMaterial(false, false, false); 
		hidden_tex = SourceEngine::Interfaces::MaterialSystem()->CreateMaterial(false, true, false);
		wireframe_tex = SourceEngine::Interfaces::MaterialSystem()->CreateMaterial(false, false, true);
	}
private:
	static void Normalize(SourceEngine::QAngle& angle)
	{
		while (angle.x > 89.0f) {
			angle.x -= 180.f;
		}
		while (angle.x < -89.0f) {
			angle.x += 180.f;
		}
		while (angle.y > 180.f) {
			angle.y -= 360.f;
		}
		while (angle.y < -180.f) {
			angle.y += 360.f;
		}
	}

	static void ClampAngles(SourceEngine::QAngle &angles)
	{
		if (angles.y > 180.0f)
			angles.y = 180.0f;
		else if (angles.y < -180.0f)
			angles.y = -180.0f;

		if (angles.x > 89.0f)
			angles.x = 89.0f;
		else if (angles.x < -89.0f)
			angles.x = -89.0f;

		angles.z = 0;
	}
};