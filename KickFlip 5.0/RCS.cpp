#include "Aim.hpp"

void Aim::RCS()
{
	if (GlobalVars.Settings.Aim.RCS.Enabled && SourceEngine::Interfaces::Engine()->IsInGame())
	{
		auto pLocal = C_CSPlayer::GetLocalPlayer();
		auto punchAngles = *pLocal->AimPunch() * 2.0f; //ADD RANDOM 
		if (punchAngles.x != 0.0f || punchAngles.y != 0) 
		{
			UserCMD->viewangles -= punchAngles;
			if (!Utils::Clamp(UserCMD->viewangles))
			{
				abort(); //Failed to clamp angles!!1! ABOOOOOORT
			}
			
		}
	}
}