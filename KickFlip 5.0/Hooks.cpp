#include "CSGOStructs.hpp"
#include "Utilities.hpp"
#include "includes.hpp"
#include "Hooks.hpp"
#include "SkinFunctions.hpp"
#include "ChatSpam.hpp"
#include "Misc.hpp"
#include "ImGUI/imgui.h"
#include "ImGUI/DX9/imgui_dx9.h"
#include "Spoofing.hpp"
#include "Config.hpp"
#include "SkinConfigFile.hpp"
//====Orig. Hooks =====
CreateMove_t                       g_fnOriginalCreateMove = nullptr;
PaintTraverse_t					   g_fnOriginalPaintTraverse = nullptr;
FrameStageNotify_t                 g_fnOriginalFrameStageNotify = nullptr;
FireEventClientSide                g_fnOriginalFireEventClientSide = nullptr;
FireEvent_t						   g_fnOriginalFireEvent = nullptr;
OverrideView_t                     g_fnOriginalOverRideView = nullptr;
EndScene_t                         g_fnOriginalEndScene = nullptr;
Reset_t                            g_fnOriginalReset = nullptr;
PlaySound_t                        g_fnOriginalPlaySound = nullptr;
DrawModelExecute_t                 g_fnOriginalDrawModelExecute = nullptr;
SourceEngine::RecvVarProxyFn	   g_fnSequenceProxyFn = NULL;


//VFTable hooks
std::unique_ptr<VFTableHook>       g_pD3DDevice9Hook = nullptr;
std::unique_ptr<VFTableHook>       g_pClientModeHook = nullptr; 
std::unique_ptr<VFTableHook>       g_pClientHook = nullptr; 
std::unique_ptr<VFTableHook>       g_pGameEventHook = nullptr;
std::unique_ptr<VFTableHook>       g_pMatSurfaceHook = nullptr;
std::unique_ptr<VFTableHook>       g_pVGUIHook = nullptr;
std::unique_ptr<VFTableHook>       g_ModelRenderHook = nullptr;

WNDPROC                            g_pOldWindowProc = nullptr; //Old WNDPROC pointer
HWND                               g_hWindow = nullptr; //Handle to the CSGO window
ServerRankRevealAllFn			   ServerRankRevealAllEx = NULL;

bool                               vecPressedKeys[256] = {};
bool                               g_bWasInitialized = false; 
bool							   g_hasGuiInit = false;

SourceEngine::CUserCmd*            UserCMD = 0;
bool							   bSendPacket = true;
bool							   Aimbotting = false;
bool							   Return = false; 
bool							   devConsole = false;
bool							   KillAll = false;

HMODULE DLL = NULL;
cSpoof* Spoof = new cSpoof;
void Hooks::Initialize(LPVOID lpParameter)
{
	if (devConsole)
	{
		AllocConsole();
		AttachConsole(GetCurrentProcessId());
		freopen("CON", "w", stdout);
		std::cout << "Attached console" << std::endl;
	}
	//Find CSGO Window
	while (!(g_hWindow = FindWindowA(XorStr("Valve001"), NULL))) Sleep(200);
	std::cout << "Found WndProc" << std::endl;
	//Replace the WndProc with ours
	if (g_hWindow)
		g_pOldWindowProc = (WNDPROC)SetWindowLongPtr(g_hWindow, GWL_WNDPROC, (LONG_PTR)Hooked_WndProc);
	std::cout << "Replaced Wndproc" << std::endl;
	
	//Builds the netvar database
	NetvarManager::Instance()->CreateDatabase();
	std::cout << "Built Database" << std::endl;
	//NetvarManager::Instance()->Dump(XorStr("netvars.txt")); //WRITE NETVARS TO FILE: USEFUL FOR REVERSING AND DEBUGGING
	std::cout << "Snagged some netvars" << std::endl;
	Offsets->Dump();
	
	std::cout << "Dumped offsets" << std::endl;
	//Set up D3D hooks
	g_pD3DDevice9Hook = std::make_unique<VFTableHook>((PPDWORD)Offsets->d3d9Device, true);
	g_fnOriginalReset = g_pD3DDevice9Hook->Hook(16, Hooked_Reset);                            //Hooks IDirect3DDevice9::EndScene
	g_fnOriginalEndScene = g_pD3DDevice9Hook->Hook(42, Hooked_EndScene);                      //Hooks IDirect3DDevice9::Reset
	std::cout << "Hooked DX9" << std::endl;
	//Create the virtual table hooks
	
	g_pClientModeHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::ClientMode(), true);
	g_pClientHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::Client(), true);
	g_pGameEventHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::GameEventManager(), true);
	g_pMatSurfaceHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::MatSurface(), true);
	g_pVGUIHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::VGUIPanel(), true);
	g_ModelRenderHook = std::make_unique<VFTableHook>((PPDWORD)SourceEngine::Interfaces::ModelRender(), true);
	std::cout << "Created Vtable hooks" << std::endl;
	//Function hooks
	 
	g_fnOriginalFrameStageNotify = g_pClientHook->Hook(36, (FrameStageNotify_t)Hooked_FrameStageNotify); //Hooks IBaseClientDLL::FrameStageNotify
	g_fnOriginalFireEventClientSide = g_pGameEventHook->Hook(8, (FireEventClientSide)Hooked_FireEventClientSide); //Hooks IGameEventManager::FireEventClientSide
	g_fnOriginalFireEvent = g_pGameEventHook->Hook(7, (FireEvent_t)Hooked_FireEvent);
	g_fnOriginalCreateMove = g_pClientModeHook->Hook(24, (CreateMove_t)Hooked_CreateMove); //Hooks IClientMode::CreateMove
	g_fnOriginalPaintTraverse = g_pVGUIHook->Hook(41, (PaintTraverse_t)Hooked_PaintTraverse); //PaintTraverse 
	g_fnOriginalOverRideView = g_pClientModeHook->Hook(18, (OverrideView_t)Hooked_OverrideView);
	g_fnOriginalPlaySound = g_pMatSurfaceHook->Hook(82, (PlaySound_t)Hooked_PlaySound);       //Hooks ISurface::PlaySound
	g_fnOriginalDrawModelExecute = g_ModelRenderHook->Hook(21, (DrawModelExecute_t)Hooked_DrawModelExecute);
	Hook_Proxy(); //Replace viewmodel proxy with our model switcher
	std::cout << "Hooked Functions" << std::endl;

	Utils::SetUpTextures();

	//LOAD CONFIG
	SkinsConfig->Load();
	Config->Load();
	
	//Set Icon CFG
	SkinFunctions::SetKillIconCfg();
	Spoof->Update();
	
	while ((!vecPressedKeys[VK_END]) || !KillAll)
	{
		if (SourceEngine::Interfaces::Engine()->IsInGame())
		{
			if (GlobalVars.Enabled.ChatSpam)
				cChatSpam::Run();
			cChatSpam::RunESP();
			cMisc::Run();
			Spoof->Run();
		}
		Sleep(1);
	}
	//REMOVE
	g_pClientModeHook->RestoreTable();
	g_pClientHook->RestoreTable();
	g_pGameEventHook->RestoreTable();
	g_pMatSurfaceHook->RestoreTable();
	g_pVGUIHook->RestoreTable();
	g_pD3DDevice9Hook->RestoreTable();
	SetWindowLongPtr(g_hWindow, GWL_WNDPROC, (LONG_PTR)g_pOldWindowProc);
	FreeLibraryAndExitThread(DLL, 0);
	
}




//HOOKS THE NETVAR PROXY SO OUR KNIFE ANIMATIONS ARENT SHIT
void Hooks::Hook_Proxy()
{
	for (SourceEngine::ClientClass* pClass = SourceEngine::Interfaces::Client()->GetAllClasses(); pClass; pClass = pClass->m_pNext)
	{
		if (!strcmp(pClass->m_pNetworkName, "CBaseViewModel"))
		{
			
			SourceEngine::RecvTable* pClassTable = pClass->m_pRecvTable;

			for (int nIndex = 0; nIndex < pClassTable->m_nProps; nIndex++)
			{
				SourceEngine::RecvProp* pProp = &pClassTable->m_pProps[nIndex];

				if (!pProp || strcmp(pProp->m_pVarName, "m_nSequence"))
					continue;

				
				g_fnSequenceProxyFn = pProp->m_ProxyFn;

				
				pProp->m_ProxyFn = (SourceEngine::RecvVarProxyFn)SetViewModelSequence;

				break;
			}

			break;
		}
	}
} 

